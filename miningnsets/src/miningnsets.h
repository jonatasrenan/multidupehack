#include "single_nset.h"
#include "util.h"

class Miningnsets{
	std::vector< single_nset > *all_cover_nsets;
	std::map< std::vector<unsigned int>, double > *tuples;

	public:
		Miningnsets(std::vector< single_nset > *p1, std::map< std::vector<unsigned int>, double > *p2);

		//sum all tuples that have attribute atr and nset's attributes.
		double sumTuples(int dim, unsigned int atr, single_nset &nset, std::vector<unsigned int> &attributes, int i = 0);

		//a = a + b
		void nsets_union(single_nset &a, const single_nset &b);

		//a = a - b ~ didnt update sum and lambda of a
		void nsets_disjunction(single_nset &a, const single_nset &b);

		//verify if a c b
		bool isInside(const single_nset &a, const single_nset &b);

		//expand
		single_nset expand(const single_nset &seed, double lambda_min);

		//choices
		std::vector< single_nset > solve(double lambda_min);

		
};