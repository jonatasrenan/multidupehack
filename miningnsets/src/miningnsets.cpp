#include "miningnsets.h"

Miningnsets::Miningnsets(std::vector< single_nset > *p1, std::map< std::vector<unsigned int>, double > *p2){
	all_cover_nsets = p1;
	tuples = p2;
}

double Miningnsets::sumTuples(int dim, unsigned int atr, single_nset &nset, std::vector<unsigned int> &attributes, int i){
	if (i >= nset.dimensions.size()){
		return (*tuples)[attributes];
	}
	if (i == dim){
		attributes.push_back(atr);
		double ans = sumTuples(dim, atr, nset, attributes, i+1);
		attributes.pop_back();
		return ans;
	}

	double ans = 0;
	for (const auto &it : nset.dimensions[i]){
		attributes.push_back(it);
		ans += sumTuples(dim, atr, nset, attributes, i+1);
		attributes.pop_back();
	}
	return ans;
}

void Miningnsets::nsets_union(single_nset &a, const single_nset &b){
	unsigned long size = 1;

	for (const auto &dim : a.dimensions){
		size *= dim.size();
	}

	for (int i = 0; i < b.dimensions.size(); i++){
		for (const auto &atr : b.dimensions[i]){
			if (a.dimensions[i].find(atr) == a.dimensions[i].end()){
				std::vector<unsigned int> aux;
				a.sum += sumTuples(i, atr, a, aux);
				size += size / a.dimensions[i].size();
				a.lambda = a.sum / size * a.sum;
				a.dimensions[i].insert(atr);
			}
		}
	}
}

void Miningnsets::nsets_disjunction(single_nset &a, const single_nset &b){
	for (int i = 0; i < a.dimensions.size(); i++){
		for (const auto &atr : a.dimensions[i]){
			if (b.dimensions[i].find(atr) != b.dimensions[i].end()){
				a.dimensions[i].erase(atr);
			}
		}
	}
}

bool Miningnsets::isInside(const single_nset &a, const single_nset &b){
	for (int i = 0; i < a.dimensions.size(); i++){
		for (const auto &atr : a.dimensions[i]){
			if (b.dimensions[i].find(atr) == b.dimensions[i].end())
				return false;
		}
	}
	return true;
}

single_nset Miningnsets::expand(const single_nset &seed, double lambda_min){
	std::set< single_nset > all_current_nsets;
	single_nset actual = seed;
	single_nset ans = seed;

	for (const auto &nset : *all_cover_nsets){
		all_current_nsets.insert(nset);
	}

	while (true){
		single_nset id;
		bool ok = false;
		double gr = ans.lambda;
		for (const auto &nset : all_current_nsets){
			if (cmpf(nset.lambda, lambda_min) <= 0)
				continue;
			actual = ans;
			nsets_union(actual, nset);
			if (cmpf(actual.lambda, gr) > 0){
				id = nset;
				gr = actual.lambda;
				ok = true;
			}
		}

		if (!ok)
			break;

		nsets_union(ans, id);
		all_current_nsets.erase(id);
	}

	return ans;
}

std::vector< single_nset > Miningnsets::solve(double lambda_min){
	std::priority_queue< single_nset > all_current_nsets;

	std::vector< single_nset > answer;

	int sz = -1;

	while (sz != answer.size()){
		sz = answer.size();
		answer.clear();

		for (const auto &nset : *all_cover_nsets){
			if (cmpf(nset.lambda, lambda_min) > 0)
				all_current_nsets.push(nset);
		}

		while (!all_current_nsets.empty()){
			bool inside = false;
				for (const auto &nset : answer){
					if (isInside(all_current_nsets.top(), nset)){
						inside = true;
						break;
					}
				}

			if (!inside){
				single_nset expansion = expand(all_current_nsets.top(), lambda_min);

				int i = 0;
				for (const auto &nset : answer){
					if (isInside(nset, expansion)){
						answer.erase(answer.begin()+i);
					}
					i++;
				}

				answer.push_back(expansion);
			}

			all_current_nsets.pop();
		}
		break;

		if (sz > 0) delete all_cover_nsets;
		all_cover_nsets = new std::vector< single_nset >(answer.begin(), answer.end());
	}

	return answer;
}