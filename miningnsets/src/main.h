#include <fstream>
#include <iostream>
#include <string>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include "sysexits.h"

#include "NoFileException.h"
#include "IncorrectNbOfDimensionsException.h"

#include "miningnsets.h"

using namespace boost;
using namespace std;

