#include "util.h"

class single_nset{
public:
	std::vector< std::set<unsigned int> > dimensions;
	double sum, lambda;

	single_nset(){
		sum = lambda = 0;
	}

	bool operator<(const single_nset &other) const{
		return cmpf(lambda, other.lambda) == -1;
	}
};