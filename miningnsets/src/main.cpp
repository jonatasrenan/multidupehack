#include "main.h"

vector<double> getVectorFromString(const string& str){
	vector<double> tokens;
  	double token;
  	stringstream ss(str);
  	while (ss >> token){
  		tokens.push_back(token);
    }
  	return tokens;
}

vector< vector<double> > getMatrixFromFile(ifstream& file) throw(NoFileException){
  	vector< vector<double> > matrix;
  	while (!file.eof()){
      	string rowString;
      	getline(file, rowString);
      	const vector<double> row = getVectorFromString(rowString);
      	if (!row.empty()){
	  		matrix.push_back(row);
		}
    }
  	return matrix;
}

int main(int argc, char* argv[]){
	if (argc != 3){
		cerr << "Usage: baseline n-set-file lambda-min" << endl;
		return EX_USAGE;
	}

	ifstream file1(argv[1]);
	if (file1.fail()){
		cerr << NoFileException(argv[1]).what() << endl;
		return EX_IOERR;
	}

	vector< vector<double> > matrix = getMatrixFromFile(file1);
	file1.close();

	double lambda_min = lexical_cast<double>(argv[2]);

	vector< single_nset > all_ncliques;
	map< vector<unsigned int>, double > tuples;

	for (int i = 0; i < matrix.size(); i++){
		int n = matrix[i].size()-1;
		single_nset aux;
		vector<unsigned int> v(matrix[i].begin(), matrix[i].end()-1);
		tuples[v] = matrix[i][n];
		aux.sum = matrix[i][n];
		aux.lambda = aux.sum * aux.sum;
		aux.dimensions.resize(n);
		for (int j = 0; j < n; j++)
			aux.dimensions[j].insert(v[j]);
		all_ncliques.push_back(aux);
	}

	Miningnsets miningnsets(&all_ncliques, &tuples);

	vector< single_nset > answer = miningnsets.solve(lambda_min);

	for (const auto& nset : answer){
		for (const auto& part : nset.dimensions){
			bool b = true;
			for (const auto& node : part){
				if (b) b = false;
				else cout << ",";
				cout << node;
			}
			cout << " ";
		}
		cout << endl;
	}

	return 0;
}
