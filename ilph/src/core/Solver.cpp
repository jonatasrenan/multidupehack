#include "Solver.h"

DataTrie<double> Solver::alldata(0.00);

Solver::Solver(const Pattern &allpatterns){
	int xs = 0, n = alldata.getChildsLen(), k = allpatterns.getNSet().size();
	unsigned int name = 0;
	total_size = allpatterns.getArea();

	for (int i = 0; i < k; i++){
		ksizes.push_back(allpatterns.getNSet()[i].size());
	}

	vector<vector<unsigned int>> nSet = allpatterns.getNSet();
	hyperplanes = vector<vector<pair<double, unsigned int>>>(k);

	total_data = total_square_data = 0;
	for (const auto& tuple: allpatterns.toTuples()){
		double vlr = alldata.getTuple(tuple.begin(), tuple.end());
		if (vlr == 0) continue;

		total_square_data += vlr*vlr;
		total_data += vlr;
	}

	for (int i = 0; i < k; i++){
		vector<vector<unsigned int>> qSet = allpatterns.getNSet();
		qSet[i].clear();
		for (const auto &it : nSet[i]){
			qSet[i].push_back(it);
			Pattern aux(qSet);
			double vlr = alldata.sumSet(aux.toTuples());
			hyperplanes[i].push_back(make_pair(vlr, it));
			qSet[i].pop_back();
		}
	}

	for (int i = 0; i < k; i++)
		sort(hyperplanes[i].rbegin(), hyperplanes[i].rend());

#ifdef DEBUG
	for (int i = 0; i < k; i++){
		cout << "hyperplanes of dimension " << i << endl << "vlr/id: ";
		for (int j = 0; j < hyperplanes[i].size(); j++)
			cout << hyperplanes[i][j].first << "/" << hyperplanes[i][j].second << ", ";
		cout << endl;
	}
#endif

}

Solver::~Solver(){}

Pattern* Solver::solve(const Pattern* fragment){
	vector<vector<unsigned int>> nSet = fragment->getNSet();
	vector<set<unsigned int>> sortedNSet(nSet.size());
	for (int i = 0; i < nSet.size(); i++){
		for (const auto &vlr : nSet[i]){
			sortedNSet[i].insert(vlr);
		}
	}

	vector<unsigned int> pointer(nSet.size(), 0);

	unsigned int area = 1;
	for (int i = 0; i < nSet.size(); i++)
		area *= nSet[i].size();

	double membershipSum = alldata.sumSet(fragment->toTuples());
	double actualG = fragment->getG();

	while (true){
		unsigned int dimF = INT_MAX;
		double membershipSum_aux;
		unsigned int area_aux;

		for (int i = 0; i < ksizes.size(); i++){
			vector<vector<unsigned int>> nSetF = nSet;
			nSetF[i].clear();
			while (pointer[i] < ksizes[i]){
				if (sortedNSet[i].find(hyperplanes[i][pointer[i]].second) == sortedNSet[i].end()){
					nSetF[i].push_back(hyperplanes[i][pointer[i]].second);
					Pattern aux(nSetF);
					double membershipSumF = alldata.sumSet(aux.toTuples());
					unsigned int areaF = aux.getArea();
					double newG = (membershipSum + membershipSumF) * (membershipSum + membershipSumF) / (area + areaF);
					if (newG > actualG){
						actualG = newG;
						membershipSum_aux = membershipSum + membershipSumF;
						area_aux = area + areaF;
						dimF = i;
					}
					break;
				}
				else pointer[i]++;
			}
		}

		if (dimF == INT_MAX) break;

#ifdef DEBUG
		cout << "selected dimension: " << dimF << endl;
		cout << "actualG: " << actualG << " membershipSum: " << membershipSum_aux << " area: " << area_aux << endl;
#endif

		nSet[dimF].push_back(hyperplanes[dimF][pointer[dimF]].second);
		sortedNSet[dimF].insert(hyperplanes[dimF][pointer[dimF]].second);
		area = area_aux;
		membershipSum = membershipSum_aux;
	}

	Pattern *p = new Pattern(nSet);
	p->setMembershipSum(membershipSum);
	return p;
}

void Solver::updateSet(const vector<vector<unsigned int>>::const_iterator nSetBegin, const vector<vector<unsigned int>>::const_iterator nSetEnd, vector<unsigned int> &prefix, Pattern *actual, DataTrie<double> &t){
	if (nSetBegin == nSetEnd){
		double s = t.getTuple(prefix.begin(), prefix.end());
		if (s != 1.0){
			actual->setArea(actual->getArea()+1);
			actual->setMembershipSum(actual->getMembershipSum() + alldata.getTuple(prefix.begin(), prefix.end()));
		}
	}
	else{
		for (const auto& valueIt : *nSetBegin){
			prefix.push_back(valueIt);
			updateSet(nSetBegin+1, nSetEnd, prefix, actual, t);
			prefix.pop_back();
		}
	}
}

vector<Pattern*> Solver::solveAll(vector<Pattern*> &patterns){
	vector<Pattern*> answer;
	vector<unsigned int> prefix;
	priority_queue<Pattern*, vector<Pattern*>, Pattern::Comparator> q;
	DataTrie<double> t(0.00);

#ifdef DEBUG
	cout << "* Main solve Phase" << endl;
#endif

	for (int i = 0; i < patterns.size(); i++){
		q.push(patterns[i]);
	}

	while (!q.empty()){
		Pattern* actual = q.top();
		q.pop();

#ifdef DEBUG
		cout << "Getting fragment " << *actual << endl << "    G=" << actual->getG() << std::endl;
#endif

		if (actual->last_id == answer.size()){
			Pattern *bigpat = solve(actual);
#ifdef DEBUG
			cout << "expanded to: " << *bigpat << endl;
#endif
			answer.push_back(bigpat);
			t.addSet(bigpat->getNSet().begin(), bigpat->getNSet().end(), prefix, 1.00);
		}
		else{
			actual->setArea(0);
			actual->setMembershipSum(0);
			updateSet(actual->getNSet().begin(), actual->getNSet().end(), prefix, actual, t);
			actual->last_id = answer.size();

			if (actual->getG() > 0)
				q.push(actual);
		}
	}

	return answer;
}
