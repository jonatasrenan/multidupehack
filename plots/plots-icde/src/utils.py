import os
import subprocess
import pandas as pd
import numpy as np

def std_above_mean(df):
    ret = pd.DataFrame(df.alpha.unique(), columns=['alpha'])
    ret = ret.set_index('alpha')
    for i in df.alpha.unique():
        df_alpha = df[df.alpha == i]
        df_alpha = df_alpha.set_index('alpha')
        for icolumn in df_alpha:
            if icolumn not in ret:
                ret[icolumn] = None
            column = df_alpha[icolumn]
            mean = column.mean()
            points = column[column > mean].dropna()
            difference = points - mean
            sum_of_squares_of_difference = (difference**2).sum()
            qtd = len(points)
            if qtd > 0:
                variance = sum_of_squares_of_difference / qtd
                std = np.sqrt(variance)
                ret.xs(i)[icolumn] = mean + std
            else:
                ret.xs(i)[icolumn] = None
            pass
    return ret.fillna(0)


def std_below_mean(df):
    ret = pd.DataFrame(df.alpha.unique(), columns=['alpha'])
    ret = ret.set_index('alpha')
    for i in df.alpha.unique():
        df_alpha = df[df.alpha == i]
        df_alpha = df_alpha.set_index('alpha')
        for icolumn in df_alpha:
            if icolumn not in ret:
                ret[icolumn] = None
            column = df_alpha[icolumn]
            mean = column.mean()
            points = column[column < mean].dropna()
            difference = points - mean
            sum_of_squares_of_difference = (difference**2).sum()
            qtd = len(points)
            if qtd > 0:
                variance = sum_of_squares_of_difference / qtd
                std = np.sqrt(variance)
                ret.xs(i)[icolumn] = mean - std
            else:
                ret.xs(i)[icolumn] = None
            pass
    return ret.fillna(0)

class ExtDF(pd.DataFrame):
    def rename(self, index=None, columns=None, **kwargs):
        newcolumns = {k if (k in self.keys()) else self.filter(regex='%s' % k).columns[0]: v for k, v in columns.items()}
        return super(ExtDF, self).rename(index=index, columns=newcolumns, **kwargs)


def sh(line):
    output = None
    devnull = open(os.devnull, 'w')
    try:
        output = list(filter(None, str(subprocess.check_output([line],  stderr=devnull, shell=True), 'utf-8').split('\n')))
    except subprocess.CalledProcessError as ex:
        if ex.returncode == 1:
            output = list(filter(None, str(ex.output, 'utf-8').split('\n')))
        else:
            output = None
    return output
