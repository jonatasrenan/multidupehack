set logscale x
set logscale y
set xlabel "number of correct observations"
set ylabel "number of extracted patterns"
set xrange [16:1]
set xtics 1,2
set yrange [1:100000]
set datafile missing "NaN"
set grid
set terminal postscript eps color solid enhanced lw 7 "Times-Roman" 24
set output "../OUTPUT.eps"
set nokey
load '../../templates/style.plt'
set bars 3
plot \
"mdhbin+ilp" using 1:3 with lines ls 3 title "Bigfoot",\
"mdhbin+ilp" using 1:3:9:6 with errorbars ls 3 pt 1,\
"mdhbin+ilphbin+ilp" using 1:3 with lines ls 7 title "Bigfoot-LR+Bigfoot",\
"mdhbin+ilphbin+ilp" using 1:3:9:6 with errorbars ls 7 pt 1,\
"boxcluster" using 1:3 with lines ls 4 title "TriclusterBox",\
"boxcluster" using 1:3:9:6 with errorbars ls 4 pt 1,\
"walk-format" using 1:3 with lines ls 6 title "Walk'n'merge",\
"walk-format" using 1:3:9:6 with errorbars ls 6 pt 1,\
"dbtf-format" using 1:3 with lines ls 8 title "DBTF",\
"dbtf-format" using 1:3:9:6 with errorbars ls 8 pt 1,\
SIZE_HIDDEN_PATTERNS with lines ls 9 lw 0.7 title "hidden patterns"
