set logscale x
set logscale y
set xlabel "number of correct observations"
set ylabel "time (s)"
set xrange [16:1]
set xtics 1,2
set datafile missing "NaN"
set yrange [0.001:1000]
set grid
set terminal postscript eps color solid enhanced lw 7 "Times-Roman" 24
set output "../OUTPUT.eps"
set nokey
load '../../templates/style.plt'
set bars 3
plot \
"mdh" using 1:4 with lines ls 1 title "multidupehack", \
"mdh" using 1:4:10:7 with errorbars ls 1 pt 1,\
"mdh+ilp" using 1:4 with lines ls 3 title "Bigfoot", \
"mdh+ilp" using 1:4:10:7 with errorbars ls 3 pt 1,\
"mdh+ilph" using 1:4 with lines ls 5 title "Bigfoot-LR", \
"mdh+ilph" using 1:4:10:7 with errorbars ls 5 pt 1,\
"mdh+ilph+ilp" using 1:4 with lines ls 7 title "Bigfoot-LR+Bigfoot", \
"mdh+ilph+ilp" using 1:4:10:7 with errorbars ls 7 pt 1
