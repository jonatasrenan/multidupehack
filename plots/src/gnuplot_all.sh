#!/bin/bash
dir=$(pwd)
for d in $1/*/ ; do 
	cd "$d"
	echo -e "\n$d" 
	gnuplot quality-all.plt
	gnuplot time-all.plt
	gnuplot size-all.plt
	gnuplot quality-bin3dim.plt
	gnuplot time-bin3dim.plt
	gnuplot size-bin3dim.plt
	cd "$dir"
done
