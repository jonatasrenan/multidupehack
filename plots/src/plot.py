#!/usr/bin/python3
# coding=utf-8
import sys
import numpy as np
import pandas as pd
from src.utils import sh, ExtDF, std_above_mean, std_below_mean


def plot(dim, epsilon_relative, minsizes, basefolder, csvfile):
    # cria pasta
    folder = "%sdim_%serel_%smin" % (dim, epsilon_relative, minsizes)
    path = "%s/%s" % (basefolder, folder)
    sh("mkdir -p %s" % path)

    # converte parametros
    df = pd.read_csv(csvfile)
    df = df.loc[df['_dim'] == int(dim)]
    df = df.loc[df['_minsizes'] == int(minsizes)]
    df = df.loc[df['_epsilon_relative'] == float(epsilon_relative)]

    # df = df[df['_coeff'] == 2]

    # altera nomes
    df = df.rename(columns={'_correct_obs': 'alpha'})

    curve_names = ['mdh', '1m_sqrtm-paf1', '0.01m_sqrtm-paf1', '0.0001m_sqrtm-paf1']

    #limpa não-definidos
    df = df.replace('not defined', 0)
    df[[col for col in df if col.startswith('status')]].fillna('', inplace=True)
    df[[col for col in df if col.startswith('error')]].fillna('', inplace=True)

    # soma os tempos
    df['time_mdh'] = df['timeuser_mdh']
    df['time_1m_sqrtm-paf1-corr'] = df['timeuser_1m_sqrtm-paf1'] + df['time_mdh']
    df['time_0.01m_sqrtm-paf1-corr'] = df['timeuser_0.01m_sqrtm-paf1'] + df['time_mdh']
    df['time_0.0001m_sqrtm-paf1-corr'] = df['timeuser_0.0001m_sqrtm-paf1'] + df['time_mdh']


    # agrupa os dados nas observacoes corretas, tira a média dos dados e o desvio acima e abaixo da média
    mean = {}
    above_mean = {}
    below_mean = {}
    for c in curve_names:
        mean[c] = ExtDF(df[['alpha', 'corr_' + c, 'quant_' + c, 'timeuser_' + c]].groupby('alpha').agg(np.mean))
        above_mean[c] = ExtDF(std_above_mean(df[['alpha', 'corr_' + c, 'quant_' + c, 'timeuser_' + c]]))
        below_mean[c] = ExtDF(std_below_mean(df[['alpha', 'corr_' + c, 'quant_' + c, 'timeuser_' + c]]))

    # renomeia as colunas:
    for c in curve_names:
        mean[c].rename(
            columns={'corr': 'quality_mean', 'quant': 'size_mean', 'timeuser': 'time_mean'}, inplace=True)
        above_mean[c].rename(
            columns={'corr': 'quality_std_above', 'quant': 'size_std_above', 'timeuser': 'time_std_above'}, inplace=True)
        below_mean[c].rename(
            columns={'corr': 'quality_std_below', 'quant': 'size_std_below', 'timeuser': 'time_std_below'}, inplace=True)

    data = {}
    # agrupa num mesmo dataframe
    for c in curve_names:
        data[c] = pd.concat([mean[c], above_mean[c], below_mean[c]], axis=1)

    # remove pontos com erro
    for c in curve_names:
        df['status_'+c].fillna('', inplace=True)
        df['error_'+c].fillna('', inplace=True)

        alphas = df[df['status_' + c] == 'error']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data[c].ix[alphas] = 'NaN'

        alphas = df[df['error_' + c] == 'err_tempo']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data[c].ix[alphas] = 'NaN'

        alphas = df[df['error_' + c] == 'err_memoria'][
            'alpha'].drop_duplicates().values
        if alphas.size > 0:
            data[c].ix[alphas] = 'NaN'

        alphas = df[df['error_' + c] == 'err_configuracao'][
            'alpha'].drop_duplicates().values
        if alphas.size > 0:
            data[c].ix[alphas] = 'NaN'

    # preenche valores nulos
    for c in curve_names:
        data[c].fillna("NaN", inplace=True)

    # quando o mdh não trazer nenhum padrão deve remover os pontos gerados pelos outros algoritmos que dependem dele
    for i in data['mdh'].index:
        if data['mdh'].loc[i, 'size_mean'] == 0:
            for deps in ['1m_sqrtm-paf1', '0.01m_sqrtm-paf1', '0.0001m_sqrtm-paf1']:
                data[deps].loc[i,     ['size_mean', 'quality_mean', 'time_mean', 'size_std_above', 'size_std_below', 'quality_std_above', 'quality_std_below', 'time_std_above', 'time_std_below']] = "NaN"

    # cria os arquivos separados por espaco
    for c in curve_names:
        data[c].to_csv('%s/%s' % (path, c), sep=' ')

    # comenta a primeira linha de cada um
    for c in curve_names:
        sh("""sed -i '1 s/^/#/' %s/%s""" % (path, c))


    # cria quality-all.plt
    quality_filename = "quality-all.plt"
    quality_fullpath = "%s/%s" % (path, quality_filename)
    sh("cp ./templates/quality-all.plt %s" % quality_fullpath)
    sh("""sed -i 's/OUTPUT/%s-%s/g' %s""" % ("quality", folder, quality_fullpath))

    # cria size-all.plt
    size_filename = "size-all.plt"
    size_fullpath = "%s/%s" % (path, size_filename)
    sh("cp ./templates/size-all.plt %s" % size_fullpath)
    sh("""sed -i 's/OUTPUT/%s-%s/g' %s""" % ("size", folder, size_fullpath))
    sh("""sed -i 's/SIZE_HIDDEN_PATTERNS/%s/g' %s""" % ('4.0', size_fullpath))

    # cria time-all.plt
    time_filename = "time-all.plt"
    time_fullpath = "%s/%s" % (path, time_filename)
    sh("cp ./templates/time-all.plt %s" % time_fullpath)
    sh("""sed -i 's/OUTPUT/%s-%s/g' %s""" % ("time", folder, time_fullpath))

    # cria memory.plt
    # time_filename = "memory-all.plt"
    # time_fullpath = "%s/%s" % (path, time_filename)
    # sh("cp ./templates/memory.plt %s" % time_fullpath)
    # sh("""sed -i 's/OUTPUT/%s-%s/g' %s""" % ("memory", folder, time_fullpath))

if __name__ == "__main__":
    if len(sys.argv) == 6:
        d = sys.argv[1]
        erel = sys.argv[2]
        s = sys.argv[3]
        folder = sys.argv[4]   # './res'
        csv = sys.argv[5]
        plot(d, erel, s, folder, csv)
    else:
        print("Usage <dim> <epsilon_relative> <minsizes> <basefolder> <csvfile>")
        exit()