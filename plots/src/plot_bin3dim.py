#!/usr/bin/python3
# coding=utf-8
import sys
import numpy as np
import pandas as pd
from src.utils import sh, ExtDF, std_above_mean, std_below_mean


def plot_bin3dim(dim, epsilon_relative, minsizes, basefolder, csvfile):
    # cria pasta
    folder = "%sdim_%serel_%smin" % (dim, epsilon_relative, minsizes)
    path = "%s/%s" % (basefolder, folder)
    sh("mkdir -p %s" % path)

    # converte parametros
    df = pd.read_csv(csvfile)
    df = df.loc[df['_dim'] == int(dim)]
    df = df.loc[df['_minsizes'] == int(minsizes)]
    df = df.loc[df['_epsilon_relative'] == float(epsilon_relative)]
    # df = df[df['_coeff'] == 2]

    # altera nomes
    df = df.rename(columns={'_correct_obs': 'alpha'})

    curve_names = ['mdhbin', '1m_sqrtmbin-paf1', 'walk-format', 'dbtf-format']
    # curve_names = ['mdhbin', '1m_sqrtmbin-paf1', 'walk-format', 'boxcluster','dbtf-format']

    #limpa não-definidos
    df = df.replace('not defined', 0)
    for c in curve_names:
        df['timeuser_'+ c] = df['timeuser_'+ c] .astype('float64')


    # soma os tempos
    df['time_mdhbin'] = df['timeuser_mdhbin']
    df['time_1m_sqrtmbin'] = df['timeuser_1m_sqrtmbin']
    df['time_1m_sqrtmbin-paf1'] = df['timeuser_1m_sqrtmbin-paf1'] + df['timeuser_1m_sqrtmbin'] + df['time_mdhbin']
    # df['time_boxcluster'] = df['timeuser_boxcluster']
    df['time_walk-format'] = df['timeuser_walk-format']
    df['time_dbtf-format'] = df['timeuser_dbtf-format']

    # agrupa os dados nas observacoes corretas, tira a média dos dados e o desvio acima e abaixo da média
    mean = {}
    above_mean = {}
    below_mean = {}
    for c in curve_names:
        mean[c] = ExtDF(df[['alpha', 'corr_' + c, 'quant_' + c, 'timeuser_' + c]].groupby('alpha').agg(np.mean))
        above_mean[c] = ExtDF(std_above_mean(df[['alpha', 'corr_' + c, 'quant_' + c, 'timeuser_' + c]]))
        below_mean[c] = ExtDF(std_below_mean(df[['alpha', 'corr_' + c, 'quant_' + c, 'timeuser_' + c]]))

    # renomeia as colunas:
    for c in curve_names:
        mean[c].rename(
            columns={'corr': 'quality_mean', 'quant': 'size_mean', 'timeuser': 'time_mean'}, inplace=True)
        above_mean[c].rename(
            columns={'corr': 'quality_std_above', 'quant': 'size_std_above', 'timeuser': 'time_std_above'}, inplace=True)
        below_mean[c].rename(
            columns={'corr': 'quality_std_below', 'quant': 'size_std_below', 'timeuser': 'time_std_below'}, inplace=True)

    data = {}
    # agrupa num mesmo dataframe
    for c in curve_names:
        data[c] = pd.concat([mean[c], above_mean[c], below_mean[c]], axis=1)

    # remove pontos com erro
    for c in curve_names:
        df['status_'+c].fillna('', inplace=True)
        df['error_'+c].fillna('', inplace=True)

        alphas = df[df['status_' + c] == 'error']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data[c].ix[alphas] = 'NaN'

        alphas = df[df['error_' + c] == 'err_tempo']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data[c].ix[alphas] = 'NaN'

        alphas = df[df['error_' + c] == 'err_memoria'][
            'alpha'].drop_duplicates().values
        if alphas.size > 0:
            data[c].ix[alphas] = 'NaN'

        # preenche valores nulos
        for c in curve_names:
            data[c].fillna("NaN", inplace=True)

    # quando o mdh não trazer nenhum padrão deve remover os pontos gerados pelos outros algoritmos que dependem dele
    for i in data['mdhbin'].index:
        if data['mdhbin'].loc[i, 'size_mean'] == 0:
            for deps in ['1m_sqrtmbin-paf1']:
                data[deps].loc[i, ['size_mean', 'quality_mean', 'time_mean', 'size_std_above', 'size_std_below',
                                   'quality_std_above', 'quality_std_below', 'time_std_above',
                                   'time_std_below']] = "NaN"

    # cria os arquivos separados por espaco
    for c in curve_names:
        data[c].to_csv('%s/%s' % (path, c), sep=' ')

    # comenta a primeira linha de cada um
    for c in curve_names:
        sh("""sed -i '1 s/^/#/' %s/%s""" % (path, c))

    # cria quality-bin3dim.plt
    quality_filename = "quality-bin3dim.plt"
    quality_fullpath = "%s/%s" % (path, quality_filename)
    sh("cp ./templates/quality-bin3dim.plt %s" % quality_fullpath)
    sh("""sed -i 's/OUTPUT/%s-%s/g' %s""" % ("quality-bin", folder, quality_fullpath))

    # cria size-bin3dim.plt
    size_filename = "size-bin3dim.plt"
    size_fullpath = "%s/%s" % (path, size_filename)
    sh("cp ./templates/size-bin3dim.plt %s" % size_fullpath)
    sh("""sed -i 's/OUTPUT/%s-%s/g' %s""" % ("size-bin", folder, size_fullpath))
    sh("""sed -i 's/SIZE_HIDDEN_PATTERNS/%s/g' %s""" % ('4.0', size_fullpath))

    # cria time-bin3dim.plt
    time_filename = "time-bin3dim.plt"
    time_fullpath = "%s/%s" % (path, time_filename)
    sh("cp ./templates/time-bin3dim.plt %s" % time_fullpath)
    sh("""sed -i 's/OUTPUT/%s-%s/g' %s""" % ("time-bin", folder, time_fullpath))

if __name__ == "__main__":
    if len(sys.argv) == 6:
        d = sys.argv[1]
        erel = sys.argv[2]
        s = sys.argv[3]
        folder = sys.argv[4]   # './res'
        csv = sys.argv[5]
        plot_bin3dim(d, erel, s, folder, csv)
    else:
        print("Usage <dim> <epsilon_relative> <minsizes> <basefolder> <csvfile>")
        exit()