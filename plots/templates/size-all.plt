set logscale x
set logscale y
set xrange [16:1]
set xtics 1,2
set yrange [1:100000]
set datafile missing "NaN"
set grid
set terminal postscript eps dashed enhanced lw 3 "Times-Roman" 24
set output "../OUTPUT.eps"
#set nokey
set bars 3
plot \
'mdh' using 1:3 w linespoints pt 3 ps 1.7, "" using 1:3:9:6 with yerrorbars ls 1 pt 1,\
'1m_sqrtm-paf1' using 1:3 w linespoints pt 5 ps 1.7, "" using 1:3:9:6 with yerrorbars ls 1 pt 1,\
'0.01m_sqrtm-paf1' using 1:3 w linespoints pt 7 ps 1.7, "" using 1:3:9:6 with yerrorbars ls 1 pt 1,\
SIZE_HIDDEN_PATTERNS with lines pt 15 ps 1.7 lw 0.7

