set logscale x
set logscale y
set xrange [16:1]
set xtics 1,2
set datafile missing "NaN"
set yrange [0.001:1000]
set grid
set terminal postscript eps dashed enhanced lw 3 "Times-Roman" 24
set output "../OUTPUT.eps"
#set nokey
set bars 3
plot \
"1m_sqrtmbin-paf1" using 1:4 with linespoints pt 3 ps 1.7, "" using 1:4:10:7 with yerrorbars ls 1 pt 1,\
"walk-format" using 1:4 with linespoints pt 11 ps 1.7, "" using 1:4:10:7 with yerrorbars ls 1 pt 1,\
"dbtf-format" using 1:4 with linespoints pt 13 ps 1.7, "" using 1:4:10:7 with yerrorbars ls 1 pt 1
