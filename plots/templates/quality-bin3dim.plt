set logscale x
set xrange [16:1]
set xtics 1,2
set yrange [0:1]
set ytics (0, 0.2, 0.4, 0.6, 0.8, 1)
set datafile missing "NaN"
set grid
set terminal postscript eps dashed enhanced lw 3 "Times-Roman" 24
set output "../OUTPUT.eps"
#set nokey
set bars 3
plot \
"1m_sqrtmbin-paf1" using 1:2 with linespoints pt 5 ps 1.7 , "" using 1:2:8:5 with yerrorbars ls 1 pt 1,\
"walk-format" using 1:2 with linespoints pt 11 ps 1.7 , "" using 1:2:8:5 with yerrorbars ls 1 pt 1,\
"dbtf-format" using 1:2 with linespoints pt 13 ps 1.7 , "" using 1:2:8:5 with yerrorbars ls 1 pt 1
