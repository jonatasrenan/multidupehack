set logscale x
set logscale y
set xrange [16:1]
set xtics 1,2
set yrange [:100000]
set grid
set terminal postscript eps enhanced lw 3 "Times-Roman" 24
set output "../OUTPUT.eps"
set nokey
plot \
"mdh" using 1:3 with linespoints pt 3 ps 1.7 title "multidupehack",\
"mdh+paf0" using 1:3 with linespoints pt 5 ps 1.7 title "PAF",\
"mdh+paf1" using 1:3 with linespoints pt 7 ps 1.7 title "APAF",\
"tribox" using 1:3 with linespoints pt 9 ps 1.7 title "PP",\
"tribox+paf1" using 1:3 with linespoints pt 11 ps 1.7 title "PP+PAF"
