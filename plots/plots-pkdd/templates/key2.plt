set noborder
set noxtics
set noytics
set notitle
set noxlabel
set noylabel
set xrange [-10:10]
set yrange [-10:10]  # fix it so you can plot outside the range!
set terminal postscript eps enhanced lw 3 "Times-Roman" 24
set output "key2.eps"
set key samplen 10 spacing 2 font ",40"
plot \
20 with linespoints pt 4 ps 4 title "PAF",\
20 with linespoints pt 8 ps 4 title "APAF"