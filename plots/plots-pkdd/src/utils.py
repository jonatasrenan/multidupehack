import os
import errno
import subprocess


def sh(line):
    output = None
    devnull = open(os.devnull, 'w')
    try:
        output = list(filter(None, str(subprocess.check_output([line],  stderr=devnull, shell=True), 'utf-8').split('\n')))
    except subprocess.CalledProcessError as ex:
        if ex.returncode == 1:
            output = list(filter(None, str(ex.output, 'utf-8').split('\n')))
        else:
            output = None
    return output
