set logscale x
set xlabel "nb of correct observations"
set ylabel "quality"
set xrange [16:1]
set xtics 1,2
set yrange [0:1.35]
set ytics (0, 0.2, 0.4, 0.6, 0.8, 1)
set grid
set terminal postscript eps enhanced lw 3 "Times-Roman" 24
set output "../OUTPUT.eps"
plot \
"mdhbin+ilp" using 1:2 with linespoints pt 5 ps 1.7 title "Bigfoot",\
"mdhbin+ilphbin+ilp" using 1:2 with linespoints pt 9 ps 1.7 title "Bigfoot-LR+Bigfoot",\
"boxcluster" using 1:2 with linespoints pt 10 ps 1.7 title "TriclusterBox",\
"walk-format" using 1:2 with linespoints pt 12 ps 1.7 title "Walk'n'merge"
