set logscale x
set logscale y
set xlabel "nb of correct observations"
set ylabel "time (s)"
set xrange [16:1]
set xtics 1,2
set datafile missing "NaN"
set yrange [:10000]
#set ytics ("0" 0.001, 0.01, 0.1, 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000)
set grid
set terminal postscript eps enhanced lw 3 "Times-Roman" 24
set output "../OUTPUT.eps"
plot \
"mdh" using 1:4 with linespoints pt 3 ps 1.7 title "multidupehack",\
"mdh+ilp" using 1:4 with linespoints pt 5 ps 1.7 title "Bigfoot",\
"mdh+ilph" using 1:4 with linespoints pt 7 ps 1.7 title "Bigfoot-LR",\
"mdh+ilph+ilp" using 1:4 with linespoints pt 9 ps 1.7 title "Bigfoot-LR+Bigfoot"
