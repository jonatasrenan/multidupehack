set logscale x
set logscale y
set xlabel "nb of correct observations"
set ylabel "nb of extracted patterns"
set xrange [16:1]
set xtics 1,2
set yrange [:10000000]
#set ytics ("0" 0.1, 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000)
#set datafile missing ""
set grid
set terminal postscript eps enhanced lw 3 "Times-Roman" 24
set output "../OUTPUT.eps"
plot \
"mdhbin+ilp" using 1:3 with linespoints pt 5 ps 1.7 title "Bigfoot",\
"mdhbin+ilphbin+ilp" using 1:3 with linespoints pt 9 ps 1.7 title "Bigfoot-LR+Bigfoot",\
"boxcluster" using 1:3 with linespoints pt 10 ps 1.7 title "TriclusterBox",\
"walk-format" using 1:3 with linespoints pt 12 ps 1.7 title "Walk'n'merge",\
SIZE_HIDDEN_PATTERNS with lines title "hidden patterns"
