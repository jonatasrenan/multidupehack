#!/usr/bin/python3
# coding=utf-8
import sys
import numpy as np
import pandas as pd
from src.utils import sh

# def sc(df,s1,s2 = ''):
#     return df.filter(regex='%s(.*)%s$' % (s1, s2)).columns[0]

class ExtDF(pd.DataFrame):
    def rename(self, index=None, columns=None, **kwargs):
        newcolumns = {k if (k in self.keys()) else self.filter(regex='%s' % k).columns[0]: v for k, v in columns.items()}
        return super(ExtDF, self).rename(index=index, columns=newcolumns, **kwargs)

    # def rename(self, columns, inplace):
    #     super().rename(columns, inplace)

def plot(dim, epsilon_relative, minsizes, basefolder, csvfile):
    # cria pasta
    folder = "%sdim_%serel_%smin" % (dim, epsilon_relative, minsizes)
    path = "%s/%s" % (basefolder, folder)
    sh("mkdir -p %s" % path)

    # converte parametros
    df = pd.read_csv(csvfile)
    df = df.loc[df['_dim'] == int(dim)]
    df = df.loc[df['_minsizes'] == int(minsizes)]
    df = df.loc[df['_epsilon_relative'] == float(epsilon_relative)]

    df = df[df['_coeff'] == 2]

    # altera nomes
    df = df.rename(columns={'_correct_obs': 'alpha'})

    #limpa não-definidos
    df = df.replace('not defined', 0)

    # soma os tempos
    df['time_mdh'] = df['time_mdh_user']
    df['time_ilph'] = df['time_mdh'] + df['time_ilph_user']
    df['time_mdh_ilp'] = df['time_mdh'] + df['time_mdh-ilp_user']
    df['time_ilph_ilp'] = df['time_mdh'] + df['time_ilph'] + df['time_ilph-ilp_user']

    # agrupa os dados nas observacoes corretas e tira a média dos dados
    mdh = ExtDF(df[['alpha', 'corr_mdh', 'quant_mdh', 'time_mdh']].groupby('alpha').agg(np.mean))
    mdh_ilph = ExtDF(df[['alpha', 'corr_ilph', 'quant_ilph', 'time_ilph']].groupby('alpha').agg(np.mean))
    mdh_ilp = ExtDF(df[['alpha', 'corr_mdh-ilp', 'quant_mdh-ilp', 'time_mdh_ilp']].groupby('alpha').agg(np.mean))
    mdh_ilph_ilp = ExtDF(df[['alpha', 'corr_ilph-ilp', 'quant_ilph-ilp', 'time_ilph_ilp']].groupby('alpha').agg(np.mean))

    # renomeia as colunas
    for p in [mdh, mdh_ilph, mdh_ilp, mdh_ilph_ilp]:
        p.rename(columns={'corr': 'quality', 'quant': 'size', 'time': 'time'}, inplace=True)

    # remove pontos com erro
    for status,status_error,data in [
        ('status_mdh', 'status_mdh-error', mdh),
        ('status_ilph', 'status_ilph-error', mdh_ilph),
        ('status_mdh-ilp', 'status_mdh-ilp-error', mdh_ilp),
        ('status_ilph-ilp', 'status_ilph-ilp-error', mdh_ilph_ilp),
    ]:
        df[status].fillna('', inplace=True)
        df[status_error].fillna('', inplace=True)

        alphas = df[df[status] == 'error']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

        alphas = df[df[status_error] == 'err_tempo']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

        alphas = df[df[status_error] == 'err_memoria']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

        alphas = df[df[status_error] == 'err_configuracao']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

    for p in [mdh, mdh_ilph, mdh_ilp, mdh_ilph_ilp]:

        # quando estoura tempo não deve apresentar o número de padrões e o tempo
        # for i in p.index:
        #     if isinstance(p.loc[i, 'time'], float) and p.loc[i, 'time'] > 3600:
        #         p.loc[i, ['size', 'time']] = "NaN"

        # preenche valores nulos
        p.fillna("NaN", inplace=True)

    # quando o mdh não trazer nenhum padrão deve remover os pontos gerados pelos outros algoritmos que dependem dele
    for i in mdh.index:
        if mdh.loc[i, 'size'] == 0:
            mdh_ilph.loc[i, ['size', 'quality', 'time']] = "NaN"
            mdh_ilp.loc[i, ['size', 'quality', 'time']] = "NaN"
            mdh_ilph_ilp.loc[i, ['size', 'quality', 'time']] = "NaN"

    # cria os arquivos separados por espaco
    mdh.to_csv('%s/mdh' % path, sep=' ')
    mdh_ilp.to_csv('%s/mdh+ilp' % path, sep=' ')
    mdh_ilph.to_csv('%s/mdh+ilph' % path, sep=' ')
    mdh_ilph_ilp.to_csv('%s/mdh+ilph+ilp' % path, sep=' ')

    # comenta a primeira linha de cada um
    sh("""sed -i '' '1 s/^/#/' %s/mdh""" % path)
    sh("""sed -i '' '1 s/^/#/' %s/mdh+ilp""" % path)
    sh("""sed -i '' '1 s/^/#/' %s/mdh+ilph""" % path)
    sh("""sed -i '' '1 s/^/#/' %s/mdh+ilph+ilp""" % path)

    # cria quality-all.plt
    quality_filename = "quality-all.plt"
    quality_fullpath = "%s/%s" % (path, quality_filename)
    sh("cp ./templates/quality-all.plt %s" % quality_fullpath)
    sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("quality", folder, quality_fullpath))

    # cria size-all.plt
    size_filename = "size-all.plt"
    size_fullpath = "%s/%s" % (path, size_filename)
    sh("cp ./templates/size-all.plt %s" % size_fullpath)
    sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("size", folder, size_fullpath))
    sh("""sed -i '' 's/SIZE_HIDDEN_PATTERNS/%s/g' %s""" % ('4.0', size_fullpath))

    # cria time-all.plt
    time_filename = "time-all.plt"
    time_fullpath = "%s/%s" % (path, time_filename)
    sh("cp ./templates/time-all.plt %s" % time_fullpath)
    sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("time", folder, time_fullpath))

    # cria memory.plt
    # time_filename = "memory-all.plt"
    # time_fullpath = "%s/%s" % (path, time_filename)
    # sh("cp ./templates/memory.plt %s" % time_fullpath)
    # sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("memory", folder, time_fullpath))

if __name__ == "__main__":
    if len(sys.argv) == 6:
        d = sys.argv[1]
        erel = sys.argv[2]
        s = sys.argv[3]
        folder = sys.argv[4]   # './res'
        csv = sys.argv[5]
        plot(d, erel, s, folder, csv)
    else:
        print("Usage <dim> <epsilon_relative> <minsizes> <basefolder> <csvfile>")
        exit()