#!/usr/bin/python3
# coding=utf-8
import sys
import numpy as np
import pandas as pd
from src.utils import sh

# def sc(df,s1,s2 = ''):
#     return df.filter(regex='%s(.*)%s$' % (s1, s2)).columns[0]

class ExtDF(pd.DataFrame):
    def rename(self, index=None, columns=None, **kwargs):
        newcolumns = {k if (k in self.keys()) else self.filter(regex='%s' % k).columns[0]: v for k, v in columns.items()}
        return super(ExtDF, self).rename(index=index, columns=newcolumns, **kwargs)

    # def rename(self, columns, inplace):
    #     super().rename(columns, inplace)

def plot_bin3dim(dim, epsilon_relative, minsizes, basefolder, csvfile):
    # cria pasta
    folder = "%sdim_%serel_%smin" % (dim, epsilon_relative, minsizes)
    path = "%s/%s" % (basefolder, folder)
    sh("mkdir -p %s" % path)

    # converte parametros
    df = pd.read_csv(csvfile)
    df = df.loc[df['_dim'] == int(dim)]
    df = df.loc[df['_minsizes'] == int(minsizes)]
    df = df.loc[df['_epsilon_relative'] == float(epsilon_relative)]
    df = df[df['_coeff'] == 2]

    # altera nomes
    df = df.rename(columns={'_correct_obs': 'alpha'})

    #limpa não-definidos
    df = df.replace('not defined', 0)
    df['time_boxcluster_user'] = df['time_boxcluster_user'].astype('float64')
    df['time_walk-format_user'] = df['time_walk-format_user'].astype('float64')

    # soma os tempos
    df['time_mdhbin'] = df['time_mdhbin_user']
    df['time_ilphbin'] = df['time_mdhbin_user'] + df['time_ilphbin_user']
    df['time_mdhbin_ilp'] = df['time_mdhbin_user'] + df['time_mdhbin-ilp_user']
    df['time_ilphbin_ilp'] = df['time_mdhbin_user'] + df['time_ilphbin_user'] + df['time_ilphbin-ilp_user']
    df['time_boxcluster'] = df['time_boxcluster_user']
    df['time_walk-format'] = df['time_walk-format_user']

    # agrupa os dados nas observacoes corretas e tira a média dos dados
    mdhbin = ExtDF(df[['alpha', 'corr_mdhbin', 'quant_mdhbin', 'time_mdhbin']].groupby('alpha').agg(np.mean))
    mdhbin_ilphbin = ExtDF(df[['alpha', 'corr_ilphbin', 'quant_ilphbin', 'time_ilphbin']].groupby('alpha').agg(np.mean))
    mdhbin_ilp = ExtDF(df[['alpha', 'corr_mdhbin-ilp', 'quant_mdhbin-ilp', 'time_mdhbin_ilp']].groupby('alpha').agg(np.mean))
    mdhbin_ilphbin_ilp = ExtDF(df[['alpha', 'corr_ilphbin-ilp', 'quant_ilphbin-ilp', 'time_ilphbin_ilp']].groupby('alpha').agg(np.mean))
    boxcluster = ExtDF(df[['alpha', 'corr_boxcluster', 'quant_boxcluster', 'time_boxcluster']].groupby('alpha').agg(np.mean))
    walk_format = ExtDF(df[['alpha', 'corr_walk-format', 'quant_walk-format', 'time_walk-format']].groupby('alpha').agg(np.mean))

    # renomeia as colunas
    for p in [mdhbin, mdhbin_ilphbin, mdhbin_ilp, mdhbin_ilphbin_ilp, boxcluster, walk_format]:
        p.rename(columns={'corr': 'quality', 'quant': 'size', 'time': 'time'}, inplace=True)

    # remove pontos com erro
    for status,status_error,data in [
        ('status_mdhbin', 'status_mdhbin-error', mdhbin),
        ('status_ilphbin', 'status_ilphbin-error', mdhbin_ilphbin),
        ('status_mdhbin-ilp', 'status_mdhbin-ilp-error', mdhbin_ilp),
        ('status_ilphbin-ilp', 'status_ilphbin-ilp-error', mdhbin_ilphbin_ilp),
        ('status_boxcluster', 'status_boxcluster-error', boxcluster),
        ('status_walk-format', 'status_walk-format-error', walk_format)
    ]:
        df[status].fillna('', inplace=True)
        df[status_error].fillna('', inplace=True)

        alphas = df[df[status] == 'error']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

        alphas = df[df[status_error] == 'err_tempo']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

        alphas = df[df[status_error] == 'err_memoria']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

        alphas = df[df[status_error] == 'err_configuracao']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

    for p in [mdhbin, mdhbin_ilphbin, mdhbin_ilp, mdhbin_ilp, mdhbin_ilphbin_ilp, boxcluster, walk_format]:
        # quando estoura tempo não deve apresentar o número de padrões e o tempo
        # for i in p.index:
        #     if isinstance(p.loc[i, 'time'], float) and p.loc[i, 'time'] > 3600:
        #         p.loc[i, ['size', 'time']] = "NaN"

        # preenche valores nulos
        p.fillna("NaN", inplace=True)

    # quando o mdh não trazer nenhum padrão deve remover os pontos gerados pelos outros algoritmos que dependem dele
    for i in mdhbin.index:
        if mdhbin.loc[i, 'size'] == 0:
            mdhbin_ilphbin.loc[i, ['size', 'quality', 'time']] = "NaN"
            mdhbin_ilp.loc[i, ['size', 'quality', 'time']] = "NaN"
            mdhbin_ilphbin_ilp.loc[i, ['size', 'quality', 'time']] = "NaN"

    # cria os arquivos separados por espaco
    mdhbin.to_csv('%s/mdhbin' % path, sep=' ')
    mdhbin_ilp.to_csv('%s/mdhbin+ilp' % path, sep=' ')
    mdhbin_ilphbin.to_csv('%s/mdhbin+ilphbin' % path, sep=' ')
    mdhbin_ilphbin_ilp.to_csv('%s/mdhbin+ilphbin+ilp' % path, sep=' ')
    boxcluster.to_csv('%s/boxcluster' % path, sep=' ')
    walk_format.to_csv('%s/walk-format' % path, sep=' ')

    # comenta a primeira linha de cada um
    sh("""sed -i '' '1 s/^/#/' %s/mdhbin""" % path)
    sh("""sed -i '' '1 s/^/#/' %s/mdhbin+ilp""" % path)
    sh("""sed -i '' '1 s/^/#/' %s/mdhbin+ilphbin""" % path)
    sh("""sed -i '' '1 s/^/#/' %s/mdhbin+ilphbin+ilp""" % path)
    sh("""sed -i '' '1 s/^/#/' %s/boxcluster""" % path)
    sh("""sed -i '' '1 s/^/#/' %s/walk-format""" % path)

    # cria quality-bin3dim.plt
    quality_filename = "quality-bin3dim.plt"
    quality_fullpath = "%s/%s" % (path, quality_filename)
    sh("cp ./templates/quality-bin3dim.plt %s" % quality_fullpath)
    sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("quality-bin", folder, quality_fullpath))

    # cria size-bin3dim.plt
    size_filename = "size-bin3dim.plt"
    size_fullpath = "%s/%s" % (path, size_filename)
    sh("cp ./templates/size-bin3dim.plt %s" % size_fullpath)
    sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("size-bin", folder, size_fullpath))
    sh("""sed -i '' 's/SIZE_HIDDEN_PATTERNS/%s/g' %s""" % ('4.0', size_fullpath))

    # cria time-bin3dim.plt
    time_filename = "time-bin3dim.plt"
    time_fullpath = "%s/%s" % (path, time_filename)
    sh("cp ./templates/time-bin3dim.plt %s" % time_fullpath)
    sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("time-bin", folder, time_fullpath))

    # cria memory.plt
    # time_filename = "memory-bin3dim.plt"
    # time_fullpath = "%s/%s" % (path, time_filename)
    # sh("cp ./templates/memory.plt %s" % time_fullpath)
    # sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("memory", folder, time_fullpath))

if __name__ == "__main__":
    if len(sys.argv) == 6:
        d = sys.argv[1]
        erel = sys.argv[2]
        s = sys.argv[3]
        folder = sys.argv[4]   # './res'
        csv = sys.argv[5]
        plot_bin3dim(d, erel, s, folder, csv)
    else:
        print("Usage <dim> <epsilon_relative> <minsizes> <basefolder> <csvfile>")
        exit()