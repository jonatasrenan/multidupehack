#!/usr/bin/python3
import sys

import pandas as pd

from src.plot import plot
from src.plot_bin3dim import plot_bin3dim
from src.utils import sh

# parametros
if len(sys.argv) == 3:
    basefolder = sys.argv[1] # './res'
    csvfile = sys.argv[2]
else:
    print("Usage <basefolder> <csvfile>")
    exit()

sh("rm -rf %s" % basefolder)

df = pd.read_csv(csvfile)
for dim in df['_dim'].unique():
    df_dim = df.loc[df['_dim'] == int(dim)]
    for minsizes in df_dim['_minsizes'].unique():
        df_dim_minsizes = df_dim.loc[df['_minsizes'] == int(minsizes)]
        for erel in df_dim_minsizes['_epsilon_relative'].unique():
            plot(dim, erel, minsizes, basefolder, csvfile)
            if (dim == 3):
                plot_bin3dim(dim, erel, minsizes, basefolder, csvfile)

sh("./src/gnuplot_all.sh %s" % basefolder)
