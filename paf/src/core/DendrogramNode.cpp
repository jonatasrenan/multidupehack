// Copyright 2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "DendrogramNode.h"
#include "CandidateNode.h"
#include "Dendrogram.h"

vector<unsigned int> DendrogramNode::external2InternalDimensionOrder;
vector<vector<string>> DendrogramNode::ids2Labels;

string DendrogramNode::outputDimensionSeparator;
string DendrogramNode::outputElementSeparator;
string DendrogramNode::patternSizeSeparator;
string DendrogramNode::sizeSeparator;
string DendrogramNode::sizeAreaSeparator;
bool DendrogramNode::isSizePrinted;
bool DendrogramNode::isAreaPrinted;


DendrogramNode::DendrogramNode(const vector<vector<unsigned int>>& nSetParam): nSet(nSetParam), area(1), membershipSum(0), children(), candidates(), partners(), parent(NULL){
}

DendrogramNode::DendrogramNode(const CandidateNode* candidate): nSet(candidate->getNSet()), area(candidate->getArea()), membershipSum(candidate->getMembershipSum()), children(), candidates(), partners(), parent(NULL){
    addChild(*candidate->getLeftChild());
    addChild(*candidate->getRightChild());
    (*candidate->getLeftChild())->setParent(this);
    (*candidate->getRightChild())->setParent(this);
}

ostream& operator<<(ostream& out, const DendrogramNode& dendrogramNode){
    vector<vector<string>>::const_iterator ids2LabelsIt = DendrogramNode::ids2Labels.begin();

    for (const unsigned int internalDimensionId : DendrogramNode::external2InternalDimensionOrder){
        if (ids2LabelsIt != DendrogramNode::ids2Labels.begin()){
            out << DendrogramNode::outputDimensionSeparator;
        }

        const vector<unsigned int>& dimension = dendrogramNode.nSet[internalDimensionId];
        out << (*ids2LabelsIt)[dimension.front()];
        const vector<unsigned int>::const_iterator end = dimension.end();

        for (vector<unsigned int>::const_iterator elementIt = dimension.begin(); ++elementIt != end; ){
            out << DendrogramNode::outputElementSeparator << (*ids2LabelsIt)[*elementIt];
        }
        ++ids2LabelsIt;
    }

    if (DendrogramNode::isSizePrinted){
        out << DendrogramNode::patternSizeSeparator << dendrogramNode.nSet[DendrogramNode::external2InternalDimensionOrder.front()].size();
        for (const auto& internalDimensionIdIt : DendrogramNode::external2InternalDimensionOrder){
            out << DendrogramNode::sizeSeparator << dendrogramNode.nSet[internalDimensionIdIt].size();
        }
    }

    if (DendrogramNode::isAreaPrinted){
        out << DendrogramNode::sizeAreaSeparator << dendrogramNode.area;
    }

    return out;
}

void DendrogramNode::completeLeaf(){
    const vector<vector<unsigned int>> externalNSet = nSet;
    vector<vector<unsigned int>>::const_iterator externalNSetDimensionIt = externalNSet.begin();

    for (const unsigned int internalDimensionId : external2InternalDimensionOrder){
        vector<unsigned int>& dimension = nSet[internalDimensionId];
        dimension = *externalNSetDimensionIt++;
        sort(dimension.begin(), dimension.end());
        area *= dimension.size();
    }

    membershipSum = CandidateNode::getOneMinusSimilarityShift() * area - Dendrogram::data.noiseSum(nSet);
}

void DendrogramNode::addChild(DendrogramNode* new_child){
    children.push_back(new_child);
}

void DendrogramNode::setParent(DendrogramNode* new_parent){
    parent = new_parent;
}

vector<vector<unsigned int>> DendrogramNode::unionWith(const DendrogramNode& otherDendrogramNode) const{
    vector<vector<unsigned int>> unionNSet;
    unionNSet.reserve(nSet.size());
    vector<vector<unsigned int>>::const_iterator otherNSetIt = otherDendrogramNode.nSet.begin();

    for (const vector<unsigned int>& nSetDimension : nSet){
        vector<unsigned int> unionNSetDimension(nSetDimension.size() + otherNSetIt->size());
        unionNSetDimension.resize(set_union(nSetDimension.begin(), nSetDimension.end(), otherNSetIt->begin(), otherNSetIt->end(), unionNSetDimension.begin()) - unionNSetDimension.begin());
        unionNSet.push_back(unionNSetDimension);
        ++otherNSetIt;
    }

    return unionNSet;
}

const vector<DendrogramNode*>& DendrogramNode::getChildren() const{
    return children;
}

DendrogramNode* DendrogramNode::getParent(){
    return parent;
}

vector<vector<unsigned int>>& DendrogramNode::getNSet(){
    return nSet;
}

unsigned int DendrogramNode::getArea() const{
    return area;
}

double DendrogramNode::getMembershipSum() const{
    return membershipSum;
}

bool DendrogramNode::isSuperSet(DendrogramNode *node) const{
    for (unsigned int dimIt = 0; dimIt < nSet.size(); dimIt++){
        unordered_set<unsigned int> values;
        for (const auto& valIt : nSet[dimIt])
            values.insert(valIt);
        for (const auto& valIt : node->getNSet()[dimIt])
            if (values.find(valIt) == values.end())
                return false;
    }
    return true;
}

CandidateNode* DendrogramNode::topCandidate(){
    return *candidates.begin();
}

set<CandidateNode*, CandidateNode::Comparator>& DendrogramNode::getCandidates(){
    return candidates;
}

unordered_set<CandidateNode*>& DendrogramNode::getPartners(){
    return partners;
}

bool DendrogramNode::addCandidate(CandidateNode* new_candidate, unsigned int lim){
    bool ok = true;
    candidates.insert(new_candidate);

    if (candidates.size() > lim){
        auto it = --candidates.end();
        if (*it == new_candidate) ok = false;
        candidates.erase(it);
    }

    return ok;
}

void DendrogramNode::addPartner(CandidateNode* partner){
    partners.insert(partner);
}

void DendrogramNode::removeCandidate(CandidateNode* candidate){
    candidates.erase(candidate);
    partners.erase(candidate);
}

int DendrogramNode::nbOfCandidates() const{
    return candidates.size();
}

void DendrogramNode::setSeparator(const char* outputDimensionSeparatorParam, const char* outputElementSeparatorParam, const char* patternSizeSeparatorParam, const char* sizeSeparatorParam, const char* sizeAreaSeparatorParam){
    outputDimensionSeparator = outputDimensionSeparatorParam;
    outputElementSeparator = outputElementSeparatorParam;
    patternSizeSeparator = patternSizeSeparatorParam;
    sizeSeparator = sizeSeparatorParam;
    sizeAreaSeparator = sizeAreaSeparatorParam;
}

void DendrogramNode::setPrinted(bool isSizePrintedParam, bool isAreaPrintedParam){
    isSizePrinted = isAreaPrintedParam;
    isAreaPrinted = isAreaPrintedParam;
}

void DendrogramNode::setExternal2InternalDimensionOrder(vector<unsigned int>& external2InternalDimensionOrderParam){
    external2InternalDimensionOrder = external2InternalDimensionOrderParam;
}

void DendrogramNode::setIds2Labels(vector<vector<string>>& ids2LabelsParam){
    ids2Labels = ids2LabelsParam;
}
