// Copyright 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Loïc (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "Trie.h"

Trie::Trie(): hyperplanes()
{
}

Trie::Trie(const Trie& otherTrie): hyperplanes()
{
  copy(otherTrie);
}

Trie::Trie(Trie&& otherTrie): hyperplanes(std::move(otherTrie.hyperplanes))
{
}

Trie::Trie(const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd, const float densityThresholdParam): hyperplanes()
{
  cardinalityOfLastDimension = *(cardinalityEnd - 1);
  densityThreshold = densityThresholdParam * cardinalityOfLastDimension;
  init(cardinalityIt, cardinalityEnd);
}

Trie::Trie(const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd): hyperplanes()
{
  init(cardinalityIt, cardinalityEnd);
}

void Trie::init(const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd)
{
  const unsigned int cardinality = *cardinalityIt;
  hyperplanes.reserve(cardinality);
  if (cardinalityIt + 2 == cardinalityEnd)
    {
      for (unsigned int hyperplaneId = 0; hyperplaneId != cardinality; ++hyperplaneId)
	{
	  hyperplanes.push_back(new SparseFuzzyTube());
	}
      return;
    }
  const vector<unsigned int>::const_iterator nextCardinalityIt = cardinalityIt + 1;
  for (unsigned int hyperplaneId = 0; hyperplaneId != cardinality; ++hyperplaneId)
    {
      hyperplanes.push_back(new Trie(nextCardinalityIt, cardinalityEnd));
    }
}

Trie::~Trie()
{
  for (AbstractData* hyperplane : hyperplanes)
    {
      delete hyperplane;
    }
}

Trie* Trie::clone() const
{
  return new Trie(*this);
}

Trie& Trie::operator=(const Trie& otherTrie)
{
  copy(otherTrie);
  return *this;
}

Trie& Trie::operator=(Trie&& otherTrie)
{
  hyperplanes = std::move(otherTrie.hyperplanes);
  return *this;
}

ostream& operator<<(ostream& out, const Trie& trie)
{
  vector<unsigned int> prefix;
  trie.print(prefix, out);
  return out;
}

void Trie::copy(const Trie& otherTrie)
{
  hyperplanes.reserve(otherTrie.hyperplanes.size());
  for (const AbstractData* hyperplane : otherTrie.hyperplanes)
    {
      hyperplanes.push_back(hyperplane->clone());
    }
}

void Trie::print(vector<unsigned int>& prefix, ostream& out) const
{
  unsigned int hyperplaneId = 0;
  for (AbstractData* hyperplane : hyperplanes)
    {
      prefix.push_back(hyperplaneId++);
      hyperplane->print(prefix, out);
      prefix.pop_back();
    }
}

void Trie::setTuples(const vector<vector<unsigned int>>& tuples, const float membership)
{
  setTuples(tuples.begin(), 1 - membership);
}

const bool Trie::setTuples(const vector<vector<unsigned int>>::const_iterator dimensionIt, const float noise)
{
  const vector<vector<unsigned int>>::const_iterator nextDimensionIt = dimensionIt + 1;
  for (const unsigned int element : *dimensionIt)
    {
      AbstractData*& hyperplane = hyperplanes[element];
      if (hyperplane->setTuples(nextDimensionIt, noise))
	{
	  DenseFuzzyTube* newHyperplane = static_cast<SparseFuzzyTube*>(hyperplane)->getDenseRepresentation();
	  delete hyperplane;
	  hyperplane = newHyperplane;
	}
    }
  return false;
}

void Trie::turnCrisp()
{
  if (dynamic_cast<Trie*>(hyperplanes.front()))
    {
      for (AbstractData* hyperplane : hyperplanes)
      	{
      	  static_cast<Trie*>(hyperplane)->turnCrisp();
      	}
      return;
    }
  for (AbstractData*& hyperplane : hyperplanes)
    {
      AbstractData* newHyperplane = hyperplane->getCrispRepresentation();
      delete hyperplane;
      hyperplane = newHyperplane;
    }
}

void Trie::sortTubes()
{
  for (AbstractData* hyperplane : hyperplanes)
    {
      hyperplane->sortTubes();
    }
}

const float Trie::noiseSum(const vector<vector<unsigned int>>& nSet) const
{
  float noise = 0;
  const vector<vector<unsigned int>>::const_iterator dimensionIt = nSet.begin();
  const vector<vector<unsigned int>>::const_iterator nextDimensionIt = dimensionIt + 1;
  for (const unsigned int id : *dimensionIt)
    {
      noise += hyperplanes[id]->noiseSum(nextDimensionIt);
    }
  return noise;
}

const float Trie::noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const
{
  float noise = 0;
  const vector<vector<unsigned int>>::const_iterator nextDimensionIt = dimensionIt + 1;
  for (const unsigned int id : *dimensionIt)
    {
      noise += hyperplanes[id]->noiseSum(nextDimensionIt);
    }
  return noise;
}
