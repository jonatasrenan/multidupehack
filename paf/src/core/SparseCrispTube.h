// Copyright 2010,2011,2012,2013,2014,2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef SPARSE_CRISP_TUBE_H_
#define SPARSE_CRISP_TUBE_H_

#include <algorithm>

#include "AbstractData.h"

class SparseCrispTube final : public AbstractData
{
 public:
  SparseCrispTube(const vector<pair<unsigned int, float>>& tube);

  void print(vector<unsigned int>& prefix, ostream& out) const;
  
  void sortTubes();

  const float noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const;

 protected:
  vector<unsigned int> tube;

  SparseCrispTube* clone() const;
};

#endif /*SPARSE_CRISP_TUBE_H_*/
