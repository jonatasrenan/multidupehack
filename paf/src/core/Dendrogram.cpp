#include "Dendrogram.h"

Trie Dendrogram::data;
ofstream Dendrogram::outputFile;

#if defined TIME || defined DETAILED_TIME
steady_clock::time_point Dendrogram::overallBeginning;
#endif
#ifdef DETAILED_TIME
double Dendrogram::parsingDuration;
#endif

Dendrogram::Dendrogram() : total_data(0), total_square_data(0), total_size(0), nbOfParents(0), dendrogramNodes(), dendrogramFrontier(), bagOfCandidates(){
	total_data = 0; total_square_data = 0;
	total_size = 0;
}

Dendrogram::~Dendrogram(){
	for (list<DendrogramNode*>::iterator it = dendrogramNodes.begin(); it != dendrogramNodes.end(); it++){
		delete *it;
	}
}

void Dendrogram::parse(const char* patternFileName, const char* dataFileName, const double maximalNbOfCandidates, const char* inputDimensionSeparator, const char* inputElementSeparator, const float densityThreshold, const char* outputFileName){
#if defined TIME || defined DETAILED_TIME
    overallBeginning = steady_clock::now();
#endif

    vector<unsigned int> external2InternalDimensionOrder;
    vector<vector<string>> ids2Labels;

    try{
        // Parse patterns
        PatternFileReader patternFileReader(patternFileName, inputDimensionSeparator, inputElementSeparator);
        for (; !patternFileReader.eof(); patternFileReader.next()){
            dendrogramFrontier.push_back(new DendrogramNode(patternFileReader.pattern()));
        }

        if (!dendrogramFrontier.empty()){
            nbOfParents = maximalNbOfCandidates * 1000000;
            if (dendrogramFrontier.size() > nbOfParents && nbOfParents != 0){
                throw UsageException((lexical_cast<string>(dendrogramFrontier.size()) + " input patterns is more than the allowed " + lexical_cast<string>(maximalNbOfCandidates) + " million candidates!").c_str());
            }
            ids2Labels = patternFileReader.captureIds2Labels();

            // Order the dimensions by increasing cardinality
            vector<pair<unsigned int, unsigned int>> dimensions;
            dimensions.reserve(ids2Labels.size());
			total_size = 1;

            for (const vector<string>& labelsInDimension : ids2Labels){
				total_size *= labelsInDimension.size();
                dimensions.push_back(pair<unsigned int, unsigned int>(labelsInDimension.size(), dimensions.size()));
            }

            sort(dimensions.begin(), dimensions.end());
            vector<unsigned int> cardinalities;
            cardinalities.reserve(dimensions.size());
            external2InternalDimensionOrder.resize(dimensions.size());

            for (const pair<unsigned int, unsigned int>& dimension : dimensions){
                external2InternalDimensionOrder[dimension.second] = cardinalities.size();
                cardinalities.push_back(dimension.first);

            }

            // Initialize data
            data = Trie(cardinalities.begin(), cardinalities.end(), densityThreshold);

            // Parse tuples
            bool isCrisp = true;

            for (NoisyTupleFileReader noisyTupleFileReader(dataFileName, inputDimensionSeparator, inputElementSeparator, patternFileReader.captureLabels2Ids(), external2InternalDimensionOrder); !noisyTupleFileReader.eof(); noisyTupleFileReader.next()){
                total_data += noisyTupleFileReader.membership();
                total_square_data += noisyTupleFileReader.membership() * noisyTupleFileReader.membership();
                isCrisp = isCrisp && noisyTupleFileReader.membership() == 1;
                data.setTuples(noisyTupleFileReader.tuples(), noisyTupleFileReader.membership());
            }

            if (isCrisp){
                data.turnCrisp();
            }

            data.sortTubes();
        }

        outputFile.open(outputFileName);
        if (!outputFile){
            throw NoFileException(outputFileName);
        }

        DendrogramNode::setExternal2InternalDimensionOrder(external2InternalDimensionOrder);
    	DendrogramNode::setIds2Labels(ids2Labels);
    }
    catch (std::exception& e){
        for (const DendrogramNode* node : dendrogramFrontier){
            delete node;
        }
        rethrow_exception(current_exception());
    }

}

void Dendrogram::paf(const float shift, const double lambda_0, const bool top_dendrogram, const char* verboseOutputFileName, const char* matrizOutputFileName, const char* outputDimensionSeparatorParam, const char* outputElementSeparatorParam, const char* patternSizeSeparatorParam, const char* sizeSeparatorParam, const char* sizeAreaSeparatorParam, const bool isSizePrintedParam, const bool isAreaPrintedParam){
    DendrogramNode::setSeparator(outputDimensionSeparatorParam, outputElementSeparatorParam, patternSizeSeparatorParam, sizeSeparatorParam, sizeAreaSeparatorParam);
    DendrogramNode::setPrinted(isSizePrintedParam, isAreaPrintedParam);
    CandidateNode::setSimilarityShift(shift);
	CandidateNode::setLambda0((lambda_0 == -1 ? total_data / total_size : lambda_0));

#ifdef DETAILED_TIME
    parsingDuration = duration_cast<duration<double>>(steady_clock::now() - overallBeginning).count();
    steady_clock::time_point startingPoint = steady_clock::now();
#endif

    if (!dendrogramFrontier.empty()){
        // Complete leaf constructions
        for (DendrogramNode* leaf : dendrogramFrontier){
            leaf->completeLeaf();
        }

        if (nbOfParents == 0 || dendrogramFrontier.size() < 2)
            dendrogramNodes = std::move(dendrogramFrontier);
        else
            agglomerate();
    }

#ifdef DETAILED_TIME
    const double agglomerationDuration = duration_cast<duration<double>>(steady_clock::now() - startingPoint).count();
    startingPoint = steady_clock::now();
#endif

	if (strlen(verboseOutputFileName) > 0){
		ofstream vof;
		vof.open(verboseOutputFileName);

		map<DendrogramNode*, int> ids;
		int i = 0;
		for (DendrogramNode* node : dendrogramNodes){
			ids[node] = i++;
		}
		vof.precision(4);
		vof << "Id\tParent Id\tDensity\t\tArea\tPattern\n";
		for (DendrogramNode* node : dendrogramNodes){
			vof << ids[node] << "\t" << (node->getParent() == NULL ? -1 : ids[node->getParent()])
				<< "\t\t" << fixed << node->getMembershipSum() / node->getArea()
				<< "\t\t" << node->getArea()
				<< "\t" << *node << endl;
		}

		vof.close();
	}

	if (strlen(matrizOutputFileName) > 0){
		ofstream mout;
		mout.open(matrizOutputFileName);

		for (auto nodesIta = dendrogramNodes.begin(); nodesIta != dendrogramNodes.end(); nodesIta++){
			for (auto nodesItb = nodesIta; nodesItb != dendrogramNodes.end(); nodesItb++){
				if (nodesIta == nodesItb){
					mout << "0 ";
				}
				else{
					CandidateNode *c = new CandidateNode(nodesIta, nodesItb);
					c->computeExactlyQuadraticErrorVariation();
					mout.precision(10);
					mout << c->getNormalizedQuadraticError() << " ";
				}
			}
			mout << endl;
		}

		mout.close();
	}

	if (top_dendrogram){
		print(*dendrogramFrontier.begin(), "", true, true);
	}
	else{
		for (const DendrogramNode* node : dendrogramNodes){
			outputFile << *node << endl;
		}
	}
    outputFile.close();

#ifdef GNUPLOT
#ifdef TIME
    cout << duration_cast<duration<double>>(steady_clock::now() - overallBeginning).count();
#endif
#ifdef DETAILED_TIME
#ifdef TIME
    cout << '\t';
#endif
    cout << parsingDuration << '\t' << agglomerationDuration << '\t' << duration_cast<duration<double>>(steady_clock::now() - startingPoint).count();
#endif
#if defined TIME || defined DETAILED_TIME
    cout << endl;
#endif
#else
#ifdef TIME
    cout << "Total time: " << duration_cast<duration<double>>(steady_clock::now() - overallBeginning).count() << 's' << endl;
#endif
#ifdef DETAILED_TIME
    cout << "Parsing time: " << parsingDuration << 's' << endl << "Agglomeration time: " << agglomerationDuration << 's' << endl << "Selection time: " << duration_cast<duration<double>>(steady_clock::now() - startingPoint).count() << 's' << endl;
#endif
#endif
}

void Dendrogram::fillCandidatesList(list<DendrogramNode*>::iterator curr){

#ifdef DEBUG
    cout << "Filling candidates of dendrogram node: " << *curr << endl << **curr << endl;
#endif

    vector<CandidateNode*> bestCandidatesTemp;

	for (auto nodesIt = dendrogramFrontier.begin(); nodesIt != dendrogramFrontier.end(); nodesIt++){
		if (nodesIt == curr) continue;
		bestCandidatesTemp.push_back(new CandidateNode(curr, nodesIt));
	}
	sort(bestCandidatesTemp.begin(), bestCandidatesTemp.end(), CandidateNode::Comparator());

	int size = min(nbOfParents / dendrogramFrontier.size(), bestCandidatesTemp.size());

	for (int i = 0; i < size; i++){
		bestCandidatesTemp[i]->computeExactlyQuadraticErrorVariation();
		if ((*curr)->addCandidate(bestCandidatesTemp[i], size)){

			DendrogramNode* aux;
			if (*bestCandidatesTemp[i]->getLeftChild() == (*curr))
				aux = *bestCandidatesTemp[i]->getRightChild();
			else
				aux = *bestCandidatesTemp[i]->getLeftChild();

			aux->addPartner(bestCandidatesTemp[i]);
		}
	}

	for (unsigned int i = size; i < bestCandidatesTemp.size(); i++)
		delete bestCandidatesTemp[i];
}

void Dendrogram::agglomerate(){
#ifdef DEBUG
    cout << endl << "Dendrogram:" << endl << endl << "* " << dendrogramFrontier.size() << " leaves generating at most " << nbOfParents << " candidates each:" << endl;
#endif

    // Original candidate construction
    for (auto child1It = dendrogramFrontier.begin(); child1It != dendrogramFrontier.end(); ++child1It){

#ifdef DEBUG
        cout << *child1It << ':' << **child1It << endl << "  area: " << (*child1It)->getArea() << endl << "  membershipSum: " << (*child1It)->getMembershipSum() << endl << "  density: " << (*child1It)->getMembershipSum() / (*child1It)->getArea() << endl;
#endif

        dendrogramNodes.push_back(*child1It);
        fillCandidatesList(child1It);
        CandidateNode* aux = (*child1It)->topCandidate();
        bagOfCandidates.insert(aux);

#ifdef DEBUG
        cout << "Inserting candidate " << aux << endl;
#endif
    }

    // Hierarchical agglomeration
#ifdef DEBUG
    cout << endl << "* Agglomerates:" << endl;
#endif

    while (!bagOfCandidates.empty()){

    	CandidateNode* candidate = *bagOfCandidates.begin();
    	bagOfCandidates.erase(bagOfCandidates.begin());

#ifdef DEBUG
    	cout << "Candidate " << candidate << " with " << candidate->getQuadraticErrorVariation() << " of left child: " << *candidate->getLeftChild() << " right child: " << *candidate->getRightChild() << endl;
#endif

    	list<list<DendrogramNode*>::iterator> subsetsToRemove;
    	subsetsToRemove.push_back(candidate->getLeftChild());
    	subsetsToRemove.push_back(candidate->getRightChild());

        DendrogramNode* node = new DendrogramNode(candidate);

        dendrogramNodes.push_back(node);
        dendrogramFrontier.push_back(node);

        const list<DendrogramNode*>::iterator end = --dendrogramFrontier.end();
        for (list<DendrogramNode*>::iterator nodesIt = dendrogramFrontier.begin(); nodesIt != end; nodesIt++){

        	if (nodesIt == candidate->getLeftChild() || nodesIt == candidate->getRightChild())
        		continue;

        	if (node->isSuperSet(*nodesIt)){
        		node->addChild(*nodesIt);
        		(*nodesIt)->setParent(node);
        		subsetsToRemove.push_back(nodesIt);
        	}
        }

        for (auto subsIt : subsetsToRemove){
			vector<CandidateNode*> candidatesToRemove;
        	for (auto& it : (*subsIt)->getCandidates()){
        	    bagOfCandidates.erase(it);
				candidatesToRemove.push_back(it);
        	}
        	for (auto& it : (*subsIt)->getPartners()){
        	    bagOfCandidates.erase(it);
				candidatesToRemove.push_back(it);
        	}
			for (auto& it : candidatesToRemove){
				if (it == candidate) continue;
				delete it;
			}
			if ((*subsIt)->getCandidates().size() > 1){
				cerr << "Something wrong when deleting candidates" << endl;
				abort();
			}
			(*subsIt)->getCandidates().clear();
        }

        delete candidate;

        for (auto subsIt : subsetsToRemove){
        	dendrogramFrontier.erase(subsIt);
        }


        for (auto nodesIt = dendrogramFrontier.begin(); nodesIt != dendrogramFrontier.end(); nodesIt++){
        	if ((*nodesIt)->nbOfCandidates() == 0){
        		fillCandidatesList(nodesIt);
        	}
        	if ((*nodesIt)->nbOfCandidates() > 0){
        		CandidateNode* aux = (*nodesIt)->topCandidate();
        		bagOfCandidates.insert(aux);
        	}
        }

#ifdef DEBUG
        DendrogramNode* lastInsertion = *--dendrogramFrontier.end();
        cout << lastInsertion << ':' << *lastInsertion << endl << "  area: " << lastInsertion->getArea() << endl << "  membershipSum: " << lastInsertion->getMembershipSum() << endl << "  density: " << lastInsertion->getMembershipSum() / lastInsertion->getArea() << endl << "  top: " << lastInsertion->topCandidate() << endl << "  children:";
        for (const auto& childIt : lastInsertion->getChildren()){
            cout << ' ' << childIt;
        }
        cout << endl;
#endif
    }
}

int Dendrogram::cmpf(double a, double b){
	if (fabs(a-b) < 1e-8) return 0;
	if (a < b) return -1;
	return 1;
}

void Dendrogram::print(DendrogramNode *node, string backspace, bool isLastBrother, bool isRoot){
    string aux = "";
    if (!isRoot){
        outputFile << backspace+"|   " << endl << backspace << "+---";
        aux = (isLastBrother?"    ":"|   ");
    }
    outputFile << *node << endl;
    int i = 0;
    for (const auto &it : node->getChildren()){
        print(it, backspace+aux, ++i >= node->getChildren().size());
    }
}
