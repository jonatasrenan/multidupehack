#include <vector>
#include <set>
#include <fstream>
#include <iostream>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include "sysexits.h"

#include "NoFileException.h"
#include "IncorrectNbOfDimensionsException.h"

using namespace boost;

namespace po = boost::program_options;

typedef unsigned int uint;

template <typename x> using single_nset = vector< set<x> >;
template <typename x> using multiple_nsets = vector< vector< set<x> > >; 
template <typename x> using ntuples = set< vector<x> >;