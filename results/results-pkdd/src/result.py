from src.utils import validfile
import collections
import re

def get_error_from_file(filename):
    """
    :param filename:
    :return:
        "err_tempo": Estourou tempo
        "err_memoria": Estourou memória
        "err_limite": estourou número de padrões
        "": Não foi executado - erro na execução de pré processos
        "ok": Sem erro
    """
    try:
        f = open("%s" % filename, 'r')
        err = f.read()
        f.close()
    except Exception:
        err = ""

    if "Signal 124: Estourou tempo" in err:
        return "err_tempo"
    elif "Signal 137: Estourou memória" in err:
        return "err_memoria"
    elif "Signal 134: Abortado" in err:
        return "err_abortado"
    elif "Não executado: Configuração atual já obteve erro." in err:
        return 'err_configuracao'
    elif "Execução prévia com erro." in err:
        return ""
    else:
        return ""

def re_filter(r, l):
    a = [m.group(0) for m in (re.search(re.compile(r), p) for p in l) if m]
    if a:
        return a
    else:
        return ['']

def get_number_lines_of_file(filename):
    try:
        return sum(1 for line in open(filename))
    except Exception:
        return None


def get_correlation_from_file(filename):
    try:
        f = open(filename, 'r')
        c = float(f.read().strip())
        f.close()
    except Exception:
        c = None
    return c


def get_time_from_file(filename):

    try:
        f = open(filename, 'r')
        o = f.read()
        f.close()
    except Exception:
        return {}

    ol = [l.strip() for l in o.split('\n')[1:] if l is not ""]
    if len(ol) == 3:
        output = dict([l.split('\t') for l in ol])
        min =    output['user'].split('m')[0]
        sec =    output['user'].split('m')[1].split('.')[0]
        msec =   output['user'].split('m')[1].split('.')[1].split('s')[0]
        user = (int(min)*60) + int(sec) + (int(msec)/1000)
        return { "maximum_resident_size": "not defined", "average_resident_size": "not defined", "user": user}
    elif len(ol) == 0:
        return { "maximum_resident_size": "not defined", "average_resident_size": "not defined", "user": "not defined"}
    else:
        output = dict([l.split(': ') for l in ol])
        sec = output['User time (seconds)'].split('.')[0]
        msec = output['User time (seconds)'].split('.')[1]
        user = int(sec) + (int(msec) / 100)
        return {
            "maximum_resident_size": output["Maximum resident set size (kbytes)"],
            "average_resident_size": output["Average resident set size (kbytes)"],
            "user": user
        }

class Results(str):
    def depth1(self):
        return '_'.join(self.split('_')[:5])

    def depth2(self):
        return '_'.join(self.split('_')[:8])

    def depth3(self):
        return self

    def depth4(self):
        params = self.split('/')[-1].split('_')
        c = re_filter("([0-9-\.]+)c$", params)[0].replace('c', '')
        return "%s_%sc" % (self.depth1(), c)

    def result(self):
        result = self.get_status()
        result.update(self.get_parameters())
        result.update(self.get_runtimes_and_memory())
        result.update(self.get_number_of_patterns())
        result.update(self.get_correlations())
        return result

    def get_status(self):
        def get_execution_status(file):
            if validfile("%s.err" % file):
                return "error"
            if validfile("%s.std" % file):
                if validfile(file):
                    return "success"
                else:
                    return "success_empty_output"
            if validfile("%s.sh" % file):
                return "started"
            return "not-started"

        status =      {"%s"       % p : get_execution_status("%s_%s"     % (self.depth2(), p)) for p in process_depth2}
        status.update({"%s-error" % p : get_error_from_file("%s_%s.err"  % (self.depth2(), p)) for p in process_depth2})
        status.update({"%s"       % p : get_execution_status("%s_%s-sel" % (self.depth3(), p)) for p in process_depth3})
        status.update({"%s-error" % p : get_error_from_file("%s_%s.err"  % (self.depth3(), p)) for p in process_depth3})
        status.update({"%s"       % p : get_execution_status("%s_%s"     % (self.depth4(), p)) for p in process_depth4})
        status.update({"%s-error" % p : get_error_from_file("%s_%s.err"  % (self.depth4(), p)) for p in process_depth4})

        #return {"status%02d_%s" % p : status[p[1]] for p in enumerate(sorted(list(status)))}
        return {"status_%s" % k: v for k,v in status.items()}

    def get_parameters(self):
        params = self.split('/')[-1].split('_')
        beta = re_filter("([0-9-]+)beta$", params)[0].replace('beta', '')

        return {
            '_dim': re_filter("([0-9-\.]+)dim$", params)[0].replace('dim', ''),
            '_dimsizes': re_filter("([0-9-\.]+)dimsizes$", params)[0].replace('dimsizes', ''),
            '_patsizes': re_filter("([0-9-\.]+)patsizes$", params)[0].replace('patsizes', ''),
            '_correct_obs': beta.split('-')[0],
            # 'param05_incorrect_obs': beta.split('-')[1],
            '_test': re_filter("([0-9-\.]+)test$", params)[0].replace('test', ''),
            '_epsilon_relative': re_filter("([0-9-\.]+)erel$", params)[0].replace('erel', ''),
            '_epsilon': re_filter("([0-9-\.]+)e$", params)[0].replace('e', ''),
            '_minsizes': re_filter("([0-9-\.]+)s$", params)[0].replace('s', ''),
            '_coeff': re_filter("([0-9-\.]+)c$", params)[0].replace('c', ''),
            '_base': self
        }

    def get_runtimes_and_memory(self):
        result = {"%s" % p: get_time_from_file("%s_%s.time" % (self.depth2(), p)) for p in process_depth2}
        result.update({"%s" % p: get_time_from_file("%s_%s.time" % (self.depth2(), p)) for p in process_depth3})
        result.update({"%s" % p: get_time_from_file("%s_%s.time" % (self.depth4(), p)) for p in process_depth4})
        flat = dict(sorted(sum([[["%s_%s" % (k, l), b] for (l, b) in v.items()] for (k, v) in result.items()], [])))
        time = dict([("time_%s" % k, v) for k,v in flat.items() if "_user" in k])
        memory = dict([("mem_%s" % k, v) for k,v in flat.items() if "_user" not in k])
        # result = {"time%02d_%s" % p: time[p[1]] for p in enumerate(time)}
        # result.update({"mem%02d_%s" % p: memory[p[1]] for p in enumerate(memory)})
        result = time.copy()
        result.update(memory)
        return result

    def get_number_of_patterns(self):
        result = {"%s" % p : get_number_lines_of_file("%s_%s" % (self.depth2(), p)) for p in process_depth2}
        result.update({"%s" % p : get_number_lines_of_file("%s_%s-sel" % (self.depth3(), p)) for p in process_depth3})
        result.update({"%s" % p : get_number_lines_of_file("%s_%s" % (self.depth4(), p)) for p in process_depth4})
        # return {"quant%02d_%s" % p : result[p[1]] for p in enumerate(sorted(list(result)))}
        return {"quant_%s" % k: v for k,v in result.items()}

    def get_correlations(self):
        result = {"%s" % p : get_correlation_from_file("%s_%s-corr" % (self.depth2(), p)) for p in process_depth2}
        result.update({"%s" % p : get_correlation_from_file("%s_%s-corr" % (self.depth3(), p)) for p in process_depth3})
        result.update({"%s" % p : get_correlation_from_file("%s_%s-corr" % (self.depth4(), p)) for p in process_depth4})
        # return {"corr%02d_%s" % p : result[p[1]] for p in enumerate(sorted(list(result)))}

        # adiciona zero caso a correlação for nula
        # result.update(dict([(k, 0) for (k, v) in result.items() if re.match("corr[0-9]", k, flags=0) and v is None]))
        result.update(dict([(k, 0) for k, v in result.items() if v is None]))

        return {"corr_%s" % k: v for k,v in result.items()}
        # result = {"corr%02d_%s" % (p[0], p[1]): get_correlation_from_file("%s_%s-corr" % (self.depth2(), p[1]))
        #           for p in enumerate(process_depth2, 1)}
        # result.update({"corr%02d_%s" % (p[0], p[1]): get_correlation_from_file("%s_%s-corr" % (self.depth3(), p[1]))
        #                for p in enumerate(process_depth3, len(result) + 1)})
        # result.update({"corr%02d_%s" % (p[0], p[1]): get_correlation_from_file("%s_%s-corr" % (self.depth4(), p[1]))
        #                for p in enumerate(process_depth4, len(result) + 1)})
        # return result

process_depth2 = ['mdh', 'ilph']
process_depth3 = ['ilph-ilp', 'mdh-ilp', 'mdh-paf0', 'mdh-paf1', 'tribox', 'tribox-paf1']
process_depth4 = ['boxcluster']

def results(list):
    return [collections.OrderedDict(sorted(Results(test).result().items())) for test in list]
