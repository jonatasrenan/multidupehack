from src.utils import sh
from src.result import results
import csv
import sys

if len(sys.argv) == 3:
    folder = sys.argv[1]
    out = sys.argv[2]
else:
    print("Error: Result Folder and csv file parameters is required: \n tests.py <folder> <csv>")
    sys.exit()

get_tests = "find %s -name '*dim_*dimsizes_*patsizes_*beta_*test_*erel_*e_*s_*c_*' | cut -d '_' -f-9 | sort -u" % folder
all_results = results(sh(get_tests))

keys = sorted(list(set([item for sublist in list(map(lambda x: list(x.keys()), all_results)) for item in sublist])))
with open(out, 'w') as f:
    w = csv.DictWriter(f, keys)
    w.writeheader()
    w.writerows(all_results)
