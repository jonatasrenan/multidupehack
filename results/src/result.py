from src.utils import validfile
import collections
import re


def fget_status(f):
    if validfile("%s.err" % f):
        return "error"
    if validfile("%s.std" % f):
        if validfile(f):
            return "success"
        else:
            return "success_empty_output"
    if validfile("%s.sh" % f):
        if validfile(f):

            if 'sem saída' in open(f, 'r').read():
                return "success_empty_output"
            else:
                return "success"
        else:
            return "started"
    return "not-started"


def fget_error(f):
    """
    :param f:
    :return:
        "err_tempo": Estourou tempo
        "err_memoria": Estourou memória
        "err_limite": estourou número de padrões
        "": Não foi executado - erro na execução de pré processos
        "ok": Sem erro
    """
    try:
        f = open("%s" % f, 'r')
        err = f.read()
        f.close()
    except Exception:
        err = ""

    if "Signal 124: Estourou tempo" in err:
        return "err_tempo"
    elif "Signal 137: Estourou memória" in err:
        return "err_memoria"
    elif "Signal 134: Abortado" in err:
        return "err_abortado"
    elif "Não executado: Configuração atual já obteve erro." in err:
        return 'err_configuracao'
    elif "Execução prévia com erro." in err:
        return ""
    else:
        return ""


def fget_lines(filename):
    try:
        return sum(1 for line in open(filename))
    except Exception:
        return None


def fget_corr(filename):
    try:
        c = float(open(filename, 'r').read().strip())
    except Exception:
        c = None
    return c


def fget_time(filename):

    try:
        f = open(filename, 'r')
        o = f.read()
        f.close()
    except Exception:
        return {}

    ol = [l.strip() for l in o.split('\n')[1:] if l is not ""]
    if len(ol) == 3:
        output = dict([l.split('\t') for l in ol])
        min =    output['user'].split('m')[0]
        sec =    output['user'].split('m')[1].split('.')[0]
        msec =   output['user'].split('m')[1].split('.')[1].split('s')[0]
        user = (int(min)*60) + int(sec) + (int(msec)/1000)
        return { "maximum_resident_size": "not defined",
                 # "average_resident_size": "not defined",
                 "user": user}
    elif len(ol) == 0:
        return { "maximum_resident_size": "not defined",
                 # "average_resident_size": "not defined",
                "user": "not defined"}
    else:
        output = dict([l.split(': ') for l in ol])
        sec = output['User time (seconds)'].split('.')[0]
        msec = output['User time (seconds)'].split('.')[1]
        user = int(sec) + (int(msec) / 100)
        return {
            "maximum_resident_size": output["Maximum resident set size (kbytes)"],
            # "average_resident_size": output["Average resident set size (kbytes)"],
            "user": user
        }


def re_filter(r, l):
    a = [m.group(0) for m in (re.search(re.compile(r), p) for p in l) if m]
    if a:
        return a
    else:
        return ['']

class Results(str):
    def depth_test(self):
        return '_'.join(self.split('_')[:5])

    def depth_erel(self):
        return '_'.join(self.split('_')[:8])

    def depth_rank(self):
        d = "0.035"
        rank = "4"
        return "%s_%sd_%srank" % (self.depth_test(), d, rank)

    # def depth_mem(self):
    #     return self
    #
    # def depth_mem_coef(self):
    #     params = self.split('/')[-1].split('_')
    #     m = re_filter("([0-9-\.]+)m$", params)[0].replace('m', '')
    #     c = '2'
    #     return "%s_%sm_%sc" % (self.depth_erel_size(), m, c)

    def get_parameters(self):
        params = self.split('/')[-1].split('_')
        beta = re_filter("([0-9-]+)beta$", params)[0].replace('beta', '')

        return {
            '_base': self,
            '_dim': re_filter("([0-9-\.]+)dim$", params)[0].replace('dim', ''),
            '_dimsizes': re_filter("([0-9-\.]+)dimsizes$", params)[0].replace('dimsizes', ''),
            '_patsizes': re_filter("([0-9-\.]+)patsizes$", params)[0].replace('patsizes', ''),
            '_correct_obs': beta.split('-')[0],
            '_test': re_filter("([0-9-\.]+)test$", params)[0].replace('test', ''),
            '_epsilon_relative': re_filter("([0-9-\.]+)erel$", params)[0].replace('erel', ''),
            '_epsilon': re_filter("([0-9-\.]+)e$", params)[0].replace('e', ''),
            '_minsizes': re_filter("([0-9-\.]+)s$", params)[0].replace('s', ''),
            '_memory': re_filter("([0-9-\.]+)m$", params)[0].replace('m', ''),
            # '_coeff': '2'
            # '_coeff': re_filter("([0-9-\.]+)c$", params)[0].replace('c', '')
        }

    def result(self):
        result = self.get_status()
        result.update(self.get_errors())
        result.update(self.get_parameters())
        result.update(self.get_runtimes_and_memory())
        result.update(self.get_number_of_patterns())
        result.update(self.get_correlations())
        return result

    def get_status(self):
        d = self.depth_erel()
        p = [{'p': 'mdh', 'f': 'mdh'},
            {'p': 'mdhbin', 'f': 'mdhbin'},
            {'p': 'walk', 'f': 'walk'},
            {'p': 'walk-format', 'f': 'walk-format'},

            {'p': '1m_sqrtm', 'f': '1m_sqrtm'},
            {'p': '0.01m_sqrtm', 'f': '0.01m_sqrtm'},
            {'p': '0.0001m_sqrtm', 'f': '0.0001m_sqrtm'},
            {'p': '1m_concat-sqrtm-paf1', 'f': '1m_concat-sqrtm-paf1'},
            {'p': '0.01m_concat-sqrtm-paf1', 'f': '0.01m_concat-sqrtm-paf1'},
            {'p': '0.0001m_concat-sqrtm-paf1', 'f': '0.0001m_concat-sqrtm-paf1'},
            {'p': '1m_concat-sqrtmbin-paf1', 'f': '1m_concat-sqrtmbin-paf1'},
            {'p': '0.01m_concat-sqrtmbin-paf1', 'f': '0.01m_concat-sqrtmbin-paf1'},
            {'p': '0.0001m_concat-sqrtmbin-paf1', 'f': '0.0001m_concat-sqrtmbin-paf1'},

            {'p': '1m_sqrtmbin', 'f': '1m_2c_sqrtmbin-sel'},
            {'p': '0.01m_sqrtmbin', 'f': '0.01m_2c_sqrtmbin-sel'},
            {'p': '0.0001m_sqrtmbin', 'f': '0.0001m_2c_sqrtmbin-sel'},
            {'p': '1m_sqrtm-paf1', 'f': '1m_2c_sqrtm-paf1-sel'},
            {'p': '0.01m_sqrtm-paf1', 'f': '0.01m_2c_sqrtm-paf1-sel'},
            {'p': '0.0001m_sqrtm-paf1', 'f': '0.0001m_2c_sqrtm-paf1-sel'},
            {'p': '1m_sqrtmbin-paf1', 'f': '1m_2c_sqrtmbin-paf1-sel'},
            {'p': '0.01m_sqrtmbin-paf1', 'f': '0.01m_2c_sqrtmbin-paf1-sel'},
            {'p': '0.0001m_sqrtmbin-paf1', 'f': '0.0001m_2c_sqrtmbin-paf1-sel'}]

        status = {"status_" + l['p']: fget_status("%s_%s" % (d, l['f'])) for l in p}

        d = self.depth_rank()
        p = [{'p': 'dbtf', 'f': 'dbtf'},
            {'p': 'dbtf-format', 'f': 'dbtf-format'}]

        status .update({"status_" + l['p']: fget_status("%s_%s" % (d, l['f'])) for l in p})
        return status

        # status = {"%s" % p: fget_status("%s_%s" % (self.depth_erel_size(), p)) for p in erel_size}
        # status.update({"%s" % p: fget_status("%s_%s" % (self.depth_mem(), p)) for p in mem})
        # status.update({"%s" % p: fget_status("%s_%s-sel" % (self.depth_mem_coef(), p)) for p in mem_coef})
        # status.update({"%s" % p: fget_status("%s_%s" % (self.depth_density_rank(), p)) for p in density_rank})
        # return {"status_%s" % k: v for k, v in status.items()}

    def get_errors(self):
        # errors = {"%s" % p: fget_error("%s_%s.err" % (self.depth_erel(), p)) for p in erel_size}
        # errors.update({"%s" % p: fget_error("%s_%s.err" % (self.depth_mem(), p)) for p in mem})
        # errors.update({"%s" % p: fget_error("%s_%s-sel.err" % (self.depth_mem_coef(), p)) for p in mem_coef})
        # errors.update({"%s" % p: fget_error("%s_%s.err" % (self.depth_rank(), p)) for p in density_rank})
        # return {"error_%s" % k: v for k, v in errors.items()}

        d = self.depth_erel()
        p = [{'p': 'mdh', 'f': 'mdh.err'},
             {'p': 'mdhbin', 'f': 'mdhbin.err'},
             {'p': 'walk', 'f': 'walk.err'},
             {'p': 'walk-format', 'f': 'walk-format.err'},

             {'p': '1m_sqrtm', 'f': '1m_sqrtm.err'},
             {'p': '0.01m_sqrtm', 'f': '0.01m_sqrtm.err'},
             {'p': '0.0001m_sqrtm', 'f': '0.0001m_sqrtm.err'},
             {'p': '1m_concat-sqrtm-paf1', 'f': '1m_concat-sqrtm-paf1.err'},
             {'p': '0.01m_concat-sqrtm-paf1', 'f': '0.01m_concat-sqrtm-paf1.err'},
             {'p': '0.0001m_concat-sqrtm-paf1', 'f': '0.0001m_concat-sqrtm-paf1.err'},
             {'p': '1m_concat-sqrtmbin-paf1', 'f': '1m_concat-sqrtmbin-paf1.err'},
             {'p': '0.01m_concat-sqrtmbin-paf1', 'f': '0.01m_concat-sqrtmbin-paf1.err'},
             {'p': '0.0001m_concat-sqrtmbin-paf1', 'f': '0.0001m_concat-sqrtmbin-paf1.err'},

             {'p': '1m_sqrtmbin', 'f': '1m_2c_sqrtmbin-sel.err'},
             {'p': '0.01m_sqrtmbin', 'f': '0.01m_2c_sqrtmbin-sel.err'},
             {'p': '0.0001m_sqrtmbin', 'f': '0.0001m_2c_sqrtmbin-sel.err'},
             {'p': '1m_sqrtm-paf1', 'f': '1m_2c_sqrtm-paf1-sel.err'},
             {'p': '0.01m_sqrtm-paf1', 'f': '0.01m_2c_sqrtm-paf1-sel.err'},
             {'p': '0.0001m_sqrtm-paf1', 'f': '0.0001m_2c_sqrtm-paf1-sel.err'},
             {'p': '1m_sqrtmbin-paf1', 'f': '1m_2c_sqrtmbin-paf1-sel.err'},
             {'p': '0.01m_sqrtmbin-paf1', 'f': '0.01m_2c_sqrtmbin-paf1-sel.err'},
             {'p': '0.0001m_sqrtmbin-paf1', 'f': '0.0001m_2c_sqrtmbin-paf1-sel.err'}]

        errors = {"error_" + l['p']: fget_error("%s_%s" % (d, l['f'])) for l in p}

        d = self.depth_rank()
        p = [{'p': 'dbtf', 'f': 'dbtf.err'},
             {'p': 'dbtf-format', 'f': 'dbtf-format.err'}]

        errors.update({"error_" + l['p']: fget_error("%s_%s" % (d, l['f'])) for l in p})
        return errors

    def get_runtimes_and_memory(self):
        # result = {"%s" % p: fget_time("%s_%s.time" % (self.depth_erel(), p)) for p in erel_size}
        # result.update({"%s" % p: fget_time("%s_%s.time" % (self.depth_mem(), p)) for p in mem})
        # result.update({"%s" % p: fget_time("%s_%s.time" % (self.depth_mem_coef(), p)) for p in mem_coef})
        # result.update({"%s" % p: fget_time("%s_%s.time" % (self.depth_rank(), p)) for p in density_rank})
        d = self.depth_erel()
        p = [{'p': 'mdh', 'f': 'mdh.time'},
             {'p': 'mdhbin', 'f': 'mdhbin.time'},
             {'p': 'walk', 'f': 'walk.time'},
             {'p': 'walk-format', 'f': 'walk-format.time'},

             {'p': '1m_sqrtm', 'f': '1m_sqrtm.time'},
             {'p': '0.01m_sqrtm', 'f': '0.01m_sqrtm.time'},
             {'p': '0.0001m_sqrtm', 'f': '0.0001m_sqrtm.time'},
             {'p': '1m_concat-sqrtm-paf1', 'f': '1m_concat-sqrtm-paf1.time'},
             {'p': '0.01m_concat-sqrtm-paf1', 'f': '0.01m_concat-sqrtm-paf1.time'},
             {'p': '0.0001m_concat-sqrtm-paf1', 'f': '0.0001m_concat-sqrtm-paf1.time'},
             {'p': '1m_concat-sqrtmbin-paf1', 'f': '1m_concat-sqrtmbin-paf1.time'},
             {'p': '0.01m_concat-sqrtmbin-paf1', 'f': '0.01m_concat-sqrtmbin-paf1.time'},
             {'p': '0.0001m_concat-sqrtmbin-paf1', 'f': '0.0001m_concat-sqrtmbin-paf1.time'},

             {'p': '1m_sqrtmbin', 'f': '1m_2c_sqrtmbin-sel.time'},
             {'p': '0.01m_sqrtmbin', 'f': '0.01m_2c_sqrtmbin-sel.time'},
             {'p': '0.0001m_sqrtmbin', 'f': '0.0001m_2c_sqrtmbin-sel.time'},
             {'p': '1m_sqrtm-paf1', 'f': '1m_2c_sqrtm-paf1-sel.time'},
             {'p': '0.01m_sqrtm-paf1', 'f': '0.01m_2c_sqrtm-paf1-sel.time'},
             {'p': '0.0001m_sqrtm-paf1', 'f': '0.0001m_2c_sqrtm-paf1-sel.time'},
             {'p': '1m_sqrtmbin-paf1', 'f': '1m_2c_sqrtmbin-paf1-sel.time'},
             {'p': '0.01m_sqrtmbin-paf1', 'f': '0.01m_2c_sqrtmbin-paf1-sel.time'},
             {'p': '0.0001m_sqrtmbin-paf1', 'f': '0.0001m_2c_sqrtmbin-paf1-sel.time'}]

        results = {l['p']: fget_time("%s_%s" % (d, l['f'])) for l in p}

        d = self.depth_rank()
        p = [{'p': 'dbtf', 'f': 'dbtf.time'},
             {'p': 'dbtf-format', 'f': 'dbtf-format.time'}]

        results.update({l['p']: fget_time("%s_%s" % (d, l['f'])) for l in p})

        flat = dict(sorted(sum([[["%s_%s" % (k, l), b] for (l, b) in v.items()] for (k, v) in results.items()], [])))
        time = dict([("timeuser_%s" % k.replace('_user', ''), v) for k,v in flat.items() if "_user" in k])
        memory = dict([("memmax_%s" % k.replace('_maximum_resident_size',''), v) for k,v in flat.items() if "_user" not in k])
        result = time.copy()
        result.update(memory)
        return result

    def get_number_of_patterns(self):
        # result = {"%s" % p : fget_lines("%s_%s" % (self.depth_erel(), p)) for p in erel_size}
        # result.update({"%s" % p : fget_lines("%s_%s" % (self.depth_mem(), p)) for p in mem})
        # result.update({"%s" % p : fget_lines("%s_%s-sel" % (self.depth_mem_coef(), p)) for p in mem_coef})
        # result.update({"%s" % p: fget_lines("%s_%s" % (self.depth_rank(), p)) for p in density_rank})
        # return {"quant_%s" % k: v for k,v in result.items()}

        d = self.depth_erel()
        p = [{'p': 'mdh', 'f': 'mdh'},
             {'p': 'mdhbin', 'f': 'mdhbin'},
             {'p': 'walk', 'f': 'walk'},
             {'p': 'walk-format', 'f': 'walk-format'},

             {'p': '1m_sqrtm', 'f': '1m_sqrtm'},
             {'p': '0.01m_sqrtm', 'f': '0.01m_sqrtm'},
             {'p': '0.0001m_sqrtm', 'f': '0.0001m_sqrtm'},
             {'p': '1m_concat-sqrtm-paf1', 'f': '1m_concat-sqrtm-paf1'},
             {'p': '0.01m_concat-sqrtm-paf1', 'f': '0.01m_concat-sqrtm-paf1'},
             {'p': '0.0001m_concat-sqrtm-paf1', 'f': '0.0001m_concat-sqrtm-paf1'},
             {'p': '1m_concat-sqrtmbin-paf1', 'f': '1m_concat-sqrtmbin-paf1'},
             {'p': '0.01m_concat-sqrtmbin-paf1', 'f': '0.01m_concat-sqrtmbin-paf1'},
             {'p': '0.0001m_concat-sqrtmbin-paf1', 'f': '0.0001m_concat-sqrtmbin-paf1'},

             {'p': '1m_sqrtmbin', 'f': '1m_2c_sqrtmbin-sel'},
             {'p': '0.01m_sqrtmbin', 'f': '0.01m_2c_sqrtmbin-sel'},
             {'p': '0.0001m_sqrtmbin', 'f': '0.0001m_2c_sqrtmbin-sel'},
             {'p': '1m_sqrtm-paf1', 'f': '1m_2c_sqrtm-paf1-sel'},
             {'p': '0.01m_sqrtm-paf1', 'f': '0.01m_2c_sqrtm-paf1-sel'},
             {'p': '0.0001m_sqrtm-paf1', 'f': '0.0001m_2c_sqrtm-paf1-sel'},
             {'p': '1m_sqrtmbin-paf1', 'f': '1m_2c_sqrtmbin-paf1-sel'},
             {'p': '0.01m_sqrtmbin-paf1', 'f': '0.01m_2c_sqrtmbin-paf1-sel'},
             {'p': '0.0001m_sqrtmbin-paf1', 'f': '0.0001m_2c_sqrtmbin-paf1-sel'}]

        quant = {"quant_" + l['p']: fget_lines("%s_%s" % (d, l['f'])) for l in p}

        d = self.depth_rank()
        p = [#{'p': 'dbtf', 'f': 'dbtf'},
             {'p': 'dbtf-format', 'f': 'dbtf-format'}]

        quant.update({"quant_" + l['p']: fget_lines("%s_%s" % (d, l['f'])) for l in p})
        return quant

    def get_correlations(self):
        # result = {"%s" % p : fget_corr("%s_%s-corr" % (self.depth_erel(), p)) for p in erel_size}
        # result.update({"%s" % p : fget_corr("%s_%s-corr" % (self.depth_mem_coef(), p)) for p in mem})
        # result.update({"%s" % p : fget_corr("%s_%s-corr" % (self.depth_mem_coef(), p)) for p in mem_coef})
        # result.update({"%s" % p: fget_corr("%s_%s-corr" % (self.depth_rank(), p)) for p in density_rank})
        d = self.depth_erel()
        p = [{'p': 'mdh', 'f': 'mdh-corr'},
             {'p': 'mdhbin', 'f': 'mdhbin-corr'},
             #{'p': 'walk', 'f': 'walk-corr'},
             {'p': 'walk-format', 'f': 'walk-format-corr'},

             # {'p': '1m_sqrtm', 'f': '1m_sqrtm-corr'},
             # {'p': '0.01m_sqrtm', 'f': '0.01m_sqrtm-corr'},
             # {'p': '0.0001m_sqrtm', 'f': '0.0001m_sqrtm-corr'},
             # {'p': '1m_concat-sqrtm-paf1', 'f': '1m_concat-sqrtm-paf1-corr'},
             # {'p': '0.01m_concat-sqrtm-paf1', 'f': '0.01m_concat-sqrtm-paf1-corr'},
             # {'p': '0.0001m_concat-sqrtm-paf1', 'f': '0.0001m_concat-sqrtm-paf1-corr'},
             # {'p': '1m_concat-sqrtmbin-paf1', 'f': '1m_concat-sqrtmbin-paf1-corr'},
             # {'p': '0.01m_concat-sqrtmbin-paf1', 'f': '0.01m_concat-sqrtmbin-paf1-corr'},
             # {'p': '0.0001m_concat-sqrtmbin-paf1', 'f': '0.0001m_concat-sqrtmbin-paf1-corr'},

             {'p': '1m_sqrtmbin', 'f': '1m_2c_sqrtmbin-corr'},
             {'p': '0.01m_sqrtmbin', 'f': '0.01m_2c_sqrtmbin-corr'},
             {'p': '0.0001m_sqrtmbin', 'f': '0.0001m_2c_sqrtmbin-corr'},
             {'p': '1m_sqrtm-paf1', 'f': '1m_2c_sqrtm-paf1-corr'},
             {'p': '0.01m_sqrtm-paf1', 'f': '0.01m_2c_sqrtm-paf1-corr'},
             {'p': '0.0001m_sqrtm-paf1', 'f': '0.0001m_2c_sqrtm-paf1-corr'},
             {'p': '1m_sqrtmbin-paf1', 'f': '1m_2c_sqrtmbin-paf1-corr'},
             {'p': '0.01m_sqrtmbin-paf1', 'f': '0.01m_2c_sqrtmbin-paf1-corr'},
             {'p': '0.0001m_sqrtmbin-paf1', 'f': '0.0001m_2c_sqrtmbin-paf1-corr'}]

        corr = {l['p']: fget_corr("%s_%s" % (d, l['f'])) for l in p}

        d = self.depth_rank()
        p = [#{'p': 'dbtf', 'f': 'dbtf-corr'},
             {'p': 'dbtf-format', 'f': 'dbtf-format-corr'}]

        corr.update({l['p']: fget_corr("%s_%s" % (d, l['f'])) for l in p})

        # adiciona zero caso a correlação for nula
        corr.update(dict([(k, 0) for k, v in corr.items() if v is None]))
        return {"corr_%s" % k: v for k, v in corr.items()}

def results(list):
    return [collections.OrderedDict(sorted(Results(test).result().items())) for test in list]
