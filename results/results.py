from src.utils import sh
from src.result import results
import csv
import sys

if len(sys.argv) == 3:
    folder = sys.argv[1]
    out = sys.argv[2]
else:
    print("Error: Result Folder and csv file parameters is required: \n tests.py <folder> <csv>")
    sys.exit()

tests = sh("find %s -name '*test_*erel_*e_*s_*m' | grep -vP random$ | cut -d '_' -f-8 | sort -u" % folder)

all_results = results(tests)

all_columns = all_results[0].keys()

# empty_columns = []
# for col in all_columns:
#     for l in all_results:
#         if l[col] != 0:
#             if col not in not_empty_columns:
#                 not_empty_columns.append(col)
#
# columns_to_remove = all_columns - not_empty_columns
#
# for l in all_results:
#     for c in columns_to_remove:
#         del l[c]

keys = sorted(list(set([item for sublist in list(map(lambda x: list(x.keys()), all_results)) for item in sublist])))
with open(out, 'w') as f:
    w = csv.DictWriter(f, keys)
    w.writeheader()
    w.writerows(all_results)
