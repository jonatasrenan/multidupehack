#include <vector>
#include <set>
#include <fstream>
#include <iostream>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include "sysexits.h"

#include "NoFileException.h"
#include "IncorrectNbOfDimensionsException.h"

#include "Node.h"

using namespace boost;

namespace po = boost::program_options;
