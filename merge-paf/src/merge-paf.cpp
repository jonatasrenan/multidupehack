#include "merge-paf.h"

set<pattern<string>> selected;
map<pattern<string>, int> ord;
map<int, Node> idToNode;
vector<Node> ans;

vector<pattern<string>> readPatterns(istream& input, int sizes) throw (IncorrectNbOfDimensionsException){
	vector<pattern<string>> nSets;
	char_separator<char> dimensionSeparator(" ");
	char_separator<char> valueSeparator(",");

	while (input.good()){
		pattern<string> nSet;
		nSet.reserve(sizes);

		string nSetString;
		getline(input, nSetString);

		tokenizer<char_separator<char> > dimensions(nSetString, dimensionSeparator);
		tokenizer<char_separator<char> >::iterator dimensionIt = dimensions.begin();

		if (dimensionIt != dimensions.end()){

			for (int i = 0; i < sizes; i++){
				if (dimensionIt == dimensions.end()){
					throw IncorrectNbOfDimensionsException(nSetString, sizes);
				}

				set<string> dimension;
				tokenizer<char_separator<char> > values(*dimensionIt, valueSeparator);
				for (const auto& valueIt : values){
					const string value = lexical_cast<string>(valueIt.c_str());
					dimension.insert(value);
				}

				nSet.push_back(dimension);
				++dimensionIt;
			}

			if (dimensionIt != dimensions.end()){
				throw IncorrectNbOfDimensionsException(nSetString, sizes);
			}
			nSets.push_back(nSet);
		}
	}
	return nSets;
}

vector<Node> readDendrogram(istream& input) throw (IncorrectNbOfDimensionsException){
	vector<Node> nodes;
	char_separator<char> attributeSeparator("\t");
	char_separator<char> dimensionSeparator(" ");
	char_separator<char> valueSeparator(",");

	string attrString;
	getline(input, attrString);

	while (input.good()){
		int id, parent, area;
		double density;
		pattern<string> p;

		getline(input, attrString);

		if (attrString == "")
			continue;

		tokenizer<char_separator<char> > attr(attrString, attributeSeparator);
		tokenizer<char_separator<char> >::iterator attrIt = attr.begin();

		if (attrIt != attr.end()){

			id = lexical_cast<int>(attrIt->c_str());
			attrIt++;
			parent = lexical_cast<int>(attrIt->c_str());
			attrIt++;
			density = lexical_cast<double>(attrIt->c_str());
			attrIt++;
			area = lexical_cast<int>(attrIt->c_str());
			attrIt++;

			tokenizer<char_separator<char> > dimensions(*attrIt, dimensionSeparator);

			for (auto& dimensionIt : dimensions){
				tokenizer<char_separator<char> > values(dimensionIt, valueSeparator);
				set<string> dim;
				for (const auto& valueIt : values){
					const string value = lexical_cast<string>(valueIt.c_str());
					dim.insert(value);
				}

				p.push_back(dim);
			}
		}

		nodes.push_back(Node(id, parent, area, density, p));
	}
	return nodes;
}

int countNodesAndSelect(Node &ac, int parent = -1){
	int total = 0;
	if (selected.find(ac.getPattern()) != selected.end()){
		ac.setParent(parent);
		parent = ac.getId();
		ans[ord[ac.getPattern()]] = ac;
		selected.erase(ac.getPattern());
		total++;
	}
	for (auto& ch : ac.getChildren()){
		total += countNodesAndSelect(idToNode[ch], parent);
	}
	return total;
}

int main(int argc, char* argv[]){

	bool VERBOSE = false;

	po::options_description generic("Generic Options");
	generic.add_options()
		("extracted-dendrogram-file", po::value<string>(), "extracted dendrogram file (verbose output from paf)")
		("selected-patterns-file", po::value<string>(), "selected patterns file (output from forward-selection)");

	po::options_description opts("Options");
	opts.add_options()
		("help", "produce help message")
		("verbose,v", "write similarity in a human friendly interface");

	po::options_description all;
	all.add(generic).add(opts);

	po::positional_options_description mandatory;
	mandatory.add("extracted-dendrogram-file", 1);
	mandatory.add("selected-patterns-file", 1);
	mandatory.add("help", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(all).positional(mandatory).run(), vm);
	po::notify(vm);

	if (vm.count("help") || !vm.count("extracted-dendrogram-file") || !vm.count("selected-patterns-file")){
		cout << "Usage: merge-paf [Options] extracted-dendrogram-file selected-patterns-file" << endl;
		cout << generic << opts;
		return 1;
	}

	string urlfile1 = "", urlfile2 = "";
	if (vm.count("extracted-dendrogram-file")){
		urlfile1 = vm["extracted-dendrogram-file"].as< string >();
	}

	if (vm.count("selected-patterns-file")){
		urlfile2 = vm["selected-patterns-file"].as< string >();
	}

	if (vm.count("verbose")){
		VERBOSE = true;
	}

	ifstream dendrogram_file(urlfile1.c_str());
	if (dendrogram_file.fail()){
		cerr << NoFileException(urlfile1.c_str()).what() << endl;
		return EX_IOERR;
	}

	ifstream selected_file(urlfile2.c_str());
	if (selected_file.fail()){
		cerr << NoFileException(urlfile2.c_str()).what() << endl;
		return EX_IOERR;
	}

	vector<Node> nodes;
	vector<pattern<string>> sel;

	try{
		nodes = readDendrogram(dendrogram_file);
		sel = readPatterns(selected_file, nodes[0].getPattern().size());
	}
	catch (IncorrectNbOfDimensionsException &e){
		cout << e.what() << endl;
		return EX_IOERR;
	}

	Node *root;

	for (auto& p : sel){
		selected.insert(p);
		ord[p] = ord.size()-1;
	}
	for (auto& n : nodes){
		idToNode[n.getId()] = n;
	}
	for (auto& n : nodes){
		if (n.getParent() != -1){
			idToNode[n.getParent()].addChild(n.getId());
		} else {
			root = &idToNode[n.getId()];
		}
	}

	ans.resize(sel.size());

	cout << "Id\tParent Id\tDensity\t\tArea\tPattern\n";
	int total = countNodesAndSelect(*root);

	if (total < ans.size()){
		cerr << "Some selected patterns are not in extracted patterns file\n";
		abort();
	}

	for (auto& p : ans){
		cout << p << endl;
	}

	return EX_OK;
}
