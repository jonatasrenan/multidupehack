#include "Node.h"

Node::Node(int pid, int pparent, int parea, double pdensity, pattern<string> pp)
        : id(pid), parent(pparent), area(parea), density(pdensity), p(pp) {}

Node::Node(){}

void Node::addChild(int c){
    childs.push_back(c);
}

void Node::setParent(int c){
    parent = c;
}

const vector<int>& Node::getChildren() const{
    return childs;
}

const pattern<string>& Node::getPattern() const{
    return p;
}

int Node::getId() const{
    return id;
}

int Node::getParent() const{
    return parent;
}

ostream& operator<<(ostream& out, const Node& a){
    out << a.id << "\t" << a.parent << "\t\t" << a.density << "\t\t" << a.area << "\t";
    for (int i = 0; i < a.p.size(); i++){
        auto vlrIt = a.p[i].begin();
        for (; vlrIt != a.p[i].end(); vlrIt++){
            out << *vlrIt;
            auto nxt = vlrIt;
            nxt++;
            if (nxt != a.p[i].end()) out << ",";
        }
        if (i < a.p.size()-1) out << " ";
    }
    return out;
}

bool Node::operator==(const Node &a) const{
    for (int i = 0; i < p.size(); i++){
        if (p[i] != a.getPattern()[i])
            return false;
    }

    return true;
}
