#include <iostream>
#include <vector>
#include <set>
using namespace std;

template <typename x> using pattern = vector< set<x> >;

class Node{
protected:
    int id, parent, area;
    double density;
    pattern<string> p;
    vector<int> childs;
public:

    Node(int pid, int pparent, int parea, double pdensity, pattern<string> pp);
    Node();

    void addChild(int c);
    void setParent(int c);

    const vector<int>& getChildren() const;
    const pattern<string>& getPattern() const;
    int getId() const;
    int getParent() const;

    friend ostream& operator<<(ostream &out, const Node &a);
    bool operator==(const Node &a) const;



};
