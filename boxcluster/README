*** DESCRIPTION ***

paf selects among a set of patterns, which hold in an uncertain
tensor, those that best explain the tensor. With option --ha, it first
hierarchically agglomerates the given patterns and then selects the
agglomerates that best explain the tensor.


*** RETURN VALUES ***

paf returns values which can be used when called from a script. They
are conformed to sysexit.h.
* 0 is returned when paf successfully terminates.
* 64 is returned when paf was incorrectly called.
* 65 is returned when an input data line is not properly formatted.
* 74 is returned when data could not be read or written on the disk.


*** GENERAL ***

* paf called with no option or with option --help (or -h) displays a
reminder of the options.
* paf --version (or simply paf -V) displays version information.


*** OPTIONS ***

Any option passed to paf may either be specified on the command line
or in an option file. If an option is present both in the option file
and on the command line, the latter is used.

The option file can be specified through option --opt. When omitted, a
file named as the input pattern file + ".opt" is supposed to be the
option file related to the input pattern file. For example, if the
input pattern file name is "patterns.txt" and "patterns.txt.opt"
exists, it is supposed to be the related option file.

The options have the same name in the option file as on the command
line. Only long names can be used though. Arguments passed to an
option are separated from the name of the option by "=". As an
example, these lines may constitute an option file:
ids = ": "
ha = 1.5
out = res/agglomerates.txt


*** INPUT DATA ***

The name of the file containing the input patterns must be passed as
an argument to paf.

Every line in that file must be either empty (it is ignored) or
contain n fields: the n dimensions of the pattern. Several different
separators may be used to separate the fields and the same goes for
the separations of the elements in a dimension field. The elements can
be any string (as far as they do not include any of the
separators!). Let us take an example. This line stands for the pattern
({01-09-2007, 02-09-2007}, {Thomas, Harry, Max}, {basketball,
gymnastics}) and could be part of an input pattern file:
01-09-2007,02-09-2007: Thomas,Harry,Max basketball,gymnastics

There are 3 dimensions here: the first one gathers dates, the second
pupils and the last one sports. The fields are separated by the
characters ":" or/and " ". The fact that two dimensions are separated
by several separators (in our example both ":" and " " separate the
dates from the pupils) does not raise any trouble. In this pattern,
the elements in a same dimension are always separated by ",".

To be properly understood, one must indicate to paf the characters
that can separate dimensions and those that can separate elements in a
dimension. Otherwise the defaults are used: " " to separate dimensions
and "," to separate elements. The related options are --ids for Input
Dimension Separators and --ies for Input Element Separators. In the
previous example, assuming the patterns are listed in a file named
"patterns", paf is to be called as follows:
$ paf --ids ": " -f tensor patterns
There is, here, no need to specify the option --ies since the default
value is correct. Let us now discuss the two mandatory options that
are used in the command above.

The file defining the uncertain tensor in which the patterns hold must
be given through option --file (-f). The file format is similar to
that of the patterns but every non-empty line must have one additional
field, the last one, containing a float number between 0 and 1. It is
the membership degree of any n-tuple that can be made by taking one
element in each of the n first fields. For example, the following line
stands for the 3-tuples (01-09-2007, Thomas, basketball) and
(01-09-2007, Harry, basketball) that both have the membership degree
0.9:
01-09-2007: Thomas,Harry basketball 0.9

The separator defined with --ids and --ies for the patterns are used
as well for the uncertain tensor. The n-tuples that are left undefined
are associated with null membership degrees. If the membership degree
of an n-tuple is defined several times in the file, only the last
definition holds.

Back to the example given earlier, the following call of paf selects
patterns among those listed in the file named "patterns", these
patterns hold in the uncertain tensor specified through the file named
"tensor", strings made of ":" and/or " " separate the dimensions in
both files, and "," (the default value) separates the elements in a
dimension:
$ paf --ids ": " -f tensor patterns


*** OUTPUT DATA ***

The output data looks like the input pattern data. Each line is a
selected pattern (a selected agglomerate if --ha is used).

Option --out (-o) is used to set the output file name. When omitted,
the output file name is set from the input pattern file name. It is
the input pattern file name + ".out". For example, if the input
pattern file name is "patterns.txt", the default output file name is
"patterns.txt.out".

The dimensions are separated by a string specified through option
--ods (by default " "). The elements are separated by a string
specified through option --oes (by default ",").

paf can append to each output pattern, information describing it. When
called with option --ps, it prints the number of elements in each
dimension. When called with option --pa, it prints the area of the
pattern. The separators can be specified:
* --css sets the string separating the pattern from the number of
elements in each dimension (by default " : ")
* --ss, used in conjunction with --css, sets the string separating two
sizes (by default " ")
* --sas sets the string separating the pattern or the sizes (when used
in conjunction with --css) from the number of elements in each
dimension (by default " : ")


*** AGGLOMERATION ***

Option --ha triggers a hierarchical agglomeration of the input
patterns. The patterns that best explain the tensor are then selected
among all agglomerates, including all the input patterns. Option --ha
must be given in argument a strictly positive double float. It stands
for the maximal number, in million, of candidate agglomerates to
consider. Unless there are only a few patterns to agglomerate (in
which case all candidates will be considered), the memory requirements
linearly grow with that number.


*** SIMILARITY SHIFT ***

Option --shift must be given a float number in argument: a shift of
every membership degree. It is 0 by default. Smaller (resp. larger)
values lead to favoring larger (resp. smaller) patterns.


*** INPUT DATA STORAGE ***

paf stores the uncertain tensor in a hybrid way: part of the n-tuples
can be stored in a sparse structure (when most of them have a null
membership degree) and part of them in a dense structure.

Option --density (-d) must be given a float number in argument. A
value between 0 and 1 makes sense. It defines a threshold at which a
denser structure is preferred. It is 0 by default, i.e., dense
structures are always used. At 1, space requirements are minimized. A
sparser storage can leads to improved performance if the uncertain
tensor is indeed sparse and cache misses are frequent with dense
structures (with constant access times).


*** BUGS ***

Report bugs to <lcerf@dcc.ufmg.br>.


*** COPYRIGHT ***

Copyright 2007-2016 Loïc Cerf (lcerf@dcc.ufmg.br) This is free
software. You may redistribute copies of it under the terms of the GNU
General Public License <http://www.gnu.org/licenses/gpl.html>. There
is NO WARRANTY, to the extent permitted by law.