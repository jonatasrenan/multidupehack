#ifndef BOXCLUSTER_H_
#define BOXCLUSTER_H_

#include <unordered_map>
#include <queue>

#include "../utilities/UsageException.h"
#include "../utilities/vector_hash.h"
#include "NoisyTupleFileReader.h"
#include "Pattern.h"
#include "DataTrie.h"
#include "Trie.h"
#include "SelectionNode.h"


class Boxcluster{
public:
	Boxcluster();
	~Boxcluster();

	static int cmpf(double a, double b = 0);

	void parse(const double threshold, const char* dataFileName, const char* inputDimensionSeparator, const char* inputElementSeparator, const char* outputFileName);
	void clustering(int selection_mode, const char* outputDimensionSeparator, const char* outputElementSeparator, const char* patternSizeSeparator, const char* sizeSeparator, const char* sizeAreaSeparator, const bool isSizePrinted, const bool isAreaPrinted);

	vector<Pattern*> patterns;

	static DataTrie<int> data;
	static ofstream outputFile;

protected:

	double total_data, total_square_data;
	unsigned int total_size, n;
	vector<unsigned int> cardinalities;

	vector<Pattern*> getInitialFragments();
	Pattern* expand(Pattern* fragment);
	vector<Pattern*> select(vector<Pattern*>& patterns);
	void generateAllFragments(vector<vector<unsigned int>> &nSet, vector<unsigned int> &tuple, vector<Pattern*> *patterns, int id = 0);

};

#endif  /* BOXCLUSTER_H_ */
