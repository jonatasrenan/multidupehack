// Copyright 2010,2011,2012,2013,2014,2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "SparseFuzzyTube.h"

const bool smallerId(const pair<unsigned int, float>& entry, const pair<unsigned int, float>& otherEntry)
{
  return entry.first < otherEntry.first;
}

SparseFuzzyTube::SparseFuzzyTube() : tube()
{
}

SparseFuzzyTube* SparseFuzzyTube::clone() const
{
  return new SparseFuzzyTube(*this);
}

void SparseFuzzyTube::print(vector<unsigned int>& prefix, ostream& out) const
{
  for (const pair<unsigned int, unsigned int>& hyperplane : tube)
    {
      for (const unsigned int id : prefix)
	{
	  out << id << ' ';
	}
      out << hyperplane.first << ' ' << 1 - hyperplane.second << endl;
    }
}

DenseFuzzyTube* SparseFuzzyTube::getDenseRepresentation() const
{
  return new DenseFuzzyTube(tube);
}

AbstractData* SparseFuzzyTube::getCrispRepresentation() const
{
  if (tube.size() < densityThreshold / sizeof(unsigned int) / 8)
    {
      return new SparseCrispTube(tube);
    }
  return new DenseCrispTube(tube);
}

const bool SparseFuzzyTube::setTuples(const vector<vector<unsigned int>>::const_iterator dimensionIt, const float noise)
{
  for (const unsigned int element : *dimensionIt)
    {
      tube.push_back(pair<unsigned int, float>(element, noise));
    }
  return tube.size() >= densityThreshold / 2;
}

void SparseFuzzyTube::sortTubes()
{
  // In case of duplicates, the last insertion is kept
  map<unsigned int, float> sortedTube;
  for (const pair<unsigned int, float>& entry : tube)
    {
      sortedTube[entry.first] = entry.second;
    }
  tube = vector<pair<unsigned int, float>>(sortedTube.begin(), sortedTube.end());
}

const float SparseFuzzyTube::noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const
{
  float noise = 0;
  const vector<pair<unsigned int, float>>::const_iterator tubeEnd = tube.end();
  vector<pair<unsigned int, float>>::const_iterator tubeBegin = tube.begin();
  for (const unsigned int id : *dimensionIt)
    {
      tubeBegin = lower_bound(tubeBegin, tubeEnd, pair<unsigned int, float>(id, 0));
      if (tubeBegin != tubeEnd && tubeBegin->first == id)
	{
	  noise += tubeBegin->second;
	}
      else
	{
	  noise += 1;
	}
    }
  return noise;
}
