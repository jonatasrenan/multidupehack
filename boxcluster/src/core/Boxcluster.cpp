#include "Boxcluster.h"

DataTrie<int> Boxcluster::data(-1);
ofstream Boxcluster::outputFile;


Boxcluster::Boxcluster() : total_data(0), total_square_data(0), total_size(0){
	total_data = 0; total_square_data = 0;
	total_size = 0;
}

Boxcluster::~Boxcluster(){
}

void Boxcluster::parse(const double threshold, const char* dataFileName, const char* inputDimensionSeparator, const char* inputElementSeparator, const char* outputFileName){
	#if defined TIME || defined DETAILED_TIME
	overallBeginning = steady_clock::now();
	#endif

	try{
		// Initialize data
		NoisyTupleFileReader noisyTupleFileReader(dataFileName, inputDimensionSeparator, inputElementSeparator);

		for (; !noisyTupleFileReader.eof(); noisyTupleFileReader.next()){
			double tensor = noisyTupleFileReader.membership();
			unsigned int membership = tensor >= threshold;
			total_data += membership;
			total_size++;
			data.addNSet(noisyTupleFileReader.tuples(), membership);
		}
		total_square_data = total_data;

		vector<vector<string>> ids2Labels = noisyTupleFileReader.getIds2Labels();
		for (const auto& ids2LabelsIt : ids2Labels)
			cardinalities.push_back(ids2LabelsIt.size());
		n = ids2Labels.size();

		//data.turnCrisp();
		//data.sortTubes();

		outputFile.open(outputFileName);
		if (!outputFile){
			throw NoFileException(outputFileName);
		}

		Pattern::setIds2Labels(ids2Labels);
	}
	catch (std::exception& e){
		rethrow_exception(current_exception());
	}

}

void Boxcluster::clustering(int selection_mode, const char* outputDimensionSeparatorParam, const char* outputElementSeparatorParam, const char* patternSizeSeparatorParam, const char* sizeSeparatorParam, const char* sizeAreaSeparatorParam, const bool isSizePrintedParam, const bool isAreaPrintedParam){
	Pattern::setSeparator(outputDimensionSeparatorParam, outputElementSeparatorParam, patternSizeSeparatorParam, sizeSeparatorParam, sizeAreaSeparatorParam);
	Pattern::setPrinted(isSizePrintedParam, isAreaPrintedParam);
	SelectionNode::setMode(selection_mode);

	vector<Pattern*> fragments = getInitialFragments();

	list<Pattern*> answer;
	for (const auto& f : fragments){
		Pattern* p = expand(f);

#ifdef DEBUG
		cout << "fragment: " << endl << *f << endl;
		cout << "expanded: " << endl << *p << endl;
#endif

		bool ok = p->getArea() > f->getArea();
		vector<list<Pattern*>::const_iterator> del;
		for (auto patIt = answer.begin(); patIt != answer.end() && ok; patIt++){
			if ((*patIt)->isSuperPattern(*p)){
				ok = false;
				break;
			}
			if (p->isSuperPattern(**patIt)){
				del.push_back(patIt);
			}
		}

		if (ok){
			answer.push_back(p);
		}
		else{
			delete p;
		}

		for (int i = 0; i < del.size(); i++)
			answer.erase(del[i]);
	}

	vector<Pattern*> aux;
	for (const auto &it : answer)
		aux.push_back(it);
	vector<Pattern*> selected_patters = select(aux);

	for (const auto &p : selected_patters){
		outputFile << *p << endl;
	}

	for (const auto &p : answer){
		delete p;
	}

	for (const auto &f : fragments){
		delete f;
	}

	outputFile.close();
}

vector<Pattern*> Boxcluster::getInitialFragments(){
	vector<Pattern*> answer;
	vector<vector<unsigned int>> nSet(n);
	vector<unsigned int> tuple(n);
	generateAllFragments(nSet, tuple, &answer);
	return answer;
}

void Boxcluster::generateAllFragments(vector<vector<unsigned int>> &nSet, vector<unsigned int> &tuple, vector<Pattern*> *patterns, int id){
	if (id == nSet.size()-1){
		for (int i = 0; i < cardinalities[id]; i++){
			tuple[id] = i;
			unsigned int membership = data.getTuple(tuple.begin(), tuple.end());
			if (membership == 1)
				nSet[id].push_back(i);
		}
		if (nSet[id].size() > 0){
			Pattern *p = new Pattern(nSet);
			p->setMembershipSum(p->getArea());
			patterns->push_back(p);
		}
		nSet[id].clear();
	}
	else{
		for (int i = 0; i < cardinalities[id]; i++){
			tuple[id] = i;
			nSet[id].push_back(i);
			generateAllFragments(nSet, tuple, patterns, id+1);
			nSet[id].pop_back();
		}
	}
}

Pattern* Boxcluster::expand(Pattern* fragment){
	double actualG = fragment->getG();
	vector<vector<unsigned int>> nSetF = fragment->getNSet();
	double membershipSumF = fragment->getMembershipSum();
	unsigned int areaF = fragment->getArea();

	vector<set<unsigned int>> sortedNSet(n);
	for (int i = 0; i < n; i++)
		for (const auto& el : nSetF[i])
			sortedNSet[i].insert(el);

	while(true){
		unsigned int dim = INT_MAX, el = INT_MAX, area_aux;
		double membershipSum_aux;

		for (int i = 0; i < n; i++){
			vector<vector<unsigned int>> nSet = nSetF;
			nSet[i].clear();
			for (int j = 0; j < cardinalities[i]; j++){
				nSet[i].push_back(j);
				double membershipSum = membershipSumF;
				unsigned int area = areaF;

				Pattern aux(nSet);
				vector<vector<unsigned int>> aux_tuples = aux.toTuples();
				double diff = data.sumSet(aux_tuples);

				if (sortedNSet[i].find(j) == sortedNSet[i].end()){
					membershipSum += diff;
					area += aux.getArea();
				}
				else{
					membershipSum -= diff;
					area -= aux.getArea();
				}

				double new_g = membershipSum * membershipSum / area;
				if (new_g > actualG){
					actualG = new_g;
					dim = i;
					el = j;
					area_aux = area;
					membershipSum_aux = membershipSum;
				}

				nSet[i].pop_back();
			}
		}

		if (dim == INT_MAX) break;
#ifdef VERBOSE_DEBUG
		cout << "dim: " << dim << " el: " << el << " G: " << actualG << endl
			<< " membershipSum: " << membershipSum_aux << " area: " << area_aux << endl;
#endif

		vector<vector<unsigned int>> nSet = nSetF;
		if (sortedNSet[dim].find(el) == sortedNSet[dim].end()){
			sortedNSet[dim].insert(el);
			nSet[dim].push_back(el);
		}
		else{
			sortedNSet[dim].erase(el);
			for (auto it = nSet[dim].begin(); it != nSet[dim].end(); it++){
				if (*it == el){
					nSet[dim].erase(it);
					break;
				}
			}
		}
		nSetF = nSet;
		membershipSumF = membershipSum_aux;
		areaF = area_aux;
	}

	Pattern *ans = new Pattern(nSetF);
	ans->setMembershipSum(membershipSumF);
	return ans;
}

vector<Pattern*> Boxcluster::select(vector<Pattern*>& patterns){
	priority_queue<SelectionNode*, vector<SelectionNode*>, SelectionNode::Comparator> q;

    SelectionNode::setDataParameters(total_data, total_size);
    SelectionNode::setTotalVariables(total_size);

    double total_res = total_square_data - total_data * total_data / total_size;
    double total_metric = INT_MAX;

    for (auto& pat : patterns){
        SelectionNode* s = new SelectionNode(pat);
        s->setResidual(total_res);
        q.push(s);
    }

    vector<Pattern*> answer;

    while (!q.empty()){
        SelectionNode* actual = q.top();
        q.pop();

#ifdef DEBUG
        cout << "Get node " << actual << ": " << *actual->getKey() << endl << "   coef: " << actual->getCoeff() << endl;
#endif

        if (actual->last_id == answer.size()){

            if (cmpf(actual->getCoeff(), total_metric) > 0){
                delete actual;
                continue;
            }

            total_res = actual->getResidual();
            total_metric = actual->getCoeff();
            answer.push_back(actual->getKey());
            SelectionNode::incVariables();
            SelectionNode::add(actual);
            delete actual;
        }
        else{
            actual->last_id = answer.size();
            actual->setResidual(total_res);
            q.push(actual);
        }
    }

	return answer;
}

int Boxcluster::cmpf(double a, double b){
	if (fabs(a-b) < 1e-8) return 0;
	if (a < b) return -1;
	return 1;
}
