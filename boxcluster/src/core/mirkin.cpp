// Copyright 2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include <iostream>
#include <boost/program_options.hpp>
#include "sysexits.h"

#include "Boxcluster.h"

using namespace boost::program_options;

int main(int argc, char* argv[]){

    Boxcluster solver;

    double threshold;
    int selection_mode;

    string outputDimensionSeparator;
    string outputElementSeparator;
    bool isSizePrinted;
    string patternSizeSeparator;
    string sizeSeparator;
    bool isAreaPrinted;
    string sizeAreaSeparator;
    // Parsing the command line and the option file

    try{
        string optionFileName;

        options_description generic("Generic options");
        generic.add_options()
            ("help,h", "produce help message")
            ("version,V", "display version information and exit")
            ("opt", value<string>(&optionFileName), "set the option file name (by default [pattern-file].opt if present)");

        options_description basicConfig("Basic configuration (on the command line or in the option file)");
        basicConfig.add_options()
            ("threshold,t", value<double>(&threshold)->default_value(0.5), "set threshold to make tensors become binary")
            ("select-coef,coef", value<int>(&selection_mode)->default_value(0), "Set variable selection coefficient:\n\t0: adjusted R squared\n\t1: BIC\n\t2: AIC")
            ("out,o", value<string>(), "set output file name (by default [data-file].out)");

        options_description io("Input/Output format (on the command line or in the option file)");
        io.add_options()
            ("ids", value<string>()->default_value(" "), "set any character separating two dimensions in input data")
            ("ies", value<string>()->default_value(","), "set any character separating two elements in input data")
            ("ods", value<string>(&outputDimensionSeparator)->default_value(" "), "set string separating two dimensions in output data")
            ("oes", value<string>(&outputElementSeparator)->default_value(","), "set string separating two elements in output data")
            ("ps", "print sizes in output data")
            ("css", value<string>(&patternSizeSeparator)->default_value(" : "), "set string separating agglomerates from sizes in output data")
            ("ss", value<string>(&sizeSeparator)->default_value(" "), "set string separating sizes of the different dimensions in output data")
            ("pa", "print areas in output data")
            ("sas", value<string>(&sizeAreaSeparator)->default_value(" : "), "set string separating sizes from areas in output data");

        options_description hidden("Hidden options");
        hidden.add_options()
            ("file", value<string>(), "set data file");

        positional_options_description p;
        p.add("file", -1);

        options_description commandLineOptions;
        commandLineOptions.add(generic).add(basicConfig).add(io).add(hidden);

        variables_map vm;
        store(command_line_parser(argc, argv).options(commandLineOptions).positional(p).run(), vm);
        notify(vm);

        if (vm.count("help")){
            cout << "Usage: mirkin [options] data-file" << endl << generic << basicConfig << io;
            return EX_OK;
        }

        if (vm.count("version")){
            cout << "Mirkin Boxcluster and tricluster" << endl;
            return EX_OK;
        }

        ifstream optionFile;
        if (vm.count("opt")){
            optionFile.open(optionFileName.c_str());
            if (!optionFile)
                throw NoFileException(optionFileName.c_str());
            optionFile.close();
        }
        else{
            if (!vm.count("file")){
                cout << "Usage: mirkin [options] data-file" << endl << generic << basicConfig << io;
                return EX_USAGE;
            }
            optionFileName = vm["file"].as<string>() + ".opt";
        }

        options_description config;
        config.add(basicConfig).add(io).add(hidden);
        optionFile.open(optionFileName.c_str());
        store(parse_config_file(optionFile, config), vm);
        notify(vm);
        optionFile.close();

        if (vm.count("out")){
            solver.parse(threshold, vm["file"].as<string>().c_str(), vm["ids"].as<string>().c_str(), vm["ies"].as<string>().c_str(), vm["out"].as<string>().c_str());
        }
        else{
            solver.parse(threshold, vm["file"].as<string>().c_str(), vm["ids"].as<string>().c_str(), vm["ies"].as<string>().c_str(), (vm["file"].as<string>() + ".out").c_str());
        }
        isSizePrinted = vm.count("ps");
        isAreaPrinted = vm.count("pa");
    }
    catch (unknown_option& e){
        cerr << "Unknown option!" << endl;
        return EX_USAGE;
    }
    catch (UsageException& e){
        cerr << e.what() << endl;
        return EX_USAGE;
    }
    catch (NoFileException& e){
        cerr << e.what() << endl;
        return EX_IOERR;
    }
    catch (DataFormatException& e){
        cerr << e.what() << endl;
        return EX_DATAERR;
    }

    solver.clustering(selection_mode, outputDimensionSeparator.c_str(), outputElementSeparator.c_str(), patternSizeSeparator.c_str(), sizeSeparator.c_str(), sizeAreaSeparator.c_str(), isSizePrinted, isAreaPrinted);
    return EX_OK;
}
