// Copyright 2010,2011,2012,2013,2014,2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "DenseCrispTube.h"

DenseCrispTube::DenseCrispTube(const vector<float>& tubeParam) : tube(tubeParam.size(), true)
{
  unsigned int element = 0;
  for (const float noise : tubeParam)
    {
      if (noise == 0)
	{
	  tube.set(element, false);
	}
      ++element;
    }
}

DenseCrispTube::DenseCrispTube(const vector<pair<unsigned int, float>>& tubeParam) : tube(cardinalityOfLastDimension, true)
{
  for (const pair<unsigned int, float>& entry : tubeParam)
    {
      tube.set(entry.first, false);
    }
}

DenseCrispTube* DenseCrispTube::clone() const
{
  return new DenseCrispTube(*this);
}

void DenseCrispTube::print(vector<unsigned int>& prefix, ostream& out) const
{
  unsigned int hyperplaneId = 0;
  for (dynamic_bitset<>::size_type absentHyperplaneId = tube.find_first(); absentHyperplaneId != dynamic_bitset<>::npos; absentHyperplaneId = tube.find_next(absentHyperplaneId))
    {
      for (; hyperplaneId != absentHyperplaneId; ++hyperplaneId)
	{
	  for (const unsigned int id : prefix)
	    {
	      out << id << ' ';
	    }
	  out << hyperplaneId << " 1" << endl;	  
	}
      ++hyperplaneId;
    }
}

const float DenseCrispTube::noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const
{
  unsigned int noise = 0;
  for (const unsigned int id : *dimensionIt)
    {
      if (tube[id])
	{
	  ++noise;
	}
    }  
  return noise;
}
