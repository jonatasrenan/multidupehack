#include <map>
#include <queue>
#include <algorithm>
#include "IO.h"
#include "Pattern.h"
#include "DataTrie.h"
#include "../../Parameters.h"
#include "../utilities/vector_hash.h"

const double BETA = (sqrt(5) - 1) / 2;

class Solver {
private:

	priority_queue<Candidate*, vector<Candidate*>, Candidate::Comparator> candidates;
	vector<vector<unsigned int>> nSetAll;

	double total_square_data, total_data;
	unsigned int total_size;
	bool is_storing;
	vector<vector<int>> slices;

	void createCandidates(Pattern *p);

public:
	Solver(const Pattern &allpatterns);
	~Solver();

	pair<vector<Pattern*>, vector<Pattern*>> solveAll(vector<Pattern*> &patterns);
	bool isStoring() const;

	static DataTrie<double> alldata;
};
