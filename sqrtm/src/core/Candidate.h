#ifndef CANDIDATE_H_
#define CANDIDATE_H_

#include <list>
#include <unordered_set>
#include <iostream>
#include <vector>
using namespace std;

class Pattern;
class Solver;

class Candidate{
    public:
        Candidate(Pattern *p, const vector<int>& pdim, int pvlr);
        Candidate(Candidate *c);
        ~Candidate();

        Pattern* getPattern() const;
        vector<int> getTargetDim() const;
        int getTargetValue() const;
        double getNewMembershipSum() const;
        double getRSSDiff() const;

        class Comparator{
        public:
            Comparator(){}
            bool operator()(Candidate* const a, Candidate* const b){
                return a->getRSSDiff() > b->getRSSDiff();
            }
        };

    protected:
        Pattern *pattern;
        vector<int> dim;
        int vlr;
        double diff, membershipSum;
};

#endif  /* CANDIDATE_H_ */
