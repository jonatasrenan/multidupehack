#include "util.h"

int cmpf(double a, double b){
	if (fabs(a-b) < EPS) return 0;
	if (a < b) return -1;
	return 1;
}

vector<string> explode(const string& s, const string& m) {
    vector<string> ans;
    size_t begin = 0, found = s.find(m);
    while (found != string::npos) {
        ans.push_back(s.substr(begin, found-begin));
        begin = found + m.size();
        found = s.find(m, begin);
    }
    ans.push_back(s.substr(begin, s.size()));
    return ans;
}
