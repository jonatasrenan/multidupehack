#include "Solver.h"

DataTrie<double> Solver::alldata(0.00);

Solver::Solver(const Pattern &allpatterns){
	this->is_storing = IO::is_storing;
	nSetAll = allpatterns.getNSet();
	int xs = 0, n = alldata.getChildsLen(), k = nSetAll.size();
	unsigned int name = 0;
	total_size = allpatterns.getArea();

	total_data = total_square_data = 0;
	for (const auto& tuple: allpatterns.toTuples()){
		double vlr = alldata.getTuple(tuple.begin(), tuple.end());
		if (vlr == 0) continue;

		total_square_data += vlr*vlr;
		total_data += vlr;
	}

	slices = IO::restrictions;

	vector<bool> mark(n, false);
	for (const auto& slice : slices) {
		for (const auto& vlr : slice) {
			mark[vlr] = true;
		}
	}

	for (int i = 0; i < k; i++) {
		if (!mark[i]) {
			vector<int> aux;
			aux.push_back(i);
			slices.push_back(aux);
		}
	}

	#ifdef DEBUG
	for (const auto& dim : slices) {
		cout << "slice: ";
		for (const auto& vlr : dim)
			cout << vlr << ", ";
		cout << endl;
	}
	#endif

}

Solver::~Solver(){}

void Solver::createCandidates(Pattern *p){
	priority_queue<Candidate*, vector<Candidate*>, Candidate::Comparator> aux;

	for (int i = 0; i < slices.size(); i++) {
		int dim = slices[i][0];
		for (const auto& vlr : nSetAll[dim]) {
			if (!p->isIn(dim, vlr)) {
				aux.push(new Candidate(p, slices[i], vlr));
			}
		}
		for (const auto& vlr : slices[i]) {

		}
	}

	if (aux.empty()) return;

	#ifdef DEBUG
	cout << "chose candidate " << aux.top() << " for pattern " << *p << endl;
	#endif

	candidates.push(aux.top());
	aux.pop();

	while (!aux.empty()){
		aux.pop();
	}
}

pair<vector<Pattern*>, vector<Pattern*>> Solver::solveAll(vector<Pattern*> &patterns){
	unordered_map<vector<set<unsigned int>>, Pattern*, vector_hash<set<unsigned int>>> kept, stored;

#ifdef DEBUG
	cout << "* Main solve Phase" << endl;
#endif

	for (auto& pattern : patterns){
		kept[pattern->getSortedNSet()] = pattern;
		stored[pattern->getSortedNSet()] = pattern;
		createCandidates(pattern);
	}

	#ifdef DEBUG
	cout << "Number of candidates: " << candidates.size() << endl;
	#endif

	long long M = sqrt(IO::maximalNbOfCandidates * 1000000);

	while (!candidates.empty() && kept.size() > M){
		Candidate* actual = candidates.top();
		candidates.pop();
		if (kept.find(actual->getPattern()->getSortedNSet()) == kept.end()){
			cerr << "Error!! Not finding pattern\n" << *(actual->getPattern()) << endl;
			abort();
		}
		kept.erase(actual->getPattern()->getSortedNSet());

		#ifdef DEBUG
		cout << "getting candidate " << actual << " nSet: " << *actual->getPattern()
			<< "\n\tsum: " << actual->getNewMembershipSum()
			<< "\n\trss: " << actual->getRSSDiff() << endl;
		#endif

		Pattern *p = new Pattern(actual);

		bool is_ok = false;
		if (isStoring()) is_ok = stored.find(p->getSortedNSet()) == stored.end();
		else is_ok = kept.find(p->getSortedNSet()) == kept.end();

		if (is_ok){
			createCandidates(p);
			kept[p->getSortedNSet()] = p;
			if (isStoring()) stored[p->getSortedNSet()] = p;
		}

		delete actual;

		#ifdef DEBUG
		cout << "fragments: " << kept.size() << endl;
		#endif
	}

	vector<Pattern*> border, all;
	for (auto pat : kept){
		border.push_back(pat.second);
	}

	if (isStoring()) {
		for (auto pat : stored) {
			all.push_back(pat.second);
		}
	}

	return make_pair(border, all);
}

bool Solver::isStoring() const {
	return is_storing;
}
