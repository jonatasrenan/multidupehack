#include "Solver.h"

DataTrie<double> Solver::alldata(0.00);

ILOBRANCHCALLBACK2(MyBranch, IloNumVarArray, vars, int, len) {
	if (getBranchType() != BranchOnVariable)
		return;

	IloNumArray x(getEnv());
	IloNumArray obj(getEnv());
	IntegerFeasibilityArray feas(getEnv());
	getValues(x, vars);
	getObjCoefs(obj, vars);
	getFeasibilities(feas, vars);

	int besti = -1;
	double maxinf = 0, maxobj = 0;

	for (int i = 0; i < len; i++){
		if (feas[i] == Infeasible){
			double xi = x[i] - IloFloor(x[i]);
			if (xi > 0.5) xi = 1 - xi;
			if (xi > maxinf || (xi == maxinf && IloAbs(obj[i]) >= maxobj)){
				besti = i;
				maxinf = xi;
				maxobj = IloAbs(obj[i]);
			}
		}
	}

	if (besti >= 0){
		makeBranch(vars[besti], x[besti], IloCplex::BranchDown, getObjValue());
		makeBranch(vars[besti], x[besti], IloCplex::BranchUp, getObjValue());
	}

	x.end();
	obj.end();
	feas.end();
}

Solver::Solver(const Pattern& allpatterns) {
	int xs = 0, n = alldata.getChildsLen(), k = allpatterns.getNSet().size();
	unsigned int name = 0;
	total_size = allpatterns.getArea();

	this->slices = IO::RESTRICTIONS;

	vector<bool> mark(n, false);
	for (const auto& slice : IO::RESTRICTIONS) {
		for (const auto& vlr : slice) {
			mark[vlr] = true;
		}
	}

	for (int i = 0; i < k; i++) {
		if (!mark[i]) {
			vector<int> aux;
			aux.push_back(i);
			slices.push_back(aux);
		}
	}

	PDEBUG("* Reading the model");

	instance = IloModel(env);
	cplex = IloCplex(env);

	int dit = 0;
	for (const auto& dimension : allpatterns.getNSet()){
		xs += dimension.size();
		for (const auto& vlr : dimension){
			const auto &p = std::make_pair(dit, vlr);
			reconv[name] = p;
			conv[p] = name++;
		}
		ksizes.push_back(dimension.size());
		dit++;
	}

	PDEBUG("|W|=" << n << " |X|=" << xs);
	tuples = allpatterns.toTuples();

	w_var = IloNumVarArray(env, n, 0, 1, ILOBOOL);
	x_var = IloNumVarArray(env, xs, 0, 1, ILOBOOL);
	w_len = n;
	x_len = xs;

	ntuples_constr = IloRangeArray(env);

	dit = 0;
	for (const auto& dimension : allpatterns.getNSet()){
		IloExpr aux(env);
		for (const auto& vlr : dimension){
			aux += x_var[conv[std::make_pair(dit, vlr)]];
		}
		dit++;
		ntuples_constr.add(aux <= 1);
	}

	IloExpr G_expr(env);

	int tid = 0;
	total_data = total_square_data = 0;
	for (const auto& tuple: tuples){
		double vlr = alldata.getTuple(tuple.begin(), tuple.end());
		if (vlr == 0) continue;

		total_square_data += vlr*vlr;
		total_data += vlr;

		G_expr += vlr * w_var[tid];

		IloExpr square_expr(env);

		for (int i = 0; i < tuple.size(); i++)
			square_expr += x_var[conv[std::make_pair(i, tuple[i])]];

		square_expr -= k * w_var[tid];

		IloRange square_constr(env, 0, square_expr, k-1);
		instance.add(square_constr);

		tid++;
	}

	//adding the equality restrictions
	IloRangeArray restr(env);
	for (const auto& slice : slices) {
		for (int i = 1; i < slice.size(); i++) {
			PDEBUG("Adding restrictions: " << slice[i-1] << "=" << slice[i]);
			for (const auto& vlr : allpatterns.getNSet()[slice[i]]){
				restr.add(
					x_var[conv[std::make_pair(slice[i], vlr)]] -
					x_var[conv[std::make_pair(slice[i-1], vlr)]] == 0
				);
			}
		}
	}

	#ifdef DEBUG
	for (const auto& dim : slices) {
		cout << "slice: ";
		for (const auto& vlr : dim)
			cout << vlr << ", ";
		cout << endl;
	}
	#endif

	optimal_constr = IloRange(env, 0, G_expr, CPX_INFBOUND);

	IloObjective obj(env, G_expr, IloObjective::Maximize);

	instance.add(obj);
	instance.add(restr);
	instance.add(ntuples_constr);
	instance.add(optimal_constr);

	cplex.extract(instance);

#ifdef SUPRESS
	cplex.setOut(env.getNullStream());
	cplex.setWarning(env.getNullStream());
#endif

#ifdef DEBUG
	cplex.exportModel("debug.lp");
#endif

	for (int i = 0; i < x_len; i++)
		cplex.setPriority(x_var[i], 3);

	cplex.use(MyBranch(env, x_var, x_len));

}

Solver::~Solver(){
	ntuples_constr.end();
	w_var.end();
	x_var.end();
	cplex.end();
	instance.end();
	env.end();
}

unsigned int Solver::increment(
		const vector<int>& begin, int pos, unsigned int prod) {
	ntuples_constr[pos].setBounds(begin[pos], begin[pos]+1);
	return prod / begin[pos] * (begin[pos]+1);
}

unsigned int Solver::decrement(
		const vector<int>& begin, int pos, unsigned int prod) {
	ntuples_constr[pos].setBounds(begin[pos]-1, begin[pos]);
	return prod / (begin[pos]+1) * begin[pos];
}

void Solver::solveN(vector<int> &begin) {
	unsigned int prod = 1;
	for (int j = 0; j < begin.size(); j++){
		prod *= begin[j];
		ntuples_constr[j].setBounds(0, begin[j]);
	}

	time_t begin_timestamp, now;

	time(&begin_timestamp);

	while (true){
		int selected_slice = -1;

		for (int i = 0; i < slices.size(); i++){
			for (const auto& vlr : slices[i]) {
				prod = increment(begin, vlr, prod);
			}
			optimal_constr.setBounds(sqrt(gbest * prod), CPX_INFBOUND);
			if (cplex.solve()){
				double v = cplex.getObjValue();
				double g = v * v / prod;
				PDEBUG("CPLEX result " << v << " g: " << g);
				if (g - gbest > EPS){
					gbest = g;
					selected_slice = i;
				}
			} else {
				PDEBUG("CPLEX didn't find any solution!");
			}
			for (const auto& vlr : slices[i]) {
				prod = decrement(begin, vlr, prod);
			}
		}

		time(&now);

		PDEBUG("now: " << now << " expanding in dimension: " << selected_slice);
		if (selected_slice == -1 || (IO::TIME_LIMIT > 0 &&
				difftime(now, begin_timestamp) > IO::TIME_LIMIT)){
			break;
		}

		for (const auto& vlr : slices[selected_slice]) {
			prod = increment(begin, vlr, prod);
			begin[vlr]++;
		}
	}
}

Pattern* Solver::solve(const std::vector<int> &begin) {
	vector<vector<unsigned int>> answer;
	IloNumArray x(env);
	unsigned int k = tuples[0].size();

	answer.resize(k);

	vector<int> aux(begin.begin(), begin.end());
	solveN(aux);
	nbest = aux;

	optimal_constr.setBounds(0, CPX_INFBOUND);

	for (int i = 0; i < k; i++){
		ntuples_constr[i].setBounds(nbest[i]-1, nbest[i]);
	}

	cplex.solve();
	cplex.getValues(x, x_var);

	for (int i = 0; i < x_len; i++)
		if (x[i] == 1){
			const std::pair<unsigned int, unsigned int> &p = reconv[i];
			answer[p.first].push_back(p.second);
		}

	x.end();

	Pattern* bigpat = new Pattern(answer);
	vector<vector<unsigned int>> tuples = bigpat->toTuples();
	bigpat->setMembershipSum(Solver::alldata.sumSet(tuples));

	return bigpat;
}

Pattern* Solver::solve(const Pattern *fragment){
	IloRangeArray contr(env);

	PDEBUG("Start expanding fragment...");
	std::vector<int> begin;
	int dit = 0;
	for (const auto& dimensionIt : fragment->getNSet()){
		for (const auto& vlr : dimensionIt){
			contr.add(x_var[conv[std::make_pair(dit, vlr)]] == 1);
		}
		dit++;
		begin.push_back(dimensionIt.size());
	}

	instance.add(contr);
	gbest = fragment->getG();
	Pattern *answer = solve(begin);
	PDEBUG("...concluded! Entire pattern: " << *answer);

	instance.remove(contr);
	contr.end();

	return answer;
}

void Solver::updateSet(
		const vector<vector<unsigned int>>::const_iterator& nSetBegin,
		const vector<vector<unsigned int>>::const_iterator& nSetEnd,
		vector<unsigned int> &prefix,
		Pattern *actual, const DataTrie<double> &t) {

	if (nSetBegin == nSetEnd){
		double s = t.getTuple(prefix.begin(), prefix.end());
		if (s != 1.0){
			actual->setArea(actual->getArea()+1);
			actual->setMembershipSum(actual->getMembershipSum() +
				alldata.getTuple(prefix.begin(), prefix.end()));
		}
	}
	else{
		for (const auto& valueIt : *nSetBegin){
			prefix.push_back(valueIt);
			updateSet(nSetBegin+1, nSetEnd, prefix, actual, t);
			prefix.pop_back();
		}
	}
}

vector<Pattern*> Solver::solveAll(vector<Pattern*> &patterns) {
	vector<Pattern*> answer;
	vector<unsigned int> prefix;
	priority_queue<Pattern*, vector<Pattern*>, Pattern::Comparator> q;
	DataTrie<double> t(0.00);

	PDEBUG("* Main solve Phase");
	for (int i = 0; i < patterns.size(); i++){
		q.push(patterns[i]);
	}

	while (!q.empty()){
		Pattern* actual = q.top();
		q.pop();

		PDEBUG("Getting fragment " << *actual << endl
								   << "    G=" << actual->getG());

		if (actual->last_id == answer.size()){
			Pattern *bigpat = solve(actual);
			answer.push_back(bigpat);
			t.addSet(bigpat->getNSet().begin(),
					 bigpat->getNSet().end(),
					 prefix, 1.00);
		}
		else{
			actual->setArea(0);
			actual->setMembershipSum(0);
			updateSet(actual->getNSet().begin(),
					  actual->getNSet().end(),
					  prefix, actual, t);
			actual->last_id = answer.size();

			if (actual->getG() > 0) {
				q.push(actual);
			}
		}
	}

	return answer;
}
