#include <ilcplex/ilocplex.h>
#include <map>
#include <queue>
#include <algorithm>
#include <ctime>
#include "Pattern.h"
#include "DataTrie.h"
#include "IO.h"
#include "../../Parameters.h"

const double BETA = (sqrt(5) - 1) / 2;

class Solver{
private:
	IloEnv env;
	IloModel instance;
	IloCplex cplex;
	IloNumVarArray w_var, x_var;
	IloRangeArray ntuples_constr;
	IloRange optimal_constr;
	unsigned int w_len, x_len;

	std::vector<std::vector<int>> slices;

	vector<unsigned int> ksizes;
	vector<vector<unsigned int>> tuples;
	map<pair<unsigned int, unsigned int>, unsigned int> conv;
	map<unsigned int, pair<unsigned int, unsigned int>> reconv;

	double gbest;
	std::vector<int> nbest;

	double total_square_data, total_data;
	unsigned int total_size;

	void solveN(std::vector<int> &begin);
	Pattern* solve(const std::vector<int> &begin);
	Pattern* solve(const Pattern *fragment);

	void updateSet(
			const vector<vector<unsigned int>>::const_iterator& nSetBegin,
			const vector<vector<unsigned int>>::const_iterator& nSetEnd,
			vector<unsigned int> &prefix,
			Pattern *actual, const DataTrie<double> &t);

	unsigned int increment(const vector<int>& begin, int pos, unsigned int prod);
	unsigned int decrement(const vector<int>& begin, int pos, unsigned int prod);

public:
	Solver(const Pattern& allpatterns);
	~Solver();

	vector<Pattern*> solveAll(vector<Pattern*> &patterns);

	static DataTrie<double> alldata;
};
