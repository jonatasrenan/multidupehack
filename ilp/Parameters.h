#ifndef PARAMETERS_H_
#define PARAMETERS_H_

// Supress
/* SUPRESS disabled the output to stdio from ILOCPLEX while expanding fragments */
#define SUPRESS

/* DEBUG turns on the output (on the standard output) of information during the selection of the patterns. This option may be enabled by who wishes to understand how this selection is performed on a small number of error-tolerant patterns. */
//#define DEBUG


#ifdef DEBUG
#define PDEBUG(x) cout << x << endl;
#else
#define PDEBUG(x)
#endif

#endif /*PARAMETERS_H_*/
