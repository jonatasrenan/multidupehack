#include "Solver.h"

DataTrie<double> Solver::alldata(0.00);

Solver::Solver(const Pattern &allpatterns){
	vector<vector<unsigned int>> tuples = allpatterns.toTuples();

	total_size = allpatterns.getArea();
	total_data = total_square_data = 0;

	for (const auto& tuple: tuples){
		double vlr = alldata.getTuple(tuple.begin(), tuple.end());
		if (vlr == 0) continue;

		if (vlr == 2){
			alldata.addTuple(tuple.begin(), tuple.end(), 0);
			continue;
		}

		total_square_data += vlr*vlr;
		total_data += vlr;
	}

	PDEBUG("total_data: " << total_data << " total_size: " << total_size);
}

Solver::~Solver(){}

vector<Pattern*> Solver::select(vector<Pattern*>& patterns){
	priority_queue<SelectionNode*, vector<SelectionNode*>,
					SelectionNode::Comparator> core;

    SelectionNode::setDataParameters(total_data, total_size);
    SelectionNode::setTotalVariables(total_size);

    double total_res = total_square_data - total_data * total_data / total_size;
    double total_metric = INT_MAX;

    for (auto& pat : patterns){
        SelectionNode* s = new SelectionNode(pat);
		s->updateSSres();
        s->setResidual(total_res);
        core.push(s);
    }

    vector<SelectionNode*> selected;
	list<SelectionNode*> buffer;
	list<SelectionNode*>::iterator winner;
	double mincoeff = INT_MAX;

    while (!core.empty()){
        SelectionNode* actual = core.top();
        core.pop();

        PDEBUG("Get node " << actual->getKey() << ": "
						   << *actual->getKey() << endl
						   << "   coef: " << actual->getCoeff()
						   << "\n    res: " << actual->getResidual()
					       << "\n     lb: " << actual->getLowerBound());
		PDEBUG("density: " << actual->getKey()->getMembershipSum() /
							  actual->getKey()->getArea());

        if (actual->last_id == selected.size()){
        	PDEBUG("send pattern to buffer!");

			buffer.push_back(actual);
			if (mincoeff > actual->getDiff()) {
				mincoeff = actual->getDiff();
				PDEBUG("Updated winner to pattern with diff: " << mincoeff);
				winner = buffer.end();
				winner--;
			}

			if (actual->getLowerBound() > mincoeff || core.empty()) {

				if (cmpf((*winner)->getCoeff(), total_metric) <= 0) {
					PDEBUG("Pattern " << (*winner)->getKey() << " was chosen!");
		            total_res = (*winner)->getResidual();
		            total_metric = (*winner)->getCoeff();
		            selected.push_back((*winner));
		            SelectionNode::incVariables();
		            SelectionNode::add(*winner);
				} else break;

				PDEBUG("adding back to core " << buffer.size()-1
										      << " elements...");

				//delete *winner;
				mincoeff = INT_MAX;
				buffer.erase(winner);
				for (auto it : buffer) {
					core.push(it);
				}
				buffer.clear();
			}
        } else {
            actual->last_id = selected.size();
			actual->updateSSres();
            actual->setResidual(total_res);
            core.push(actual);
        }
    }

	for (auto it : buffer) {
		core.push(it);
	}
	buffer.clear();

	while (!core.empty()) {
		delete core.top();
		core.pop();
	}

	PDEBUG("REPEATING PROCESS STARTING...");

	/* Repeat the process until it converges */
	unordered_map<vector<unsigned int>, int, vector_hash<unsigned int>>
			tuples_selected;

	int i = 0;
	for (const auto& pat : selected) {
		PDEBUG("Processing pattern " << i++);
		PDEBUG("diffres: " << pat->getDiff());
		for (const auto& tuple : pat->getKey()->toTuples()) {
			if (tuples_selected.find(tuple) == tuples_selected.end()) {
				tuples_selected[tuple] = 0;
				double vlr = alldata.getTuple(tuple.begin(), tuple.end());
			}
			tuples_selected[tuple]++;
		}
	}

	while (true) {
		SelectionNode::setTotalVariables(tuples_selected.size());
		total_res = total_square_data - total_data * total_data / total_size;
		total_metric = INT_MAX;
		int i = 0;
		for (; i < selected.size(); i++) {
			selected[i]->setResidual(total_res);
			PDEBUG("pattern " << i << " coeff: " << selected[i]->getCoeff());
			if (cmpf(selected[i]->getCoeff(), total_metric) > 0) break;
			total_res = selected[i]->getResidual();
			total_metric = selected[i]->getCoeff();
			SelectionNode::incVariables();
		}
		int j = i;
		PDEBUG("stop at: " << j);
		for (; i < selected.size(); i++) {
			for (const auto& tuple : selected[i]->getKey()->toTuples()) {
				tuples_selected[tuple]--;
				if (tuples_selected[tuple] == 0){
					tuples_selected.erase(tuple);
					double vlr = alldata.getTuple(tuple.begin(), tuple.end());
				}
			}
			delete selected[i];
		}
		if (selected.size() == j) break;
		selected.resize(j);
	}

	vector<Pattern*> answer;
	for (const auto& node : selected) {
		answer.push_back(node->getKey());
		delete node;
	}

	return answer;
}
