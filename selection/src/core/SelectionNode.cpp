#include "SelectionNode.h"
#include "Solver.h"

DataTrie<double> SelectionNode::data(0);
unsigned int SelectionNode::totalVariables;
unsigned int SelectionNode::currentSelectedVariables;
int SelectionNode::mode = 0;


SelectionNode::SelectionNode(Pattern* const p): coeff(0){
    key = p;
    last_id = 0;
}

Pattern* SelectionNode::getKey(){
    return key;
}

void SelectionNode::setMode(int v){
    mode = v;
}

void SelectionNode::setDataParameters(const double total_sum,
                                      const unsigned int total_area){
    data = DataTrie<double>(total_sum / total_area);
}

void SelectionNode::setTotalVariables(const unsigned int totalVariablesParam){
    totalVariables = totalVariablesParam;
    currentSelectedVariables = 0;
}

void SelectionNode::incVariables(){
    currentSelectedVariables++;
}

void SelectionNode::add(SelectionNode* s) {
    const vector<vector<unsigned int>>& nSetParam = s->getKey()->getNSet();
    const double lambda = s->getKey()->getMembershipSum() /
                          s->getKey()->getArea();
    vector<unsigned int> prefix;
    data.addSet(nSetParam.begin(), nSetParam.end(), prefix, lambda);
}

double SelectionNode::getCoeff() const {
    return coeff;
}

void SelectionNode::setCoeff(const double acc) {
    coeff = acc;
}

double SelectionNode::getResidual() const {
    return res;
}

double SelectionNode::getDiff() const {
    return diffres;
}

double SelectionNode::getLowerBound() const {
    return lower_bound;
}

void SelectionNode::setResidual(const double acc) {
    res = acc + diffres;
    if (res < 0) {
        cerr << "ABORTED BY RESIDUAL NEGATIVE res: " << res << endl;
        abort();
    }
    switch (mode) {
        case 2:
            coeff = 2 * currentSelectedVariables + totalVariables * log(res);
        break;

        case 1:
            coeff = totalVariables * log(res / totalVariables) +
                    currentSelectedVariables * log(totalVariables);
        break;

        default:
            coeff = res / (totalVariables - currentSelectedVariables - 1);
        break;
    }
}

void SelectionNode::updateSSres() {
    double ssres = 0;
    double lambda = key->getMembershipSum() / key->getArea();
    vector<vector<unsigned int>> tuples = key->toTuples();
    this->lower_bound = 0;

    for (const auto &tuple : tuples) {
        double noisy = Solver::alldata.getTuple(tuple.begin(), tuple.end());
        double old_lambda = data.getTuple(tuple.begin(), tuple.end());
        double best_lambda = max(old_lambda, lambda);
        double aux = 2 * noisy * (old_lambda - best_lambda) +
                     best_lambda * best_lambda - old_lambda * old_lambda;
        ssres += aux;
        if (aux < 0) lower_bound += aux;
    }
    diffres = ssres;
}
