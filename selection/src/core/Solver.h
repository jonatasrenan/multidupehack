#include <map>
#include <queue>
#include <list>
#include <algorithm>
#include <climits>
#include "Pattern.h"
#include "DataTrie.h"
#include "SelectionNode.h"
#include "../utilities/vector_hash.h"
#include "../../Parameters.h"

const double BETA = (sqrt(5) - 1) / 2;

class Solver{
private:

	double total_square_data, total_data;
	unsigned int total_size;

public:
	Solver(const Pattern &allpatterns);
	~Solver();

	vector<Pattern*> select(vector<Pattern*> &patterns);

	static DataTrie<double> alldata;
};
