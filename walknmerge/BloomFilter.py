##################
#### Authors: Dora Erdos and Pauli Miettinen
#### Copyright (c) 2013 Max Planck Institut for Informatics
#### All rights reserved
####
#### When using this code please cite:
#### [1] Dora Erdos and Pauli Miettinen: Walk'N'Merge: A Scalable Algorithm for Boolean Tensor Factorization. In Proc. 13th IEEE International Conference on Data Mining (ICDM'13), 2013.
##################  

import ctypes
import itertools

#calls the Bloom filter and its functions in myBloomFilter.c
class BloomFilter(object):
    """A Bloom filter implemented in C"""
    def __init__(self, _capacity, _errorRate):
        self.bloom = ctypes.CDLL('./pBloomFilter.so')
        self.capacity = _capacity
        self.errorRate = ctypes.c_double(_errorRate)
        self.bf =self.bloom.create_mbf(self.capacity,self.errorRate)
        if not self.bf: # null pointer
            raise ValueError('Error when allocating space for the Bloom filter (out of memory or requiring more than 2^32 bits)')

    #def __del__(self):
    #    self.bloom.destroy_mbf(self.bf)
    #    self.bloom.__del__()

    def _bitExpToBytes(self, x):
        """Returns a human-readable byte value from 2^x bits"""
        bytes = x - 3 # i.e. divided by 8
        if bytes <= 0:
            return "0B"
        prefix = ""
        divisor = 0
        if bytes >= 30:
            prefix = "G"
            divisor = 30
        elif bytes >= 20:
            prefix = "M"
            divisor = 20
        elif bytes >= 10:
            prefix = "k"
            divisor = 10
        return str(2**(bytes-divisor))+prefix+"B"

    def __str__(self):
        """ Provides the string representation """
        # Do not use the stored values, but query them from self.bf
        numBitsExp = self.bloom.get_numBitsExp(self.bf)
        numBits = self.bloom.get_numBits(self.bf)
        capacity = self.bloom.get_capacity(self.bf)
        get_errorRate = self.bloom.get_errorRate
        get_errorRate.restype = ctypes.c_double
        errorRate = get_errorRate(self.bf)
        res = "Bloom filter of capacity "+str(capacity)\
              +" and error rate "+str(errorRate)\
              +" having bit array of "+str(numBits)\
              +" bits (2^"+str(numBitsExp)+" bits, "\
              +self._bitExpToBytes(numBitsExp)+")"
        return res

    #adds tensor cell to BF  
    def add(self,key1, key2, key3):
        self.bloom.add_mbf(self.bf,key1, key2, key3)

    #adds whole block (product of vectors X,Y,Z) to BF
    def addBlock(self,X,Y,Z):
         for (key1,key2,key3) in (itertools.product(X,Y,Z)):self.bloom.add_mbf(self.bf,key1, key2, key3)

    #checks if cell (key1,key2,key3) is covered 
    def check(self,key1,key2,key3):
        return self.bloom.check_mbf(self.bf,key1,key2,key3)
        
    #counts how many cells in key1 x key2 x key3 are covered. returns num of zeros or maxErr if larger than that
    def multi_check_zeros(self,key1,key2,key3,vecLen,maxErr):
        keyArray = ctypes.c_int*vecLen
        return self.bloom.multi_check_mbf_zeros(self.bf, keyArray(*tuple(key1)), keyArray(*tuple(key2)), keyArray(*tuple(key3)),vecLen, maxErr)

    #counts how many cells in key1 x key2 x key3 are covered. returns num of zeros or maxErr if larger than that
    def multi_check_ones(self,key1,key2,key3,vecLen,minones):
        keyArray = ctypes.c_int*vecLen
        return self.bloom.multi_check_mbf_ones(self.bf, keyArray(*tuple(key1)), keyArray(*tuple(key2)), keyArray(*tuple(key3)),vecLen, minones)

    def check_ones_product(self, x, y, z, minones):
        """Checks how many 1s there are in all the combinations of x, y, and z.
        Stops as soon as there are more than minones 1s"""
        x_a = ctypes.c_int*len(x)
        y_a = ctypes.c_int*len(y)
        z_a = ctypes.c_int*len(z)

        return self.bloom.check_ones_product(self.bf, x_a(*tuple(x)), int(len(x)),
                                             y_a(*tuple(y)), int(len(y)),
                                             z_a(*tuple(z)), int(len(z)),
                                             int(minones))

    # Checks how many elements in the merge are NOT covered
    def check_merge(self, l0, l1, l2, t0, t1, t2, maxErr):
        # We need to make l? and t? into int arrays
        a0 = ctypes.c_int*len(l0)
        a1 = ctypes.c_int*len(l1)
        a2 = ctypes.c_int*len(l2)
        b0 = ctypes.c_int*len(t0)
        b1 = ctypes.c_int*len(t1)
        b2 = ctypes.c_int*len(t2)
        la = ctypes.c_int*3
        lb = ctypes.c_int*3

        return self.bloom.check_merge_mbf(self.bf, a0(*tuple(l0)), a1(*tuple(l1)), a2(*tuple(l2)),
                                     la(len(l0), len(l1), len(l2)),
                                     b0(*tuple(t0)), b1(*tuple(t1)), b2(*tuple(t2)),
            lb(len(t0), len(t1), len(t2)), maxErr)

