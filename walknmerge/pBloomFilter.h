//
//  libpBloomFilter.h
//  pBloomFilter
//
//  Created by Pauli Miettinen on 27.8.2012.
//  Copyright (c) 2012 Pauli Miettinen. All rights reserved.
//

#ifndef pBloomFilter_libpBloomFilter_h
#define pBloomFilter_libpBloomFilter_h

/* Struct to hold the Bloom filter data */
typedef struct {
    unsigned int numBitsExp;
    unsigned long numBits;
    unsigned long bitMask;
    char *bArray;
    unsigned int capacity;
    double errorRate;
} mbf;


/* Creates a Bloom Filter capable of holding capacity bits with error rate error rate */
mbf *
create_mbf(unsigned int capacity, double errorRate);

/* Destroys a Bloom Filter */
void
destroy_mbf(mbf* bf);

/* Adds an item to mbf */
void
add_mbf(mbf *const bf, const int key1, const int key2, const int key3);

/* Functions to query the status of the Bloom filter */
unsigned int
get_numBitsExp(const mbf *);

unsigned long
get_numBits(const mbf *);

unsigned int
get_capacity(const mbf *);

double
get_errorRate(const mbf *);


/* Checks a value in mbf */
int
check_mbf(const mbf *bf, const int key1, const int key2, const int key3);

/* Checks multiple values of Bloom filter and returns the count of found objects */
/*returns if the number of zeros is above maxErr*/
int
multi_check_mbf_zeros(const mbf *bf, const int *key1, const int *key2, const int *key3, const int len, const int maxErr);

/* Checks multiple values of Bloom filter and returns the count of found objects */
/*returns if the number of ones is above minOnes. Used to count min density*/
int
multi_check_mbf_ones(const mbf *bf, const int *key1, const int *key2, const int *key3, const int len, const int minOnes);
/*
 * Checks the number of 1s in the filter using all the combinations of values in x, y, and z
 */
int
check_ones_product(const mbf *bf, const int *x, const int lenx, const int *y, const int leny, const int *z, const int lenz, const int minones);

#endif
