import sys

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: python format [file]"
    else:
        with open(sys.argv[1]) as f:
            pat = []
            for line in f:
                values = [i for i in line.split('\t')[:-1]]
                if (len(values) > 0 and len(pat) <= 2):
                    pat.append(','.join(values))
                if (len(pat) == 3):
                    print ' '.join(pat)
                    pat = []

