import sys

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: python unc2bin [dataset] [eps]"
    else:
        EPS = float(sys.argv[2])
        with open(sys.argv[1]) as f:
            for line in f:
                values = line.split(' ')
                if float(values[-1]) >= EPS:
                    print ' '.join(values[:-1])

