##################
#### Authors: Dora Erdos and Pauli Miettinen
#### Copyright (c) 2013 Max Planck Institut for Informatics
#### All rights reserved
####
#### When using this code please cite:
#### [1] Dora Erdos and Pauli Miettinen: Walk'N'Merge: A Scalable Algorithm for Boolean Tensor Factorization. In proc. 13th IEEE International Conference on Data Mining (ICDM'13), 2013.
##################
    

import math
import os
import getopt, sys
import time
import commands
import random
import operator
import ctypes 
import collections
import itertools
import multiprocessing as mp
import multiprocessing.sharedctypes as mpsctype
import BloomFilter as BF

## GLOBAL VARIABLES
GlobalOptions = None # Command-line options, set in main(), only read elsewhere

class Blockclass(object):
   
     
    def __init__(self,_id):
        self.id = _id
        self.counter = 0
        #set of block ids
        self.blocks = set([])
        #list containing coords[i] = (X,Y,Z) 
        #the elements of block i
        self.coords = []
 
    #prints blocks to standard error.
    #Don't use with large data!
    def printBlocks(self):
        sys.stderr.write('-------------------\n')
        sys.stderr.write('blockclass %i\n'%self.id)
        sys.stderr.write('blocks:')
        for bId in self.blocks:
            sys.stderr.write('block %i:\n'%bId)
            sys.stderr.write('X:')
            (X,Y,Z) = self.coords[bId]
            for ind in X:
                sys.stderr.write('\t%i'%ind)
            sys.stderr.write('\n')
            sys.stderr.write('Y:')
            for ind in Y:
                sys.stderr.write('\t%i'%ind)
            sys.stderr.write('\n')
            sys.stderr.write('Z:')
            for ind in Z:
                sys.stderr.write('\t%i'%ind)
            sys.stderr.write('\n')

    #add new block with index sets X,Y,Z
    def addBlockXYZ(self,X,Y,Z):
        self.blocks.add(self.counter)
        self.coords.append((X,Y,Z))
        self.counter += 1
        
    #add singleton block with coords x,y,z
    def addBlockSingleton(self,x,y,z):
        X = set([x])
        Y = set([y])
        Z = set([z])
        self.blocks.add(self.counter)
        self.coords.append((X,Y,Z))
        self.counter += 1

    #merge block rightblock onto block leftblock
    def mergeBlocks(self,leftblock,rightblock):
        self.blocks.discard(rightblock)
        (lX,lY,lZ) = self.coords[leftblock]
        (rX,rY,rZ) = self.coords[rightblock]
        self.coords[leftblock] = (lX.update(RX),lY.update(RY),lZ.update(RZ))

    #add coordinates X,Y,Z to blockid
    def mergeXYZ(self,leftblock,X,Y,Z):
        (lX,lY,lZ) = self.coords[leftblock]
        self.coords[leftblock] = (lX.union(X),lY.union(Y),lZ.union(Z))


class ListGraph(object):
    """A graph where neighbourhood structure is stored in lists"""
    def __init__(self):
        self.counter = 0
        self.Xmap = {}
        self.Ymap = {}
        self.Zmap = {}
        self.nodes = [] # List of lists of neighbour relations
        self.coords = []

    def _getNeighbours(self, map, key):
        n = set()
        if key in map:
            for idx in map[key]:
                n.add(idx)
        return n

    def _toMap(self, map, key, id):
        if key not in map:
            map[key] = [id]
        else:
            map[key].append(id)

    def add(self, x, y, z):
        """Adds a node x, y, z in the graph"""
        self.coords.append((x, y, z))
        neighbours = set()
        # what existing nodes are my neighbours?
        neighbours.update(self._getNeighbours(self.Xmap, (y, z)))
        neighbours.update(self._getNeighbours(self.Ymap, (x, z)))
        neighbours.update(self._getNeighbours(self.Zmap, (x, y)))
        for n in neighbours:
            # Add me to my neighbours
            self.nodes[n].append(self.counter)
        #neighbours.add(self.counter) # I'm NOT my own neighbour
        # Turn neighbours into a list
        self.nodes.append(list(neighbours))
        # Add me to X, Y, and Z maps
        self._toMap(self.Xmap, (y, z), self.counter)
        self._toMap(self.Ymap, (x, z), self.counter)
        self._toMap(self.Zmap, (x, y), self.counter)
        # Increase counter
        self.counter += 1

    def __str__(self):
        return 'Xmap: '+str(self.Xmap)+'\nYmap: '+str(self.Ymap)+'\nZmap: '+str(self.Zmap)+'\nnodes: '+str(self.nodes)+'\ncoords: '+str(self.coords)

class ArrayGraph(object):
    """A graph where neighbourhood structure is stored in arrays"""
    def __init__(self, lgr):
        """Takes a ListGraph object and turns it into an ArrayGraph object"""
        self.numNodes = len(lgr.nodes)
        self.coords = lgr.coords
        # nodePos tells where this node's part begins
        # Last element tells the overall length
        self.nodePos = mpsctype.RawArray('l', len(lgr.nodes)+1) # 1+ nodes elements
        for i in range(len(lgr.nodes)):
            self.nodePos[i+1] = self.nodePos[i] + len(lgr.nodes[i])
        self.neighbours = mpsctype.RawArray('l', self.nodePos[-1])
        # Copy the lists from lgr.nodes
        j = 0
        for n in range(len(lgr.nodes)):
            for i in range(len(lgr.nodes[n])):
                self.neighbours[j] = lgr.nodes[n][i]
                j += 1

    def deleteNodes(self, delNodes):
        """Deletes the nodes in delNodes"""
        delNodes = set(delNodes)
        if len(delNodes) == 0:
            return
        # A mapping of node id's after delNodes have been removed
        # nodeIdMap: key = old index, value = new index
        # nodeIdMapRev: index = new index, value = old index 
        nodeIdMapRev = set(range(self.numNodes))
        for d in delNodes:
            nodeIdMapRev.discard(d)
        nodeIdMapRev = list(nodeIdMapRev)
        nodeIdMapRev.sort()
        nodeIdMap = dict(zip(nodeIdMapRev, range(len(nodeIdMapRev))))
        
        lSNeighbours = 0 # left shift neighbours; how much we've removed
        numNeighboursList = [] # keep track on the number of neighbours
        for n in range(self.numNodes):
            # iterate over the nodes
            if n in delNodes:
                # remove this node altogether
                lSNeighbours += self.nodePos[n+1] - self.nodePos[n]
            else:
                # node remains, what about neighbours?
                numNeighbours = 0
                for j in range(self.nodePos[n], self.nodePos[n+1]):
                    if self.neighbours[j] not in delNodes:
                        # Keep the node
                        self.neighbours[j - lSNeighbours] = nodeIdMap[self.neighbours[j]]
                        # This moves the neighbour lSNeighbours left and fixes the id
                        numNeighbours += 1
                    else:
                        # Delete the node; tell code to move one position left
                        lSNeighbours += 1
                numNeighboursList.append(numNeighbours)
                # Store the number of neighbours
        # We have updated all nodes' neighbours; copy newNodePos to nodePos
        self.numNodes = len(numNeighboursList)
        for i in range(self.numNodes):
            self.nodePos[i+1] = self.nodePos[i] + numNeighboursList[i]
        # Update coordinates by removing the nodes from delNodes
        newCoords = [self.coords[i] for i in xrange(len(self.coords)) if i not in delNodes]
        self.coords = newCoords
        
    def getNeighbours(self, node):
        """Returns an array of neighbours of a node"""
        return self.neighbours[self.nodePos[node]:self.nodePos[node+1]]

    def __str__(self):
        s = 'numNodes: '+str(self.numNodes)+'\nnodePos: '
        if self.numNodes > 0:
            s += str(list(self.nodePos[0:self.numNodes+1]))
        else:
            s += '[]'
        s+='\nneighbours: '
        if self.numNodes > 0 and self.nodePos[self.numNodes] > 0:
            s += str(list(self.neighbours[0:(self.nodePos[self.numNodes])]))
        else:
            s += '[]'
        s += '\ncoords: '+str(self.coords)
        return s



#reads tensor from file
#creates graph for random walk algo
#also populates Bloom filter
def createGraph(FILENAME,bf,bfMask):

    #contains for every x dict of y
    #contains for every (x,y) list of z 
    indexmap = {}
    gr = ListGraph()
    linenum = 0
    with open(FILENAME,'r') as f:
        for l in f:
            linenum += 1
            if linenum%1000000==0:sys.stderr.write('createGraph iteration %i \n'%linenum)
            a = l.split(" ")
            x = int(a[0].strip())
            y = int(a[1].strip())
            z = int(a[2].strip())
            gr.add(x,y,z)
            bf.add(x,y,z)
            bfMask.add(x,y,z)
            if not x in indexmap:indexmap[x] = {}
            if not y in indexmap[x]:indexmap[x][y] = set()
            indexmap[x][y].add(z)
    agr = ArrayGraph(gr)
    return (agr,bf,bfMask,indexmap)



#performs random walk on current graph object
#takes as input the length and number of rnd walks to be performed
#returns frequency list of nodes affected by the walk
#new walk starts from already discovered nodes. With higher probability from node with higher freq
def randomWalk(nodePos, neighbours, startNode, removedNodes):

    #contains node id and the frequency it was encounered in the walk
    frequencies = {}
    #contains node ids with multiplicity
    #used to pick starting node of next walk
    multinodes = []
    for i in xrange(GlobalOptions.walk_num):
        if i>0:
            startNode = random.choice(multinodes)
        else:
            frequencies[startNode] = 0
        frequencies[startNode] += 1
        multinodes.append(startNode)
        for j in range(GlobalOptions.walk_length):
            timesSampled = 0
            while True:
                # Rejection sampling for the next node
                randNeighIdx = random.randint(nodePos[startNode], nodePos[startNode+1] - 1)
                candStartNode = neighbours[randNeighIdx]
                timesSampled += 1
                if candStartNode not in removedNodes:
                    startNode = candStartNode
                    break
                elif timesSampled > 10:
                    # We've sampled too many times w/ rejection sampling, change to other methods
                    n = [neighbours[i] for i in range(nodePos[startNode], nodePos[startNode+1]) if neighbours[i] not in removedNodes]
                    # We expect stat node being its own neighbour
                    startNode = random.choice(n)
                    break
            if startNode not in frequencies: frequencies[startNode] = 0
            frequencies[startNode] += 1
            multinodes.append(startNode)

    totalnum = len(multinodes)
    return (frequencies,totalnum)

def randomWalkPar(walkqueue,results, nodePos, neighbours):

    sys.stderr.write('started randomWalkPar\n')
    while True:
        a = walkqueue.get()
        if a is None:
            results.close()
            return
        else:
            start = a[0]
            removedNodes = a[1]
            (frequencies,totalnum) = randomWalk(nodePos, neighbours, start, removedNodes)
            results.put((frequencies,totalnum))
    return



#calls randomWalk, creates blocks and removes affected nodes
#based on walk output
#runs max_num_threads iterations of the algo in parallel, then does the contractions
#before running the next iterations
def randomWalkAlgoPar(gr,bf,bfMask):

    sys.stderr.write('starting randomWalkAlgoPar \n')
    blocks = Blockclass(1)
    #number of thread is whatever the machine settings allow
    #max_num_threads = mp.cpu_count()
    #this is how many random walks are run in each iteration

    #queue that contains the input for the random walks
    walkqueue = mp.Queue()
    #queue that fetches the result from the parallel processes
    results = mp.Queue()

    removedNodes = set() # Contains the nodes that are removed, but still in the graph

    # Initiate and start workers
    Workers = [mp.Process(target=randomWalkPar, args=(walkqueue,results,gr.nodePos,gr.neighbours)) for i in range(GlobalOptions.num_threads)]
    for each in Workers:
        each.start()

    max_num_tasks = GlobalOptions.num_threads*GlobalOptions.job_factor
    while gr.numNodes > 0:
        num_tasks = min(max_num_tasks, gr.numNodes)
        # Let's get some nodes to start with
        # Check simultaneously for nodes with no neighbours
        startnodes = []
        candNodes = range(gr.numNodes)
        random.shuffle(candNodes)
        for cand in candNodes:
            if cand in removedNodes:
                continue
            elif len(set(gr.getNeighbours(cand)).difference(removedNodes)) == 0:
                removedNodes.add(cand) # this node has no neighbours
                continue
            else:
                startnodes.append(cand)
                if len(startnodes) == num_tasks:
                    break
                
            
        # Put stuff into the working queue
        for i in xrange(len(startnodes)):
            walkqueue.put((startnodes[i], frozenset(removedNodes)))

        # Start reading the results; there are len(startnodes) of them
        for i in xrange(len(startnodes)):
            res = results.get()
            (frequencies, totalnum) = res
            if totalnum is not None:
                (blocks,removeidsl) = createBlockPar(gr,bfMask,blocks,frequencies,totalnum)
                removedNodes.update(removeidsl)

        if len(removedNodes) > gr.numNodes / GlobalOptions.rebuild_freq \
            or gr.numNodes < 4*max_num_tasks:
            sys.stderr.write('Removing nodes: had %i, will remove %i\n'%(gr.numNodes, len(removedNodes)))
            gr.deleteNodes(removedNodes)
            removedNodes.clear()
        sys.stderr.write('Number of nodes left in graph %i\n'%(gr.numNodes - len(removedNodes)))

    # That's it for the random walk, all's done
    for i in xrange(GlobalOptions.num_threads):
        # Poison pill to tell the workers to quit
        walkqueue.put(None)

    walkqueue.close() # Probably useless
    results.close() # Probably useless
    sys.stderr.write('randomWalk end\n')
    return (bfMask, blocks)
 

#creates a block based on the frequencies of the random walk
#only creates the block if the size is above minsize and density is above threshold 
def createBlockPar(gr,bfMask,blocks,frequencies,totalnum):

    removeids = set()
    avgnum = float(totalnum)/float(len(frequencies))
    X = set([])
    Y = set([])
    Z = set([])
    for nId in frequencies:
        if frequencies[nId] >= avgnum:
            (x,y,z) = gr.coords[nId]
            X.add(x)
            Y.add(y)
            Z.add(z)
        removeids.add(nId)
    if len(X)<GlobalOptions.minsize \
        or len(Y)<GlobalOptions.minsize \
        or len(Z)<GlobalOptions.minsize \
        or len(X)*len(Y)*len(Z) < GlobalOptions.minvol:
        return (blocks,removeids)
    freq = checkDensity(bfMask,X,Y,Z,GlobalOptions.threshold)
    if freq == True:
        blocks.addBlockXYZ(X,Y,Z)
        l = itertools.product(X,Y,Z)
        for (x,y,z) in l:
            bfMask.add(x,y,z)
    return (blocks,removeids)



#given index sets X,Y,Z checks the density of the resulting block
#in bfMask
def checkDensity(bfMask,X,Y,Z,thres):

    size = len(X)*len(Y)*len(Z)
    tp = bfMask.check_ones_product(X,Y,Z,int(math.floor(size*thres)))
    if tp>=int(math.floor(size*thres)):return True
    return False


#first removes indices covered by any block from indexmap
#finds 2x2x2 monochromatic blocks in remaining indices
def monoMerge(blocks,bf,bfMask,indexmap):

    sys.stderr.write('monoMerge\n')
    sys.stderr.write('%s\n'%str(time.time()))
    #remove index  triplets from indexmap that are already covered by block
    for bId in blocks.blocks:
        (X,Y,Z) = blocks.coords[bId]
        l = itertools.product(X,Y,Z)
        for (x,y,z) in l:
             if x in indexmap:
                 if y in indexmap[x]:
                     indexmap[x][y].discard(z)
                     if len(indexmap[x][y]) <2:del indexmap[x][y]
                 if len(indexmap[x])<2:del indexmap[x]
    candidates = ((x1,x2) for x1 in indexmap for x2 in indexmap if x1>x2 and len(set(indexmap[x1].keys()) & set(indexmap[x2].keys()))>1)
    for (x1,x2) in candidates:
        ycand = set(indexmap[x1].keys()) & set(indexmap[x2].keys())
        zcand = ((y1,y2) for y1 in ycand for y2 in ycand if y1>y2 and len(set(indexmap[x1].keys()) & set(indexmap[x2].keys()))>1)
        for (y1,y2) in zcand:
            S = set(indexmap[x1][y1])&set(indexmap[x1][y2])&set(indexmap[x2][y1])&set(indexmap[x2][y2])
            if len(S)>1:
                blocks.addBlockXYZ(set([x1,x2]),set([y1,y2]),S)
    sys.stderr.write('monoMerge end\n')
    sys.stderr.write('%s\n'%str(time.time()))
    return (bfMask,blocks)

#returns list of (blockid,[dependent blocks]) pairs
#where blockids correspond to blocks independent of each other
#[dependent blocks] contain ids that share a cell(!) with blockid
def findIndependentBlocks(blocks, notupdate,xindex,yindex,zindex):

    independentblocks = {}
    for bId in blocks.blocks:
        if bId in notupdate:continue
        isindependent = True
        candx = []
        (X,Y,Z) = blocks.coords[bId]
        for x in X:
            for nId in xindex[x]: 
                if nId in independentblocks:
                    candx.append(nId)
                    isindependent = False
        candy = []
        for y in Y:
            for nId in yindex[y]:
                if nId in independentblocks:
                    candy.append(nId)
                    isindependent = False
        candz = []
        for z in Z:
            for nId in zindex[z]:
                if nId in independentblocks:
                    candz.append(nId)
                    isindependent = False
        if isindependent == True: independentblocks[bId] = []
        else:
            #find blocks in independentblocks that share at least one cell  with bId
            neighbors = set(candx) & set(candy) & set(candz)
            for nId in neighbors:
                independentblocks[nId].append(bId)
    return independentblocks

#returns inverted index of coordinates -- blocks that contain them for every dim
def invertedBlockIndex(blocks):

    xindex = {}
    yindex = {}
    zindex = {}
    for bId in blocks.blocks:
        (X,Y,Z) = blocks.coords[bId]
        for x in X:
            if not x in xindex: xindex[x] = set([])
            xindex[x].add(bId)
        for y in Y:
            if not y in yindex: yindex[y] = set([])
            yindex[y].add(bId)
        for z in Z:
            if not z in zindex: zindex[z] = set([])
            zindex[z].add(bId)
    return (xindex,yindex,zindex)

# Temporary global variables for read-only access for tryMerge
GlobalBF = None
GlobalBlocks = None

def tryMerge(qChosenBlocks,results):

    threshold = GlobalOptions.threshold

    while True:
        #get next block from queue
        a= qChosenBlocks.get()
        #end of queue has been reached
        if a is None:
            results.close()
            return
        else:
            (bId,candidates) = a
            mergedblocks = []
            (bX,bY,bZ) = GlobalBlocks.coords[bId]
            X = bX.copy()
            Y = bY.copy()
            Z = bZ.copy()
            #decide for every candidate block whether to merge
            for nId in candidates:
                (nX,nY,nZ) = GlobalBlocks.coords[nId]
                #size of the newly covered area
                size = len(X|nX)*len(Y|nY)*len(Z|nZ) - len(X)*len(Y)*len(Z) - len(nX)*len(nY)*len(nZ) + len(X&nX)*len(Y&nY)*len(Z&nZ)
                error = GlobalBF.check_merge(X, Y, Z, nX, nY, nZ, int(math.floor(size*threshold)));
                if error<=int(math.floor(size*threshold)+1):
                    X|= nX
                    Y|= nY
                    Z|= nZ
                    mergedblocks.append(nId)
            results.put((bId,(X,Y,Z),mergedblocks))
            

def subsequentMerge(blocks,bf,bfMask):

    sys.stderr.write('subsequentMerge\n')
    #number of thread is whatever the machine settings allow
    #max_num_threads = mp.cpu_count()
 
    (xindex,yindex,zindex) = invertedBlockIndex(blocks)
    #list fo block ids that cannot be updated
    notupdate = []
    # Global variables for parallel threads' read-only access
    global GlobalBlocks
    global GlobalBF
    GlobalBlocks = blocks
    GlobalBF = bfMask
    # Queues for tasks and results
    qChosenBlocks = mp.Queue()
    results = mp.Queue()
    # Workers
    Workers = [mp.Process(target=tryMerge, args=(qChosenBlocks,results)) for i in range(GlobalOptions.num_threads)]
    for w in Workers:
        w.start()

    
    #true if there was at least one merge in the previous iteration
    continueMerge = True
    while continueMerge:
        continueMerge = False
        independentblocks = findIndependentBlocks(blocks,notupdate,xindex,yindex,zindex)

        # Put work into the queue
        for bId in independentblocks:
            qChosenBlocks.put((bId,independentblocks[bId]))

        # Results wait here
        for i in xrange(len(independentblocks)):
            res = results.get()
            (bId,(X,Y,Z),mergedblocks) = res
            if len(mergedblocks)==0:
                #newBlock is not merged to anything.
                #it is not a candidate any more for left side of merge
                notupdate.append(bId)
            else:
                continueMerge = True
                #executes merge
                blocks.mergeXYZ(bId,X,Y,Z)
                #update bfMask
                (lX,lY,lZ) = blocks.coords[bId]
                cells = itertools.product(lX,lY,lZ)
                for (x,y,z) in cells:
                    bfMask.add(x,y,z)
                for x in lX: 
                    if not bId in xindex[x]:xindex[x].add(bId)
                for y in lY: 
                    if not bId in yindex[y]:yindex[y].add(bId)
                for z in lZ: 
                    if not bId in zindex[z]:zindex[z].add(bId)
                for nId in mergedblocks:
                    (nX,nY,nZ) = blocks.coords[nId]
                    for x in nX: 
                        if nId in xindex[x]:xindex[x].remove(nId)
                    for y in nY: 
                        if nId in yindex[y]:yindex[y].remove(nId)
                    for z in nZ: 
                        if nId in zindex[z]:zindex[z].remove(nId)
                    blocks.blocks.discard(nId)
                # End FOR
            # End IF
        # End FOR
    # End While

    #poison pill to signal end of threads
    for i in range(GlobalOptions.num_threads):
        qChosenBlocks.put(None)
    qChosenBlocks.close()
    results.close()
    # Remove global variables
    del GlobalBF
    del GlobalBlocks
    
    sys.stderr.write('subsequentMerge end\n')
    return (bfMask,blocks) 


def writeblocks_singlefile(blocks,BLOCKFILE):

    with open(BLOCKFILE,'w') as f:
        for bId in blocks.blocks:
            (X,Y,Z) = blocks.coords[bId]
            for x in X:
                f.write('%i\t'%x)
            f.write('\n')
            for y in Y:
                f.write('%i\t'%y)
            f.write('\n')
            for z in Z:
                f.write('%i\t'%z)
            f.write('\n\n')

def _writecol_matlab(file, col, rowids):
    rowids = list(rowids)
    rowids.sort()
    for row in rowids:
        file.write('%i %i 1\n'%(row, col))

def writeblocks_matlab(blocks, BLOCKFILE):
    fA = open(BLOCKFILE+'.A', 'w')
    fB = open(BLOCKFILE+'.B', 'w')
    fC = open(BLOCKFILE+'.C', 'w')
    col = 1
    for bId in blocks.blocks:
        (X, Y, Z) = blocks.coords[bId]
        _writecol_matlab(fA, col, X)
        _writecol_matlab(fB, col, Y)
        _writecol_matlab(fC, col, Z)
        col += 1
    fA.close()
    fB.close()
    fC.close()
                     
        
#runs the whole walknmerge algo starting from the initial file read
def walknmerge(bf,bfMask):

    
    f = open(GlobalOptions.TIMEFILE,'a')
    sys.stderr.write('%s\n'%str(time.ctime()))
    f.write('tensor file: %s\tthreshold: %s\tnum_threads: %s\n'%(GlobalOptions.FILENAME,GlobalOptions.threshold,GlobalOptions.num_threads))
    f.write('start createGraph\t%s\n'%time.time())
    (gr,bf,bfMask,indexmap) = createGraph(GlobalOptions.FILENAME, bf, bfMask)
    f.write('end createGraph\t%s\n'%time.time())
    sys.stderr.write('createGraph done\n')
    f.write('start randomWalkAlgoPar\t%s\n'%time.time())
    (bfMask,blocks) = randomWalkAlgoPar(gr,bf,bfMask)
    f.write('end randomWalkAlgoPar\t%s\n'%time.time())
    sys.stderr.write('randomWalkAlgoPar done\n')
    writeblocks_singlefile(blocks, GlobalOptions.RANDOMBLOCKS)
    del gr
    f.write('start merge phase\t%s\n'%time.time())
    (bfMask,blocks) = monoMerge(blocks,bf,bfMask,indexmap)
    sys.stderr.write('monoMerge done\n')
    (bfMask,blocks) = subsequentMerge(blocks,bf,bfMask)
    f.write('end merge phase\t%s\n'%time.time())
    sys.stderr.write('subsequentMerge done\n')
    if GlobalOptions.OUTPUT_MATLAB:
        writeblocks_matlab(blocks, GlobalOptions.BLOCKFILE)
    else:
        writeblocks_singlefile(blocks, GlobalOptions.BLOCKFILE)
    f.close()
    sys.stderr.write('%s\n'%str(time.ctime()))
    return (bf,bfMask,blocks)




def main(args=[]):
    from optparse import OptionParser, OptionGroup

    parser = OptionParser()
    parser.add_option('-f', '--file', dest="FILENAME", help="Read input from FILE", metavar="FILE")
    parser.add_option('-o', '--output', dest="BLOCKFILE", help="Write final output to FILE", metavar="FILE")

    # Additional output options
    OUTPUT_opt_group = OptionGroup(parser, "Additional output options", "These options control additional output features")
    OUTPUT_opt_group.add_option( '--time_file', dest="TIMEFILE", help="outputs running time results in this file (default %default)", metavar="FILE",default="time_file.txt")
    OUTPUT_opt_group.add_option('--matlab', action='store_true', dest='OUTPUT_MATLAB', default=False, help='Print output in three files (suffixes ".A", ".B", and ".B"), suitable for loading to Matlab')
    OUTPUT_opt_group.add_option('--single-file', action='store_false', dest='OUTPUT_MATLAB', help='Print the output in single file (turn off three-file printing of "--matlab", default behaviour)')
    parser.add_option_group(OUTPUT_opt_group)


    # Options controlling the blocks
    BLCK_opt_group = OptionGroup(parser, "Options for the blocks", "These options control the shape and density of the blocks the algorithm aims at finding")
    BLCK_opt_group.add_option('-t', '--threshold', dest="threshold", help="Set the threshold for merging blocks (default %default)",
                      type="float", default=0.9)
    BLCK_opt_group.add_option('-m', '--minsize', type='int', dest='minsize', help="The minimum size of blocks in each mode (default %default)", default=2)
    BLCK_opt_group.add_option('-M', '--min-volume', type='int', dest='minvol', help="The minimum volume of a block (default 3*MINSIZE). While MINSIZE controls the minimum dimension, this parameter can be used to require other dimensions of the block to be larger than the smallest one.")
    parser.add_option_group(BLCK_opt_group)

    # Options for random walk
    RW_opt_group = OptionGroup(parser, "Options for random walk", "These options control the behaviour of the random walk")
    
    RW_opt_group.add_option('-r', '--randomwalk-output', dest="RANDOMBLOCKS",
                      help="Write the results of the random walk algorithm to FILE", metavar="FILE")
    RW_opt_group.add_option('--walk_length', type='int', dest='walk_length', help="The length of the random walks (default %default)", default=10)
    RW_opt_group.add_option('--walk_number', type='int', dest='walk_num', help="The number of random walks in each iteration (default %default)", default=1000)
    RW_opt_group.add_option('--rebuild_frequency', type='int', dest='rebuild_freq', help="Rebuild the graph after removing |V|/REBUILD_FREQ nodes (default %default)", default=4)
    parser.add_option_group(RW_opt_group)

    # Options for parallel processing
    PAR_opt_group = OptionGroup(parser, "Options controlling parallel processing", "These options control the parallel processing used by the algorithm")
    PAR_opt_group.add_option('--num_threads', type="int", dest='num_threads', help="The number of threads the algorithm uses (default %default)", default=2)
    PAR_opt_group.add_option('--job_factor', type="int", dest='job_factor', help="Controls the number of parallel random walks, which is JOB_FACTOR*NUM_THREADS (default %default)", default=5)
    parser.add_option_group(PAR_opt_group)

    # Options for Bloom filter
    BF_opt_group = OptionGroup(parser, "Options for Bloom filters", "These options control the behaviour of the Bloom filters")
    BF_opt_group.add_option('--capacity', dest='capacity', help="The capacity of the Bloom filter (default %default)",
                            type="int", default=1000000000)
    BF_opt_group.add_option('--error_rate', dest='errorRate', help="The error rate of the Bloom filter (default %default)",
                            type="float", default=0.01)
    parser.add_option_group(BF_opt_group)

    

    (options, args) = parser.parse_args(args=args)

    if options.FILENAME is None:
        sys.stderr.write("Input filename must be provided!\n")
        return -1

    if options.BLOCKFILE is None:
        options.BLOCKFILE = options.FILENAME+'.blocks'
    if options.RANDOMBLOCKS is None:
        options.RANDOMBLOCKS = options.BLOCKFILE+'.random'

    if options.minvol is None:
        options.minvol = 3*options.minsize

    global GlobalOptions
    GlobalOptions = options # Make options globally known

    # Bloom filter for original data
    bf = BF.BloomFilter(options.capacity, options.errorRate)
    # Bloom filter for covered data
    bfMask = BF.BloomFilter(options.capacity, options.errorRate)

    # set the number of parallel threads in the Bloom filter
    os.environ['OMP_NUM_THREADS'] = '4'
    
    # Run everything
    (bf,bfMask,blocks) = walknmerge(bf, bfMask)
    return 0

if __name__ == "__main__":
    ret = main(sys.argv[1:])
    sys.exit(ret)

