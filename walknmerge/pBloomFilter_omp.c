//
//  main.c
//  pBloomFilterProg
//
//  Created by Pauli Miettinen on 27.8.2012.
//  Copyright (c) 2012 Pauli Miettinen. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
//#include <limits.h>/* for CHAR_BIT */
#include <inttypes.h>
#include <time.h>
#include <omp.h>
#include "pBloomFilter.h"


//#define BYTEOFN(n) (((unsigned)n)>>3)
#define BYTEOFN(n) ((n)/8)
#define BITOFN(n) ((unsigned)(n) & 7)
//#define BITOFN(n) ((n)%8)


#define SETBIT(a, n) ((a)[BYTEOFN(n)] |= (1<<(BITOFN(n))))
#define GETBIT(a, n) ((a)[BYTEOFN(n)] & (1<<(BITOFN(n))))


#define KEYLEN 4*3



//creates a bit_array of appropriate size
mbf *
create_mbf(unsigned int capacity, double errorRate)
{
    
    mbf *bf;
    int numHashFuncs = 6;
    unsigned long numBits;
    
    if(!(bf=(mbf*)malloc(sizeof(mbf)))) return NULL;
    bf->capacity = capacity;
    bf->errorRate = errorRate;
    /* Old way !?
     bf->numBitsExp = (unsigned int)(ceil(log2(2 * capacity * fabs(log2(errorRate)))));
     */
    bf->numBitsExp = (unsigned int)(ceil(log2(-((double)numHashFuncs*(double)capacity)/log(1-pow(errorRate, 1.0/(double)numHashFuncs)))));
    if (bf->numBitsExp > 31) {
      /* Will overflow numBits, release bf and return NULL */
      free(bf);
      return NULL;
    }
    numBits = 1 << bf->numBitsExp;
    bf->numBits = numBits;
    bf->bitMask = numBits - 1;
#if DEBUG
    fprintf(stderr, "Capacity:\t%u\nError rate:\t%f\nBit exponent:\t%u\nNum bits:\t%lu\nBit mask:\t%lu\n",
            bf->capacity, bf->errorRate, bf->numBitsExp, numBits, bf->bitMask);
#endif
    
    if(!(bf->bArray=(char *)malloc(numBits/8))) {
		free(bf);
		return NULL;
    }
    memset(bf->bArray, 0, numBits/8);
    return bf;
}

void
destroy_mbf(mbf* bf)
{
    free(bf->bArray);
    free(bf);
}

unsigned int
get_numBitsExp(const mbf *bf)
{
  return bf->numBitsExp;
}

unsigned long
get_numBits(const mbf *bf)
{
  return bf->numBits;
}

unsigned int
get_capacity(const mbf *bf)
{
  return bf->capacity;
}

double
get_errorRate(const mbf *bf)
{
  return bf->errorRate;
}

/* Jenkins's one-at-a-time hash function */
static inline
unsigned long
jenkins(const char *key)
{
    unsigned long hash, i;
    
    hash = 0;
    for (i = 0; i < KEYLEN; i++) {
        hash += key[i];
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    
    return hash;
}

/* CRC32 by the CPU -- if CPU supports*/
#ifndef NO_SSE42
static inline
unsigned long
crc(const unsigned int key1, const unsigned int key2, const unsigned int key3){
    unsigned int hash = 0;
    hash = __builtin_ia32_crc32si(hash, key1);
    hash = __builtin_ia32_crc32si(hash, key2);
    hash = __builtin_ia32_crc32si(hash, key3);
    return hash;
}
#else
static inline
unsigned long
crc(const unsigned int key1, const unsigned int key2, const unsigned int key3) {
  return ~key1 ^ ~key2 ^ ~key3;
}
#endif

/* Fowler--Noll--Vo hash v1 */
static inline
unsigned long
fnv1(const char *key)
{
    unsigned long hash, i;
    hash = 2166136261UL;

    for (i = 0; i < KEYLEN; i++) {
        hash *= 16777619UL;
        hash ^= key[i];
    }
    return hash;
}

/* Fowler--Noll--Vo hash v1a */
static inline
unsigned long
fnv1a(const char *key)
{
    unsigned long hash, i;
 
    hash = 2166136261UL;
    for (i = 0; i < KEYLEN; i++) {
        hash ^= KEYLEN;
        hash *= 16777619UL;
    }
    return hash;
}

/* SAX hash */
static inline
unsigned long
sax(const char *key)
{
    unsigned long h = 0;
    int i;
    for (i=0; i<KEYLEN; i++) {
        h ^= (h<<5)+(h>>2) + (unsigned char)key[i];
    }
    return h;
}

/* Random memory map hash function */
static inline
unsigned long
memorymap(const char *key)
{
    unsigned long hash, i;
    
    /* The memory map */
    const unsigned long map[256] = {
        16641514868538588497UL,
        5481713173636788678UL,
        4026083515215031171UL,
        1645791160120140878UL,
        14921196707213484745UL,
        18152668261542204947UL,
        100413521923678827UL,
        4316370458278505151UL,
        16708666746528090225UL,
        147510488617749356UL,
        12982454067546162554UL,
        12696978494833739786UL,
        6268158255811782029UL,
        11086204802226293953UL,
        13516491422511768542UL,
        11790686955926731762UL,
        12261534686626543620UL,
        17660792248027478477UL,
        2565269575125552025UL,
        17718286192493570559UL,
        5240725651377703243UL,
        5569315900536519788UL,
        2817318945570482041UL,
        3910987984757596064UL,
        17962109997253958350UL,
        2365530130694650677UL,
        1567746475191500987UL,
        6152991906036609450UL,
        1434730045048974293UL,
        17183296646520025696UL,
        7992878216618217466UL,
        4940945872045863352UL,
        5044832474179542029UL,
        13353061892816675509UL,
        2231559103826375535UL,
        8859667441296985955UL,
        5389092287913265459UL,
        16586246299441809046UL,
        7008421439621964007UL,
        14135533300544271336UL,
        5616397816500704902UL,
        12198838935048965585UL,
        1884688689715139528UL,
        5032349245076506661UL,
        778088439760318566UL,
        17103072013675933923UL,
        3893568285620398563UL,
        2024970550202295238UL,
        12024677203464631203UL,
        16404136977333870055UL,
        17129872083911066379UL,
        10233907424355819261UL,
        12420003976584900832UL,
        10696734078028696327UL,
        2205408347058605367UL,
        16005118410963708392UL,
        2342137227198158321UL,
        5287722708085650554UL,
        15170718334447646850UL,
        15962660376181202168UL,
        5726913087849560817UL,
        61864355757115153UL,
        4480112470209536916UL,
        5448112641759421468UL,
        8221628855415839672UL,
        7516031042328979206UL,
        14376142079062598865UL,
        156942060620897759UL,
        14916887416268576885UL,
        15099974986041929190UL,
        8606485898947836203UL,
        10044652556180367806UL,
        1430280070075571173UL,
        18300014174382849679UL,
        8200264107917844704UL,
        3133470125035123569UL,
        8582775785953475905UL,
        8909359549832341640UL,
        10811346750115651961UL,
        3378817605666029070UL,
        16928640619258811760UL,
        17990227769519248679UL,
        3870151451924470771UL,
        11021835916912437266UL,
        16501916035283588559UL,
        1133475510424770880UL,
        18316490649730842258UL,
        8528534569928830632UL,
        4011913948235800991UL,
        3282824100409945333UL,
        8967232935231441143UL,
        11377418049575072217UL,
        17496065754039774101UL,
        14019301800567387134UL,
        17796259623312383778UL,
        6987449588364332758UL,
        8663194661255744862UL,
        13350450029375818676UL,
        1987743054657577725UL,
        83932856537765985UL,
        16904630893037076622UL,
        6159811378592281685UL,
        13113865364467775480UL,
        8804208200493157863UL,
        2358861106261135451UL,
        2241879239211527760UL,
        456687112821411248UL,
        16475173870334554687UL,
        2700456382440753121UL,
        17084252451733943907UL,
        16281836421602287384UL,
        5228943952901335324UL,
        4496174792267111065UL,
        8858496779620641255UL,
        11583654912609775818UL,
        5064410696306015471UL,
        15031877030907236746UL,
        6837172794520805396UL,
        14165903481330382739UL,
        17421691498600736126UL,
        8105457796873583578UL,
        3008681866734111477UL,
        8310257244294534395UL,
        1066107577491201203UL,
        10990044176074804117UL,
        8693435999226433049UL,
        13586432128908591493UL,
        17771397977286580853UL,
        4995470756839211663UL,
        10216722290786995034UL,
        15291296493937748935UL,
        2729938677569983625UL,
        13379876176637820031UL,
        4365286814869443754UL,
        3059596718398280825UL,
        7532370240252796010UL,
        1541379402523585516UL,
        14817973495817531885UL,
        14416018830458989627UL,
        4849424337715193320UL,
        18337352443513073955UL,
        14426198381155634686UL,
        4281151481996122625UL,
        12338068896429017371UL,
        14358628087930388595UL,
        15972405569331841960UL,
        12265671236414267783UL,
        13921432074836134908UL,
        6962375108188131384UL,
        3332092922946934915UL,
        345826220756793364UL,
        2693339444550302818UL,
        17701092525962932816UL,
        9593911324576876146UL,
        17783834899831825686UL,
        4431811113159851803UL,
        15966798464725071158UL,
        2815743731261244915UL,
        4984071114099988670UL,
        6639804620366913611UL,
        13664341030763451605UL,
        11309640429871696658UL,
        10735678295104430334UL,
        15866135912095708554UL,
        6617800017263573125UL,
        11766126103955309015UL,
        18165942026328211449UL,
        6532932457609705379UL,
        6053522093677056744UL,
        10344874381867588908UL,
        18255282607178429898UL,
        11962998800176675687UL,
        3750010244955054472UL,
        18146106664626214499UL,
        8588948597714614673UL,
        4954826370598162312UL,
        8169700494961526559UL,
        2528938746176374720UL,
        3986623565280561641UL,
        7830962634233267724UL,
        11640118183127681889UL,
        9898628278022877083UL,
        4524343114181744609UL,
        2341214564767489269UL,
        6340321489085931684UL,
        17747809545687042006UL,
        2437434076487267350UL,
        10305251934047480748UL,
        12239756467975017139UL,
        10647482702034007139UL,
        17929217089562570016UL,
        5211549390327325301UL,
        11482815698705564571UL,
        10348579429699534332UL,
        15993357975695443202UL,
        11214235792683306338UL,
        3510898627031431994UL,
        5821720629490329389UL,
        14662355070111761377UL,
        7222662030956197177UL,
        17774935623740240741UL,
        11974945284825894305UL,
        3154386802618281485UL,
        6737794360364039472UL,
        8855824703280951686UL,
        8918993477388647574UL,
        7723816472055846846UL,
        3535937974612616617UL,
        14439190858556730527UL,
        9435560680890823152UL,
        7529788566064964358UL,
        7347954879052394703UL,
        8094483556490263408UL,
        7444984120354473770UL,
        15507940574816622143UL,
        10999055283212219643UL,
        11030082005362357676UL,
        8371749768761868966UL,
        16071974930139108631UL,
        1463308055040690734UL,
        8851870818045759765UL,
        4801643612748837400UL,
        7129380158302790714UL,
        3630118621488280345UL,
        6831750680171112241UL,
        10608352339955266542UL,
        13540379499624778717UL,
        1730749883422237295UL,
        11858075851677271069UL,
        13334886760985214589UL,
        16467238424153065047UL,
        8688590530790906097UL,
        18152625423704687932UL,
        11193953015499457069UL,
        12212317236330363390UL,
        7250074785886813622UL,
        6290323086106912583UL,
        6982372400623031343UL,
        5553089166070758402UL,
        16428064095211128170UL,
        18238511787998081290UL,
        16023221515545054482UL,
        8846867797927803658UL,
        11661770272686109143UL,
        1870973768613085585UL,
        14630271674093952117UL,
        9239170233010429543UL,
        10711382682746280950UL,
        13398207369768038462UL,
        7250063409797386903UL,
        7919588884921339668UL,
        12982330842273712550UL,
        14185588979251282778UL,
        3564806499400410296UL,
        11178608601910942982UL,
        10004369963852187769UL
    };
    /* End the memory map */
    
    hash = 0;
    for (i = 0; i < KEYLEN; i++) {
        hash ^= map[key[i]];
    }
    return hash;
    
}

/* XOR of the keys */
static inline
unsigned long
xorhash(const int key1, const int key2, const int key3)
{
    return key1 ^ key2 ^ key3;
}

/* Fowler--Noll--Vo hash v1a for the XOR */
static inline
unsigned long
xorfnv1a(const int key1, const int key2, const int key3)
{
    unsigned long hash, i;
    unsigned long int key = key1 ^ key2 ^ key3;
    const char sift[4] = {0, 8, 16, 24};
    
    hash = 2166136261U;
    for (i = 0; i < 4; i++) {
        const char byte = (key >> sift[i]) & 0xff;
        hash ^= byte;
        hash *= 16777619U;
    }
    return hash;
}


static inline
void
bytearray(char *array, const int key1, const int key2, const int key3)
{
    unsigned long j, i;
    const char shift[4] = {0, 8, 16, 24};
    const int key[3] = {key1, key2, key3};      
        
    for (j=0; j < 3; j++) {
        for (i = 0; i < 4; i++) {
            array[j*4+i] = (key[j] >> shift[i]) & 0xff;
        }
    }
}

void
add_mbf(mbf *const bf, const int key1, const int key2, const int key3)
{
    char key[KEYLEN];
    bytearray(key, key1, key2, key3);
    
    SETBIT(bf->bArray, xorhash(key1, key2, key3) & (bf->bitMask));
    SETBIT(bf->bArray, xorfnv1a(key1, key2, key3) & (bf->bitMask));
    SETBIT(bf->bArray, memorymap(key) & (bf->bitMask));
    SETBIT(bf->bArray, fnv1(key) & (bf->bitMask));
    SETBIT(bf->bArray, fnv1a(key) & (bf->bitMask));
    //SETBIT(bf->bArray, jenkins(key) & (bf->bitMask));
    //SETBIT(bf->bArray, sax(key) & (bf->bitMask));
    SETBIT(bf->bArray, crc(key1, key2, key3) & (bf ->bitMask));
}

int
check_mbf(const mbf *bf, const int key1, const int key2, const int key3)
{
    char key[KEYLEN];
    
    
    if (!GETBIT(bf->bArray, xorhash(key1, key2, key3) & (bf->bitMask)) ||
        !GETBIT(bf->bArray, crc(key1, key2, key3) & (bf->bitMask)) ||
        !GETBIT(bf->bArray, xorfnv1a(key1, key2, key3) & (bf->bitMask)))
        return 0;
    else
        bytearray(key, key1, key2, key3);
    if (GETBIT(bf->bArray, memorymap(key) & (bf->bitMask)) &&
        GETBIT(bf->bArray, fnv1(key) & (bf->bitMask)) &&
        GETBIT(bf->bArray, fnv1a(key) & (bf->bitMask)) //&&
        //GETBIT(bf->bArray, sax(key) & (bf->bitMask)) &&
        //GETBIT(bf->bArray, jenkins(key) & (bf->bitMask))
        )
    {
        return 1;
    }
    return 0;
}

int
multi_check_mbf_zeros(const mbf *bf, const int *key1, const int *key2, const int *key3, const int len, const int maxerr)
{
    int i, res;
    res = 0;
    for (i=0; i < len; i++) {
        res += 1-check_mbf(bf, key1[i], key2[i], key3[i]);
	if (res > maxerr)
	  return res;
    }
    return res;
}

/*
 * Checks the number of 1s in the filter using all the combinations of values in x, y, and z
 */
int
check_ones_product(const mbf *bf, const int *x, const int lenx, const int *y, const int leny, const int *z, const int lenz, const int minones)
{
  int i, j, k, res;
  res = 0;
  for (i=0; i < lenx; i++) {
    for (j=0; j < leny; j++) {
      for (k=0; k < lenz; k++) {
	res += check_mbf(bf, x[i], y[j], z[k]);
	if (res > minones)
	  return res;
      }
    }
  }
  return res;
}

int
multi_check_mbf_ones(const mbf *bf, const int *key1, const int *key2, const int *key3, const int len, const int minones)
{
    int i, res;
    res = 0;
    for (i=0; i < len; i++) {
        res += check_mbf(bf, key1[i], key2[i], key3[i]);
	if (res > minones)
	  return res;
    }
    return res;
}


/* A function to compare two ints for qsort */
int
compint(const void *a, const void *b)
{
  const int *ia = (const int *)a;
  const int *ib = (const int *)b;

  return (*ia > *ib) - (*ia < *ib);
}

/* A function that does in-place uniq for sorted array of ints */
size_t
uniqi(int *a, const size_t l)
{
  size_t offset, i;
  int curr;

  for (i=1, offset=0, curr = a[0]; i < l; i++) {
    /* DEBUG */
    //fprintf(stderr, "i = %i, offset = %i, curr = %i\n", (int)i, (int)offset, curr);
    if (curr == a[i]) {
      offset++; // we ignore a[i]; offset tells us how many we've ignored
    }
    else {
      curr = a[i]; // update curr
      a[i-offset] = curr; // move the current upwards
    }
  }
  return l - offset;
}

/* Computes the overall error caused by the merge of two blocks */
int
check_merge_mbf(const mbf *bf, 
		const int *r1,  // Rows of the first block 
		const int *c1,  // Cols           "
		const int *t1,  // Tubes          "
		const int *lengths1, // Lengths of the above three
		const int *r2,  // Rows of the second block
		const int *c2,  // Cols           "
		const int *t2,  // Tubes          "
		const int *lengths2, // Lengths of the above three
		const int maxerr // Maximum error
		)
{
  int i, j, k, res;
  int *jr, *jc, *jt;
  //mbf *blocks;
  //unsigned size; 
  size_t jrl, jcl, jtl;

  /* Store the area covered by the existing blocks in the Bloom filter */
  /****** REMOVED
  size = (unsigned)lengths1[0]*lengths1[1]*lengths1[2]			\
    +lengths2[0]*lengths2[1]*lengths2[2]; // maximum size for the two blocks
  blocks = create_mbf(size, 0.01);

  for (i=0; i < lengths1[0]; i++) {
    for (j=0; j < lengths1[1]; j++) {
      for (k=0; k < lengths1[2]; k++) {
	add_mbf(blocks, r1[i], c1[j], t1[k]);
      }
    }
  }
  for (i=0; i < lengths2[0]; i++) {
    for (j=0; j < lengths2[1]; j++) {
      for (k=0; k < lengths2[2]; k++) {
	add_mbf(blocks, r2[i], c2[j], t2[k]);
      }
    }
  }

  ******/

  // The two blocks are now build; start looking thru every possible combination
  // First, initialize space for the indices
  jrl = (size_t)(lengths1[0]+lengths2[0]);
  jr = (int *)malloc(jrl*sizeof(int));
  if (jr == NULL) {
    perror("Error when allocating memory for merged blocks");
    return -1;
  }
  jcl = (size_t)(lengths1[1]+lengths2[1]);
  jc = (int *)malloc(jcl*sizeof(int));
  if (jc == NULL) {
    perror("Error when allocating memory for merged blocks");
    return -1;
  }
  jtl = (size_t)(lengths1[2]+lengths2[2]);
  jt = (int *)malloc(jtl*sizeof(int));
  if (jt == NULL) {
    perror("Error when allocating memory for merged blocks");
    return -1;
  }

  /* DEBUG */
  //fprintf(stderr, "The joint lenghts are: %i, %i, %i\n", (int)jrl, (int)jcl, (int)jtl);

  // Make jr be the concatenation of r1 and r2
  memcpy(jr, r1, lengths1[0]*sizeof(int));
  memcpy(jr+lengths1[0], r2, lengths2[0]*sizeof(int));
  // Make jc be the concatenation of c1 and c2
  memcpy(jc, c1, lengths1[1]*sizeof(int));
  memcpy(jc+lengths1[1], c2, lengths2[1]*sizeof(int));
  // Make jt be the concatenation of t1 and t2
  memcpy(jt, t1, lengths1[2]*sizeof(int));
  memcpy(jt+lengths1[2], t2, lengths2[2]*sizeof(int));

  // Order the lists so we can kill of the repeated indices
  qsort(jr, jrl, sizeof(int), compint);
  qsort(jc, jcl, sizeof(int), compint);
  qsort(jt, jtl, sizeof(int), compint);


  // Remove the repeated indices; this function returns the new length
  // and does in-place substitution to the array
  jrl = uniqi(jr, jrl);
  // We could re-alloc to save memory, but bah
  jcl = uniqi(jc, jcl);
  jtl = uniqi(jt, jtl);


  // And now it's just checking...
  res = 0;
#pragma omp parallel for private(j, k) shared(res)
  for (i=0; i < jrl; i++) {
#pragma omp flush (res)
    if (res > maxerr) {
      continue;
    }
    int myres = 0;
    // DEBUG
    int tid = omp_get_thread_num();
    if (tid == 0)
    {
      fprintf(stderr, "Number of threads is %i\n", omp_get_num_threads());
    }

    for (j=0; j < jcl; j++) {
      for (k=0; k < jtl; k++) {
	/****** REMOVED
	if (check_mbf(blocks, jr[i], jc[j], jt[k]))
	  continue;
	*****/
	// A new block, let's check
	// check_mbf returns either 0 or 1, so 1 XOR x is same as NOT(x) but faster
	myres += 1 ^ check_mbf(bf, jr[i], jc[j], jt[k]); 
      }
    }
#pragma omp atomic
    res += myres;
  } // End for i=1:jrl

  // Post-check clean
  /******* REMOVED
  destroy_mbf(blocks);
  ********/
  free(jr);
  free(jc);
  free(jt);

  return res;
}

//#if 0
int main(int argc, char **argv){
    /* Calling main() with arguments will report timing */
    mbf* bf;
    unsigned long iters, i, j;
    int keys[1000];
    clock_t start, end;
    double time;
    unsigned long numBits;
    
    if (argc > 1) {
        iters = (unsigned long)strtol(argv[1], NULL, 0);
        fprintf(stdout, "Running test with %lu iterations.\n", iters);
    }
    
    iters = 10000000;
    
    /* Create a Bloom Filter and initialize it to alternating series of 0s and 1s */
    bf = create_mbf(100000000, 0.01);
    
    numBits = 1 << bf->numBitsExp;
    
    memset(bf->bArray, 255, numBits/8);
    
    /* Create the test strings */
    for (i=0; i < 1000; i++) {
        keys[i] = (int)i;
    }
    
    /* Do the test */
    start = clock();
    j = 0;
    for (i=0; i < iters; i++) {
        j += check_mbf(bf, keys[i%1000], keys[(i+1)%1000], keys[(i+2)%1000]);
    }
    end = clock();
    time = ((double) (end - start)) / CLOCKS_PER_SEC;
    
    fprintf(stdout, "Number of positives: %lu\n", j);
    fprintf(stdout, "Overall time: %f\n", time);
    
    /* TEST OF CHECK_MERGE IF THERE'S > 1 COMMAND-LINE ARGUMENT */
    if (argc > 2) {
      int *b1, *b2;
      int l1[3] = {100, 100, 100};
      int l2[3] = {100, 100, 100}; 
      int i;
      
      b1 = (int *)malloc(100*sizeof(int));
      b2 = (int *)malloc(100*sizeof(int));

      /* DEBUG */
      /*
      b1[0] = 1;
      b1[1] = 1;
      b1[2] = 2;
      i = uniqi(b1, 3);
      fprintf(stderr, "Length after uniqi = %i, b[1] = %i\n", i, b1[1]);

      return 0;
      */
      // block 1 is [0:99]x[0:99]x[0:99]
      // block 2 is [50:149]x[50:149]x[50:149]
      for (i=0; i<100; i++) {
	b1[i] = i;
	b2[i] = i+50;
      }
      i = 0;
      i = check_merge_mbf(bf, b1, b1, b1, l1, b2, b2, b2, l2, 1);
      fprintf(stdout, "Error with full filter is %i\n", i);
      // Erase the filter
      memset(bf->bArray, 0, numBits/8);
      i = check_merge_mbf(bf, b1, b1, b1, l1, b2, b2, b2, l2, 1<<30);
      fprintf(stdout, "Error with empty filter is %i\n", i);
    }

    
    return 0;
}
//#endif
