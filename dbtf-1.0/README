################################################################################
# Fast and Scalable Distributed Boolean Tensor Factorization
#
# Author: Namyong Park (namyong.park@snu.ac.kr), Seoul National University
#         Sejoon Oh (sejun6431@gmail.com), Seoul National University
#         U Kang (ukang@snu.ac.kr), Seoul National University
#
# Version: 1.1
# Date: Nov 12, 2016
# Main Contact: Namyong Park
#
# This software is free of charge for research purposes.
# For commercial purposes, please contact the authors.
################################################################################

1. General information

DBTF (Distributed Boolean Tensor Factorization) is a distributed algorithm for 
Boolean CP factorization running on the Spark framework.
Given a binary input tensor and rank, DBTF performs Boolean CP factorization on it,
and outputs binary factor matrices.

2. How to run DBTF

IMPORTANT!
- Before you use it, please check that the script files are executable. If not,
you may manually modify the permission of scripts or you may type "make install"
to do the same work.
- You may type "make demo" to try DBTF on a sample tensor.
- Spark should be installed in your system.

To run DBTF, you need to do the followings:
- Prepare a binary input tensor file.
  The binary input tensor should be stored in a sparse tensor format as a plain text file
  where each line is in "MODE1-INDEX SEPARATOR MODE2-INDEX SEPARATOR MODE3-INDEX" format.
  You can use a comma (csv, default), a space (ssv), or a tab (tsv) as a separator, and 
  specify the separator of an input tensor file as an option.

  For example, these are the first five lines of an example tensor file "sample.tensor."

    22,32,28
    23,29,42
    10,15,39
    17,8,49
    3,7,30
    .....

- execute ./dbtf.sh
  Usage: dbtf.sh [options] <tensor-file-path>

    -b <value> | --base-index <value>
          base (start) index of a tensor
    -r <value> | --rank <value>
          rank for tensor decomposition
    -m1 <value> | --mode1-length <value>
          dimensionality of the 1st mode
    -m2 <value> | --mode2-length <value>
          dimensionality of the 2nd mode
    -m3 <value> | --mode3-length <value>
          dimensionality of the 3rd mode
    -nif <value> | --num-initial-factor-sets <value>
          number of initial sets of factor matrices (default=1)
    -ifd <value> | --initial-factor-matrix-density <value>
          density of an initial factor matrix
    -cni <value> | --converge-by-num-iters <value>
          marks the execution as converged when the number of iterations reaches the given value (default=10). this is the default method used for checking convergence.
    -ced <value> | --converge-by-abs-error-delta <value>
          marks the execution as converged when the absolute error delta between two consecutive iterations becomes less than the given value.
    -nci <value> | --num-consecutive-iters-for-converge-by-abs-error-delta <value>
          when --converge-by-abs-error-delta is set, marks the execution as converged when the absolute error delta was smaller than the specified threshold value for the given number of consecutive iterations (default=3)
    -p <value> | --num-partitions <value>
          number of partitions
    --compute-error
          when this option is set, computes the reconstruction error after the decomposition is finished
    -od <value> | --output-dir-path <value>
          path to an output directory (if missing, log messages will be printed only to stdout)
    -op <value> | --output-file-prefix <value>
          prefix of an output file name
    --tensor-file-separator-type <value>
          separator type of an input tensor file: ssv, tsv, or csv (default: csv)
    --help
          prints this usage text
    --version
          prints the DBTF version
    <tensor-file-path>
          input tensor file path
  if --converge-by-num-iters and --converge-by-abs-error-delta are given together, the execution will stop when either of the convergence conditions is satisfied

You can try DBTF by running ./demo.sh.
The demo script executes DBTF on a sample binary tensor (sample.tensor).
