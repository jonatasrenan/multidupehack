#!/usr/bin/env bash

cd $(dirname $(readlink -f $0))

OUTPUT_DIR_PATH=/home/${USER}/tmp/DBTF
rm -rf ${OUTPUT_DIR_PATH}
mkdir -p ${OUTPUT_DIR_PATH}
if [ ! -d "${OUTPUT_DIR_PATH}" ]; then
    echo "unable to make output directory: ${OUTPUT_DIR_PATH}"
    exit 1
fi

hadoop fs -rm -r -f sample.tensor
hadoop fs -put sample.tensor sample.tensor 

./dbtf.sh \
--base-index 0 \
--rank 10 \
--mode1-length 30 \
--mode2-length 40 \
--mode3-length 50 \
--num-initial-factor-sets 5 \
--initial-factor-matrix-density 0.01 \
--converge-by-num-iters 5 \
--num-partitions 8 \
--output-dir-path ${OUTPUT_DIR_PATH} \
--output-file-prefix sample \
--tensor-file-separator-type csv \
sample.tensor

if [ $? -eq 0 ]; then
    if [ "$(ls -A ${OUTPUT_DIR_PATH})" ]; then
        echo "DBTF prints factor matrices in both dense and sparse formats. Final factor matrices in dense format are as follows:"
        for i in $(seq 1 3); do
            echo "[Factor Matrix $i]"
            cat ${OUTPUT_DIR_PATH}/sample_factor${i}_dense.txt
            echo ""
        done
    fi
fi

cd -
